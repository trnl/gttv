/**
 * Created with JetBrains PhpStorm.
 * User: Aliaksandr_Litvinok
 * Date: 5/19/12
 * Time: 9:35 AM
 * To change this template use File | Settings | File Templates.
 */

$(document).ready(function() {
    $('#videos-list li').keynav('active', 'notActive');
    $('#videos-list li:first').removeClass().addClass('active');
    $('.category-switch').keynav('active', 'notActive');

    document.onkeydown=function(e){
        if (!e) e=window.event;

        var element = $('.active');

        switch(e.keyCode) {

            case 37:
                break;

            case 38:
                break;

            case 39:
                break;

            case 40:
                break;

            case 13:
                console.log(element.hasClass('item'))

                if (element.hasClass('category-switch')) {
                    var href = element.find('a').attr('href');
                    window.location = href;
                    return false;
                }

                if (element.find('div').hasClass('item')) {
                    var href = element.find('a').attr('href');
                    window.location = href;
                    return false;
                }
                break;
        }
    }
});
