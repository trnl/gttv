/* namespace.js */
/* 
 * jQuery Namespace 
 * Use namespaces to protect your global integrity.
 * Creates new object (namespace) within JQuery / JQuery.fn
 */
(function ($) { 
	var methods = {}; 

	/**
	 * This one defines namespace 
	 */
	$.namespace = function(ns) { 
    
		methods[ns] =  methods[ns] || {}; 
		
		// getting reference to a namespaced jquery object
		function nsfun(selector, context) {
			return $(selector, context).namespace(ns);
		}
    
		// adding methods to FN (using namespaces in jquery plugins)
		nsfun.fn = methods[ns];

		return nsfun;
	};
  
	/**
	 * Function that allows using namespaces in jQuery plugins
	 */
	$.fn.namespace = function(ns) { 
		if (methods[ns]) {
			$.extend(this, methods[ns]);
		}
		return this; 
	}; 
}) (jQuery);/* pluginize.js */
/* 
 * JQuery Pluginize
 * Use it for pluginize classes
 * It allows use classes (within some namespace) like plugins
 */ 
$.pluginize = function(name, object, namespace) {
	namespace.fn[name] = function(options) {
		// getting plugin arguments
		var args = Array.prototype.slice.call(arguments, 1);
		
		// returning this for saving chainability 
		return this.each(function() {
			var instance = $.data(this, name);

			// if incstance of our object is defined
			if (instance) {
				// applying arguments to options
				instance[options].apply(instance, args);
			} else {
				var o = $Crabapple.instantiate(object, [this, options]);
				// concrete adding plugin to $ using JQuery.data method
				instance = $.data(this, name, o);
			}
		});
	};
};/* cookie.js */
/**
 * jQuery Cookie plugin
 *
 * Copyright (c) 2010 Klaus Hartl (stilbuero.de)
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 */
jQuery.cookie = function (key, value, options) {

    // key and at least value given, set cookie...
    if (arguments.length > 1 && String(value) !== "[object Object]") {
        options = jQuery.extend({}, options);

        if (value === null || value === undefined) {
            options.expires = -1;
        }

        if (typeof options.expires === 'number') {
            var days = options.expires, t = options.expires = new Date();
            t.setDate(t.getDate() + days);
        }

        value = String(value);

        return (document.cookie = [
            encodeURIComponent(key), '=',
            options.raw ? value : encodeURIComponent(value),
            options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
            options.path ? '; path=' + options.path : '',
            options.domain ? '; domain=' + options.domain : '',
            options.secure ? '; secure' : ''
        ].join(''));
    }

    // key and possibly options given, get cookie...
    options = value || {};
    var result, decode = options.raw ? function (s) { return s; } : decodeURIComponent;
    return (result = new RegExp('(?:^|; )' + encodeURIComponent(key) + '=([^;]*)').exec(document.cookie)) ? decode(result[1]) : null;
};
/* jcarousel.js */
/*!
 * jCarousel - Riding carousels with jQuery
 *   http://sorgalla.com/jcarousel/
 *
 * Copyright (c) 2006 Jan Sorgalla (http://sorgalla.com)
 * Dual licensed under the MIT (http://www.opensource.org/licenses/mit-license.php)
 * and GPL (http://www.opensource.org/licenses/gpl-license.php) licenses.
 *
 * Built on top of the jQuery library
 *   http://jquery.com
 *
 * Inspired by the "Carousel Component" by Bill Scott
 *   http://billwscott.com/carousel/
 */

/*global window, jQuery */
(function($) {
    // Default configuration properties.
    var defaults = {
        vertical: false,
        rtl: false,
        start: 1,
        offset: 1,
        size: null,
        scroll: 3,
        visible: null,
        animation: 'normal',
        easing: 'swing',
        auto: 0,
        wrap: null,
        initCallback: null,
        setupCallback: null,
        reloadCallback: null,
        itemLoadCallback: null,
        itemFirstInCallback: null,
        itemFirstOutCallback: null,
        itemLastInCallback: null,
        itemLastOutCallback: null,
        itemVisibleInCallback: null,
        itemVisibleOutCallback: null,
        animationStepCallback: null,
        buttonNextHTML: '<div></div>',
        buttonPrevHTML: '<div></div>',
        buttonNextEvent: 'click',
        buttonPrevEvent: 'click',
        buttonNextCallback: null,
        buttonPrevCallback: null,
        itemFallbackDimension: null
    }, windowLoaded = false;

    $(window).bind('load.jcarousel', function() { windowLoaded = true; });

    /**
     * The jCarousel object.
     *
     * @constructor
     * @class jcarousel
     * @param e {HTMLElement} The element to create the carousel for.
     * @param o {Object} A set of key/value pairs to set as configuration properties.
     * @cat Plugins/jCarousel
     */
    $.jcarousel = function(e, o) {
        this.options    = $.extend({}, defaults, o || {});

        this.locked          = false;
        this.autoStopped     = false;

        this.container       = null;
        this.clip            = null;
        this.list            = null;
        this.buttonNext      = null;
        this.buttonPrev      = null;
        this.buttonNextState = null;
        this.buttonPrevState = null;

        // Only set if not explicitly passed as option
        if (!o || o.rtl === undefined) {
            this.options.rtl = ($(e).attr('dir') || $('html').attr('dir') || '').toLowerCase() == 'rtl';
        }

        this.wh = !this.options.vertical ? 'width' : 'height';
        this.lt = !this.options.vertical ? (this.options.rtl ? 'right' : 'left') : 'top';

        // Extract skin class
        var skin = '', split = e.className.split(' ');

        for (var i = 0; i < split.length; i++) {
            if (split[i].indexOf('jcarousel-skin') != -1) {
                $(e).removeClass(split[i]);
                skin = split[i];
                break;
            }
        }

        if (e.nodeName.toUpperCase() == 'UL' || e.nodeName.toUpperCase() == 'OL') {
            this.list      = $(e);
            this.clip      = this.list.parents('.jcarousel-clip');
            this.container = this.list.parents('.jcarousel-container');
        } else {
            this.container = $(e);
            this.list      = this.container.find('ul,ol').eq(0);
            this.clip      = this.container.find('.jcarousel-clip');
        }

        if (this.clip.size() === 0) {
            this.clip = this.list.wrap('<div></div>').parent();
        }

        if (this.container.size() === 0) {
            this.container = this.clip.wrap('<div></div>').parent();
        }

        if (skin !== '' && this.container.parent()[0].className.indexOf('jcarousel-skin') == -1) {
            this.container.wrap('<div class=" '+ skin + '"></div>');
        }

        this.buttonPrev = $('.jcarousel-prev', this.container);

        if (this.buttonPrev.size() === 0 && this.options.buttonPrevHTML !== null) {
            this.buttonPrev = $(this.options.buttonPrevHTML).appendTo(this.container);
        }

        this.buttonPrev.addClass(this.className('jcarousel-prev'));

        this.buttonNext = $('.jcarousel-next', this.container);

        if (this.buttonNext.size() === 0 && this.options.buttonNextHTML !== null) {
            this.buttonNext = $(this.options.buttonNextHTML).appendTo(this.container);
        }

        this.buttonNext.addClass(this.className('jcarousel-next'));

        this.clip.addClass(this.className('jcarousel-clip')).css({
            position: 'relative'
        });

        this.list.addClass(this.className('jcarousel-list')).css({
            overflow: 'hidden',
            position: 'relative',
            top: 0,
            margin: 0,
            padding: 0
        }).css((this.options.rtl ? 'right' : 'left'), 0);

        this.container.addClass(this.className('jcarousel-container')).css({
            position: 'relative'
        });

        if (!this.options.vertical && this.options.rtl) {
            this.container.addClass('jcarousel-direction-rtl').attr('dir', 'rtl');
        }

        var di = this.options.visible !== null ? Math.ceil(this.clipping() / this.options.visible) : null;
        var li = this.list.children('li');

        var self = this;

        if (li.size() > 0) {
            var wh = 0, j = this.options.offset;
            li.each(function() {
                self.format(this, j++);
                wh += self.dimension(this, di);
            });

            this.list.css(this.wh, (wh + 100) + 'px');

            // Only set if not explicitly passed as option
            if (!o || o.size === undefined) {
                this.options.size = li.size();
            }
        }

        // For whatever reason, .show() does not work in Safari...
        this.container.css('display', 'block');
        this.buttonNext.css('display', 'block');
        this.buttonPrev.css('display', 'block');

        this.funcNext   = function() { self.next(); };
        this.funcPrev   = function() { self.prev(); };
        this.funcResize = function() { 
            if (self.resizeTimer) {
                clearTimeout(self.resizeTimer);
            }

            self.resizeTimer = setTimeout(function() {
                self.reload();
            }, 100);
        };

        if (this.options.initCallback !== null) {
            this.options.initCallback(this, 'init');
        }

        if (!windowLoaded && $.browser.safari) {
            this.buttons(false, false);
            $(window).bind('load.jcarousel', function() { self.setup(); });
        } else {
            this.setup();
        }
    };

    // Create shortcut for internal use
    var $jc = $.jcarousel;

    $jc.fn = $jc.prototype = {
        jcarousel: '0.2.8'
    };

    $jc.fn.extend = $jc.extend = $.extend;

    $jc.fn.extend({
        /**
         * Setups the carousel.
         *
         * @method setup
         * @return undefined
         */
        setup: function() {
            this.first       = null;
            this.last        = null;
            this.prevFirst   = null;
            this.prevLast    = null;
            this.animating   = false;
            this.timer       = null;
            this.resizeTimer = null;
            this.tail        = null;
            this.inTail      = false;

            if (this.locked) {
                return;
            }

            this.list.css(this.lt, this.pos(this.options.offset) + 'px');
            var p = this.pos(this.options.start, true);
            this.prevFirst = this.prevLast = null;
            this.animate(p, false);

            $(window).unbind('resize.jcarousel', this.funcResize).bind('resize.jcarousel', this.funcResize);

            if (this.options.setupCallback !== null) {
                this.options.setupCallback(this);
            }
        },

        /**
         * Clears the list and resets the carousel.
         *
         * @method reset
         * @return undefined
         */
        reset: function() {
            this.list.empty();

            this.list.css(this.lt, '0px');
            this.list.css(this.wh, '10px');

            if (this.options.initCallback !== null) {
                this.options.initCallback(this, 'reset');
            }

            this.setup();
        },

        /**
         * Reloads the carousel and adjusts positions.
         *
         * @method reload
         * @return undefined
         */
        reload: function() {
            if (this.tail !== null && this.inTail) {
                this.list.css(this.lt, $jc.intval(this.list.css(this.lt)) + this.tail);
            }

            this.tail   = null;
            this.inTail = false;

            if (this.options.reloadCallback !== null) {
                this.options.reloadCallback(this);
            }

            if (this.options.visible !== null) {
                var self = this;
                var di = Math.ceil(this.clipping() / this.options.visible), wh = 0, lt = 0;
                this.list.children('li').each(function(i) {
                    wh += self.dimension(this, di);
                    if (i + 1 < self.first) {
                        lt = wh;
                    }
                });

                this.list.css(this.wh, wh + 'px');
                this.list.css(this.lt, -lt + 'px');
            }

            this.scroll(this.first, false);
        },

        /**
         * Locks the carousel.
         *
         * @method lock
         * @return undefined
         */
        lock: function() {
            this.locked = true;
            this.buttons();
        },

        /**
         * Unlocks the carousel.
         *
         * @method unlock
         * @return undefined
         */
        unlock: function() {
            this.locked = false;
            this.buttons();
        },

        /**
         * Sets the size of the carousel.
         *
         * @method size
         * @return undefined
         * @param s {Number} The size of the carousel.
         */
        size: function(s) {
            if (s !== undefined) {
                this.options.size = s;
                if (!this.locked) {
                    this.buttons();
                }
            }

            return this.options.size;
        },

        /**
         * Checks whether a list element exists for the given index (or index range).
         *
         * @method get
         * @return bool
         * @param i {Number} The index of the (first) element.
         * @param i2 {Number} The index of the last element.
         */
        has: function(i, i2) {
            if (i2 === undefined || !i2) {
                i2 = i;
            }

            if (this.options.size !== null && i2 > this.options.size) {
                i2 = this.options.size;
            }

            for (var j = i; j <= i2; j++) {
                var e = this.get(j);
                if (!e.length || e.hasClass('jcarousel-item-placeholder')) {
                    return false;
                }
            }

            return true;
        },

        /**
         * Returns a jQuery object with list element for the given index.
         *
         * @method get
         * @return jQuery
         * @param i {Number} The index of the element.
         */
        get: function(i) {
            return $('>.jcarousel-item-' + i, this.list);
        },

        /**
         * Adds an element for the given index to the list.
         * If the element already exists, it updates the inner html.
         * Returns the created element as jQuery object.
         *
         * @method add
         * @return jQuery
         * @param i {Number} The index of the element.
         * @param s {String} The innerHTML of the element.
         */
        add: function(i, s) {
            var e = this.get(i), old = 0, n = $(s);

            if (e.length === 0) {
                var c, j = $jc.intval(i);
                e = this.create(i);
                while (true) {
                    c = this.get(--j);
                    if (j <= 0 || c.length) {
                        if (j <= 0) {
                            this.list.prepend(e);
                        } else {
                            c.after(e);
                        }
                        break;
                    }
                }
            } else {
                old = this.dimension(e);
            }

            if (n.get(0).nodeName.toUpperCase() == 'LI') {
                e.replaceWith(n);
                e = n;
            } else {
                e.empty().append(s);
            }

            this.format(e.removeClass(this.className('jcarousel-item-placeholder')), i);

            var di = this.options.visible !== null ? Math.ceil(this.clipping() / this.options.visible) : null;
            var wh = this.dimension(e, di) - old;

            if (i > 0 && i < this.first) {
                this.list.css(this.lt, $jc.intval(this.list.css(this.lt)) - wh + 'px');
            }

            this.list.css(this.wh, $jc.intval(this.list.css(this.wh)) + wh + 'px');

            return e;
        },

        /**
         * Removes an element for the given index from the list.
         *
         * @method remove
         * @return undefined
         * @param i {Number} The index of the element.
         */
        remove: function(i) {
            var e = this.get(i);

            // Check if item exists and is not currently visible
            if (!e.length || (i >= this.first && i <= this.last)) {
                return;
            }

            var d = this.dimension(e);

            if (i < this.first) {
                this.list.css(this.lt, $jc.intval(this.list.css(this.lt)) + d + 'px');
            }

            e.remove();

            this.list.css(this.wh, $jc.intval(this.list.css(this.wh)) - d + 'px');
        },

        /**
         * Moves the carousel forwards.
         *
         * @method next
         * @return undefined
         */
        next: function() {
            if (this.tail !== null && !this.inTail) {
                this.scrollTail(false);
            } else {
                this.scroll(((this.options.wrap == 'both' || this.options.wrap == 'last') && this.options.size !== null && this.last == this.options.size) ? 1 : this.first + this.options.scroll);
            }
        },

        /**
         * Moves the carousel backwards.
         *
         * @method prev
         * @return undefined
         */
        prev: function() {
            if (this.tail !== null && this.inTail) {
                this.scrollTail(true);
            } else {
                this.scroll(((this.options.wrap == 'both' || this.options.wrap == 'first') && this.options.size !== null && this.first == 1) ? this.options.size : this.first - this.options.scroll);
            }
        },

        /**
         * Scrolls the tail of the carousel.
         *
         * @method scrollTail
         * @return undefined
         * @param b {Boolean} Whether scroll the tail back or forward.
         */
        scrollTail: function(b) {
            if (this.locked || this.animating || !this.tail) {
                return;
            }

            this.pauseAuto();

            var pos  = $jc.intval(this.list.css(this.lt));

            pos = !b ? pos - this.tail : pos + this.tail;
            this.inTail = !b;

            // Save for callbacks
            this.prevFirst = this.first;
            this.prevLast  = this.last;

            this.animate(pos);
        },

        /**
         * Scrolls the carousel to a certain position.
         *
         * @method scroll
         * @return undefined
         * @param i {Number} The index of the element to scoll to.
         * @param a {Boolean} Flag indicating whether to perform animation.
         */
        scroll: function(i, a) {
            if (this.locked || this.animating) {
                return;
            }

            this.pauseAuto();
            this.animate(this.pos(i), a);
        },

        /**
         * Prepares the carousel and return the position for a certian index.
         *
         * @method pos
         * @return {Number}
         * @param i {Number} The index of the element to scoll to.
         * @param fv {Boolean} Whether to force last item to be visible.
         */
        pos: function(i, fv) {
            var pos  = $jc.intval(this.list.css(this.lt));

            if (this.locked || this.animating) {
                return pos;
            }

            if (this.options.wrap != 'circular') {
                i = i < 1 ? 1 : (this.options.size && i > this.options.size ? this.options.size : i);
            }

            var back = this.first > i;

            // Create placeholders, new list width/height
            // and new list position
            var f = this.options.wrap != 'circular' && this.first <= 1 ? 1 : this.first;
            var c = back ? this.get(f) : this.get(this.last);
            var j = back ? f : f - 1;
            var e = null, l = 0, p = false, d = 0, g;

            while (back ? --j >= i : ++j < i) {
                e = this.get(j);
                p = !e.length;
                if (e.length === 0) {
                    e = this.create(j).addClass(this.className('jcarousel-item-placeholder'));
                    c[back ? 'before' : 'after' ](e);

                    if (this.first !== null && this.options.wrap == 'circular' && this.options.size !== null && (j <= 0 || j > this.options.size)) {
                        g = this.get(this.index(j));
                        if (g.length) {
                            e = this.add(j, g.clone(true));
                        }
                    }
                }

                c = e;
                d = this.dimension(e);

                if (p) {
                    l += d;
                }

                if (this.first !== null && (this.options.wrap == 'circular' || (j >= 1 && (this.options.size === null || j <= this.options.size)))) {
                    pos = back ? pos + d : pos - d;
                }
            }

            // Calculate visible items
            var clipping = this.clipping(), cache = [], visible = 0, v = 0;
            c = this.get(i - 1);
            j = i;

            while (++visible) {
                e = this.get(j);
                p = !e.length;
                if (e.length === 0) {
                    e = this.create(j).addClass(this.className('jcarousel-item-placeholder'));
                    // This should only happen on a next scroll
                    if (c.length === 0) {
                        this.list.prepend(e);
                    } else {
                        c[back ? 'before' : 'after' ](e);
                    }

                    if (this.first !== null && this.options.wrap == 'circular' && this.options.size !== null && (j <= 0 || j > this.options.size)) {
                        g = this.get(this.index(j));
                        if (g.length) {
                            e = this.add(j, g.clone(true));
                        }
                    }
                }

                c = e;
                d = this.dimension(e);
                if (d === 0) {
                    throw new Error('jCarousel: No width/height set for items. This will cause an infinite loop. Aborting...');
                }

                if (this.options.wrap != 'circular' && this.options.size !== null && j > this.options.size) {
                    cache.push(e);
                } else if (p) {
                    l += d;
                }

                v += d;

                if (v >= clipping) {
                    break;
                }

                j++;
            }

             // Remove out-of-range placeholders
            for (var x = 0; x < cache.length; x++) {
                cache[x].remove();
            }

            // Resize list
            if (l > 0) {
                this.list.css(this.wh, this.dimension(this.list) + l + 'px');

                if (back) {
                    pos -= l;
                    this.list.css(this.lt, $jc.intval(this.list.css(this.lt)) - l + 'px');
                }
            }

            // Calculate first and last item
            var last = i + visible - 1;
            if (this.options.wrap != 'circular' && this.options.size && last > this.options.size) {
                last = this.options.size;
            }

            if (j > last) {
                visible = 0;
                j = last;
                v = 0;
                while (++visible) {
                    e = this.get(j--);
                    if (!e.length) {
                        break;
                    }
                    v += this.dimension(e);
                    if (v >= clipping) {
                        break;
                    }
                }
            }

            var first = last - visible + 1;
            if (this.options.wrap != 'circular' && first < 1) {
                first = 1;
            }

            if (this.inTail && back) {
                pos += this.tail;
                this.inTail = false;
            }

            this.tail = null;
            if (this.options.wrap != 'circular' && last == this.options.size && (last - visible + 1) >= 1) {
                var m = $jc.intval(this.get(last).css(!this.options.vertical ? 'marginRight' : 'marginBottom'));
                if ((v - m) > clipping) {
                    this.tail = v - clipping - m;
                }
            }

            if (fv && i === this.options.size && this.tail) {
                pos -= this.tail;
                this.inTail = true;
            }

            // Adjust position
            while (i-- > first) {
                pos += this.dimension(this.get(i));
            }

            // Save visible item range
            this.prevFirst = this.first;
            this.prevLast  = this.last;
            this.first     = first;
            this.last      = last;

            return pos;
        },

        /**
         * Animates the carousel to a certain position.
         *
         * @method animate
         * @return undefined
         * @param p {Number} Position to scroll to.
         * @param a {Boolean} Flag indicating whether to perform animation.
         */
        animate: function(p, a) {
            if (this.locked || this.animating) {
                return;
            }

            this.animating = true;

            var self = this;
            var scrolled = function() {
                self.animating = false;

                if (p === 0) {
                    self.list.css(self.lt,  0);
                }

                if (!self.autoStopped && (self.options.wrap == 'circular' || self.options.wrap == 'both' || self.options.wrap == 'last' || self.options.size === null || self.last < self.options.size || (self.last == self.options.size && self.tail !== null && !self.inTail))) {
                    self.startAuto();
                }

                self.buttons();
                self.notify('onAfterAnimation');

                // This function removes items which are appended automatically for circulation.
                // This prevents the list from growing infinitely.
                if (self.options.wrap == 'circular' && self.options.size !== null) {
                    for (var i = self.prevFirst; i <= self.prevLast; i++) {
                        if (i !== null && !(i >= self.first && i <= self.last) && (i < 1 || i > self.options.size)) {
                            self.remove(i);
                        }
                    }
                }
            };

            this.notify('onBeforeAnimation');

            // Animate
            if (!this.options.animation || a === false) {
                this.list.css(this.lt, p + 'px');
                scrolled();
            } else {
                var o = !this.options.vertical ? (this.options.rtl ? {'right': p} : {'left': p}) : {'top': p};
                // Define animation settings.
                var settings = {
                    duration: this.options.animation,
                    easing:   this.options.easing,
                    complete: scrolled
                };
                // If we have a step callback, specify it as well.
                if ($.isFunction(this.options.animationStepCallback)) {
                    settings.step = this.options.animationStepCallback;
                }
                // Start the animation.
                this.list.animate(o, settings);
            }
        },

        /**
         * Starts autoscrolling.
         *
         * @method auto
         * @return undefined
         * @param s {Number} Seconds to periodically autoscroll the content.
         */
        startAuto: function(s) {
            if (s !== undefined) {
                this.options.auto = s;
            }

            if (this.options.auto === 0) {
                return this.stopAuto();
            }

            if (this.timer !== null) {
                return;
            }

            this.autoStopped = false;

            var self = this;
            this.timer = window.setTimeout(function() { self.next(); }, this.options.auto * 1000);
        },

        /**
         * Stops autoscrolling.
         *
         * @method stopAuto
         * @return undefined
         */
        stopAuto: function() {
            this.pauseAuto();
            this.autoStopped = true;
        },

        /**
         * Pauses autoscrolling.
         *
         * @method pauseAuto
         * @return undefined
         */
        pauseAuto: function() {
            if (this.timer === null) {
                return;
            }

            window.clearTimeout(this.timer);
            this.timer = null;
        },

        /**
         * Sets the states of the prev/next buttons.
         *
         * @method buttons
         * @return undefined
         */
        buttons: function(n, p) {
            if (n == null) {
                n = !this.locked && this.options.size !== 0 && ((this.options.wrap && this.options.wrap != 'first') || this.options.size === null || this.last < this.options.size);
                if (!this.locked && (!this.options.wrap || this.options.wrap == 'first') && this.options.size !== null && this.last >= this.options.size) {
                    n = this.tail !== null && !this.inTail;
                }
            }

            if (p == null) {
                p = !this.locked && this.options.size !== 0 && ((this.options.wrap && this.options.wrap != 'last') || this.first > 1);
                if (!this.locked && (!this.options.wrap || this.options.wrap == 'last') && this.options.size !== null && this.first == 1) {
                    p = this.tail !== null && this.inTail;
                }
            }

            var self = this;

            if (this.buttonNext.size() > 0) {
                this.buttonNext.unbind(this.options.buttonNextEvent + '.jcarousel', this.funcNext);

                if (n) {
                    this.buttonNext.bind(this.options.buttonNextEvent + '.jcarousel', this.funcNext);
                }

                this.buttonNext[n ? 'removeClass' : 'addClass'](this.className('jcarousel-next-disabled')).attr('disabled', n ? false : true);

                if (this.options.buttonNextCallback !== null && this.buttonNext.data('jcarouselstate') != n) {
                    this.buttonNext.each(function() { self.options.buttonNextCallback(self, this, n); }).data('jcarouselstate', n);
                }
            } else {
                if (this.options.buttonNextCallback !== null && this.buttonNextState != n) {
                    this.options.buttonNextCallback(self, null, n);
                }
            }

            if (this.buttonPrev.size() > 0) {
                this.buttonPrev.unbind(this.options.buttonPrevEvent + '.jcarousel', this.funcPrev);

                if (p) {
                    this.buttonPrev.bind(this.options.buttonPrevEvent + '.jcarousel', this.funcPrev);
                }

                this.buttonPrev[p ? 'removeClass' : 'addClass'](this.className('jcarousel-prev-disabled')).attr('disabled', p ? false : true);

                if (this.options.buttonPrevCallback !== null && this.buttonPrev.data('jcarouselstate') != p) {
                    this.buttonPrev.each(function() { self.options.buttonPrevCallback(self, this, p); }).data('jcarouselstate', p);
                }
            } else {
                if (this.options.buttonPrevCallback !== null && this.buttonPrevState != p) {
                    this.options.buttonPrevCallback(self, null, p);
                }
            }

            this.buttonNextState = n;
            this.buttonPrevState = p;
        },

        /**
         * Notify callback of a specified event.
         *
         * @method notify
         * @return undefined
         * @param evt {String} The event name
         */
        notify: function(evt) {
            var state = this.prevFirst === null ? 'init' : (this.prevFirst < this.first ? 'next' : 'prev');

            // Load items
            this.callback('itemLoadCallback', evt, state);

            if (this.prevFirst !== this.first) {
                this.callback('itemFirstInCallback', evt, state, this.first);
                this.callback('itemFirstOutCallback', evt, state, this.prevFirst);
            }

            if (this.prevLast !== this.last) {
                this.callback('itemLastInCallback', evt, state, this.last);
                this.callback('itemLastOutCallback', evt, state, this.prevLast);
            }

            this.callback('itemVisibleInCallback', evt, state, this.first, this.last, this.prevFirst, this.prevLast);
            this.callback('itemVisibleOutCallback', evt, state, this.prevFirst, this.prevLast, this.first, this.last);
        },

        callback: function(cb, evt, state, i1, i2, i3, i4) {
            if (this.options[cb] == null || (typeof this.options[cb] != 'object' && evt != 'onAfterAnimation')) {
                return;
            }

            var callback = typeof this.options[cb] == 'object' ? this.options[cb][evt] : this.options[cb];

            if (!$.isFunction(callback)) {
                return;
            }

            var self = this;

            if (i1 === undefined) {
                callback(self, state, evt);
            } else if (i2 === undefined) {
                this.get(i1).each(function() { callback(self, this, i1, state, evt); });
            } else {
                var call = function(i) {
                    self.get(i).each(function() { callback(self, this, i, state, evt); });
                };
                for (var i = i1; i <= i2; i++) {
                    if (i !== null && !(i >= i3 && i <= i4)) {
                        call(i);
                    }
                }
            }
        },

        create: function(i) {
            return this.format('<li></li>', i);
        },

        format: function(e, i) {
            e = $(e);
            var split = e.get(0).className.split(' ');
            for (var j = 0; j < split.length; j++) {
                if (split[j].indexOf('jcarousel-') != -1) {
                    e.removeClass(split[j]);
                }
            }
            e.addClass(this.className('jcarousel-item')).addClass(this.className('jcarousel-item-' + i)).css({
                'float': (this.options.rtl ? 'right' : 'left'),
                'list-style': 'none'
            }).attr('jcarouselindex', i);
            return e;
        },

        className: function(c) {
            return c + ' ' + c + (!this.options.vertical ? '-horizontal' : '-vertical');
        },

        dimension: function(e, d) {
            var el = $(e);

            if (d == null) {
                return !this.options.vertical ?
                       (el.outerWidth(true) || $jc.intval(this.options.itemFallbackDimension)) :
                       (el.outerHeight(true) || $jc.intval(this.options.itemFallbackDimension));
            } else {
                var w = !this.options.vertical ?
                    d - $jc.intval(el.css('marginLeft')) - $jc.intval(el.css('marginRight')) :
                    d - $jc.intval(el.css('marginTop')) - $jc.intval(el.css('marginBottom'));

                $(el).css(this.wh, w + 'px');

                return this.dimension(el);
            }
        },

        clipping: function() {
            return !this.options.vertical ?
                this.clip[0].offsetWidth - $jc.intval(this.clip.css('borderLeftWidth')) - $jc.intval(this.clip.css('borderRightWidth')) :
                this.clip[0].offsetHeight - $jc.intval(this.clip.css('borderTopWidth')) - $jc.intval(this.clip.css('borderBottomWidth'));
        },

        index: function(i, s) {
            if (s == null) {
                s = this.options.size;
            }

            return Math.round((((i-1) / s) - Math.floor((i-1) / s)) * s) + 1;
        }
    });

    $jc.extend({
        /**
         * Gets/Sets the global default configuration properties.
         *
         * @method defaults
         * @return {Object}
         * @param d {Object} A set of key/value pairs to set as configuration properties.
         */
        defaults: function(d) {
            return $.extend(defaults, d || {});
        },

        intval: function(v) {
            v = parseInt(v, 10);
            return isNaN(v) ? 0 : v;
        },

        windowLoaded: function() {
            windowLoaded = true;
        }
    });

    /**
     * Creates a carousel for all matched elements.
     *
     * @example $("#mycarousel").jcarousel();
     * @before <ul id="mycarousel" class="jcarousel-skin-name"><li>First item</li><li>Second item</li></ul>
     * @result
     *
     * <div class="jcarousel-skin-name">
     *   <div class="jcarousel-container">
     *     <div class="jcarousel-clip">
     *       <ul class="jcarousel-list">
     *         <li class="jcarousel-item-1">First item</li>
     *         <li class="jcarousel-item-2">Second item</li>
     *       </ul>
     *     </div>
     *     <div disabled="disabled" class="jcarousel-prev jcarousel-prev-disabled"></div>
     *     <div class="jcarousel-next"></div>
     *   </div>
     * </div>
     *
     * @method jcarousel
     * @return jQuery
     * @param o {Hash|String} A set of key/value pairs to set as configuration properties or a method name to call on a formerly created instance.
     */
    $.fn.jcarousel = function(o) {
        if (typeof o == 'string') {
            var instance = $(this).data('jcarousel'), args = Array.prototype.slice.call(arguments, 1);
            return instance[o].apply(instance, args);
        } else {
            return this.each(function() {
                var instance = $(this).data('jcarousel');
                if (instance) {
                    if (o) {
                        $.extend(instance.options, o);
                    }
                    instance.reload();
                } else {
                    $(this).data('jcarousel', new $jc(this, o));
                }
            });
        }
    };

})(jQuery);
/* crabapple.js */
/**
 * Creates $Crabapple global object which will hold all crabapple core javascript
 */
(function ($) {
	$Crabapple = $.namespace('Crabapple');
}) (jQuery);/* gt.js */
/**
 * Creates $Gt global object which will hold all Gt javascript stuff
 */
(function($) {
	$GT = $.namespace('GT'); 
}) (jQuery);/* class.js */
/**
 * Crabapple Class class is a base parent class for all classes
 * //TODO DOC all features of this class
 */
(function($){
	$Crabapple.Class = function(){};

	/**
	 * Type hierarchy class.
	 * 
	 * This class helps us navigate through type hierarchy.
	 * 
	 * @class
	 * @memberOf $Crabapple.Class
	 * @param type
	 */
	var Hierarchy = $Crabapple.Class.Hierarchy = function(/** Function */type){
		this.type = type;
	}
	/**
	 * @param type
	 * @returns Fluent interface
	 */
	Hierarchy.prototype.set = function(/** Function */type)/** Hierarchy */{
		this.type = type;
		return this;
	}
	/**
	 * @param cb callback will receive ancestor
	 * @returns Fluent interface
	 */
	Hierarchy.prototype.map = function(/** Function */cb)/** Hierarchy */{
		var stack = this.stack = [];
		for (var ancestor = this.type.prototype; 'parentClass' in ancestor; ancestor = ancestor.parentClass) {
			if (false !== cb(ancestor)) {
				stack.push(ancestor);
			}
		}
		return this;
	}
	/**
	 * @param cb callback will receive all matched ancestor and should return array with filtered ancestors 
	 * @returns Fluent interface
	 */
	Hierarchy.prototype.reduce = function(/** Function */cb)/** Hierarchy */{
		this.stack = cb(this.stack);
		return this;
	}
	/**
	 * @param cb callback will receive ancestor
	 * @returns Fluent interface
	 */
	Hierarchy.prototype.apply = function(/** Function */cb)/** Hierarchy */{
		var stack = this.stack;
		for (var i = stack.length - 1; i > -1; -- i) {
			cb(stack[i]);
		}
		return this;
	}

	/**
	 * Create a new Class that inherits from this class.
	 * 
	 * Merge 'options' and 'requires' properties.
	 * 
	 * @param parent parent class to extends
	 * @param child new child class
	 * @param [o] options to add to child's prototype
	 */
	$Crabapple.extend = function(/** Function */parent, /** Function */child, /** Object */o){
		o = o || {};
		var fn = function(){};

		fn.prototype = parent.prototype;
		child.prototype = new fn();
		child.prototype.parentClass = parent.prototype;
		child.prototype.constructor = child;

		for (var key in o) {
			child.prototype[key] = o[key];
		}

		var h = new Hierarchy(child).map(function(ancestor){
			return 'options' in ancestor;

		}).apply(function(ancestor){
			$.extend(child.prototype.options, ancestor.options);
		});
		
		h.set(child).map(function(ancestor){
			return 'requires' in ancestor;

		}).apply(function(ancestor){
			$.extend(child.prototype.requires, ancestor.requires);
		});
	};

	/**
	 * Instantiate new instance of specified type.
	 * 
	 * Calls init() methods step-by-step.
	 * 
	 * @param type type to instantiate
	 * @param args arguments for init() method
	 * @returns initialized new instance of type
	 */
	$Crabapple.instantiate = function(/** Function */type, /** Array */args)/** Object */{
		var o = new type();
		new Hierarchy(type).map(function(ancestor){
			return 'init' in ancestor;

		}).apply(function(ancestor){
			ancestor.init.apply(o, args);
		});
		return o;
	};

})(jQuery);/* module.js */
/**
 * Crabapple Module class is a base parent class for all Crabapple Modules
 */
(function($) {
	$Crabapple.Module = function(){};
	
	$Crabapple.extend($Crabapple.Class, $Crabapple.Module, {
		/**
		 * Default module class options
		 * 
		 * @type Object
		 */
		options: {},

		/**
		 * @type Array
		 */
		required: [],

		/**
		 * Initialize method. Called by chain.
		 * 
		 * @param options
		 * @param elem
		 */
		init: function(/** DOMElement */elem, /** Object */options){
			this.elem  = elem;
			this.$elem = $(elem);
			$.extend(this.options, options);
		}
	});
}) (jQuery);/* utils.js */
(function($) {
	/**
	 * @namespace Holds Crabapple utils methods
	 */
	$Crabapple.utils = {};
		
	$Crabapple.utils.DateTime = {};
	$Crabapple.utils.DateTime.currentDate = false;
	$Crabapple.utils.DateTime.format = function(format) {
		var returnStr = '';
		var replace = this.replaceChars;
		for (var i = 0; i < format.length; i++) {
			var curChar = format.charAt(i);
			if (i - 1 >= 0 && format.charAt(i - 1) == "\\") {
				returnStr += curChar;
			}
			else if (replace[curChar]) {
				returnStr += replace[curChar].call(this);
			} else if (curChar != "\\") {
				returnStr += curChar;
			}
		}

		return returnStr;
	};
	
	$Crabapple.utils.DateTime.replaceChars = {
		shortMonths: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
		longMonths: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
		shortDays: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
		longDays: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],

		// Day
		d: function() { return (this.currentDate.getDate() < 10 ? '0' : '') + this.currentDate.getDate(); },
		D: function() { return this.replaceChars.shortDays[this.currentDate.getDay()]; },
		j: function() { return this.currentDate.getDate(); },
		l: function() { return this.replaceChars.longDays[this.currentDate.getDay()]; },
		N: function() { return this.currentDate.getDay() + 1; },
		S: function() { return (this.currentDate.getDate() % 10 == 1 && this.currentDate.getDate() != 11 ? 'st' : (this.currentDate.getDate() % 10 == 2 && this.currentDate.getDate() != 12 ? 'nd' : (this.currentDate.getDate() % 10 == 3 && this.currentDate.getDate() != 13 ? 'rd' : 'th'))); },
		w: function() { return this.currentDate.getDay(); },
		z: function() { var d = new Date(this.currentDate.getFullYear(),0,1); return Math.ceil((this.currentDate - d) / 86400000); }, // Fixed now
		// Week
		W: function() { var d = new Date(this.currentDate.getFullYear(), 0, 1); return Math.ceil((((this.currentDate - d) / 86400000) + d.getDay() + 1) / 7); }, // Fixed now
		// Month
		F: function() { return this.replaceChars.longMonths[this.currentDate.getMonth()]; },
		m: function() { return (this.currentDate.getMonth() < 9 ? '0' : '') + (this.currentDate.getMonth() + 1); },
		M: function() { return this.replaceChars.shortMonths[this.currentDate.getMonth()]; },
		n: function() { return this.currentDate.getMonth() + 1; },
		t: function() { var d = new Date(); return new Date(d.getFullYear(), d.getMonth(), 0).getDate() }, // Fixed now, gets #days of date
		// Year
		L: function() { var year = this.currentDate.getFullYear(); return (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0)); },   // Fixed now
		o: function() { var d  = new Date(this.currentDate.valueOf());  d.setDate(d.getDate() - ((this.currentDate.getDay() + 6) % 7) + 3); return d.getFullYear();}, //Fixed now
		Y: function() { return this.currentDate.getFullYear(); },
		y: function() { return ('' + this.currentDate.getFullYear()).substr(2); },
		// Time
		a: function() { return this.currentDate.getHours() < 12 ? 'am' : 'pm'; },
		A: function() { return this.currentDate.getHours() < 12 ? 'AM' : 'PM'; },
		B: function() { return Math.floor((((this.currentDate.getUTCHours() + 1) % 24) + this.currentDate.getUTCMinutes() / 60 + this.currentDate.getUTCSeconds() / 3600) * 1000 / 24); }, // Fixed now
		g: function() { return this.currentDate.getHours() % 12 || 12; },
		G: function() { return this.currentDate.getHours(); },
		h: function() { return ((this.currentDate.getHours() % 12 || 12) < 10 ? '0' : '') + (this.currentDate.getHours() % 12 || 12); },
		H: function() { return (this.currentDate.getHours() < 10 ? '0' : '') + this.currentDate.getHours(); },
		i: function() { return (this.currentDate.getMinutes() < 10 ? '0' : '') + this.currentDate.getMinutes(); },
		s: function() { return (this.currentDate.getSeconds() < 10 ? '0' : '') + this.getSeconds(); },
		u: function() { var m = this.currentDate.getMilliseconds(); return (m < 10 ? '00' : (m < 100 ? '0' : '')) + m; },
		// Timezone
		e: function() { return "Not Yet Supported"; },
		I: function() { return "Not Yet Supported"; },
		O: function() { return (-this.currentDate.getTimezoneOffset() < 0 ? '-' : '+') + (Math.abs(this.currentDate.getTimezoneOffset() / 60) < 10 ? '0' : '') + (Math.abs(this.currentDate.getTimezoneOffset() / 60)) + '00'; },
		P: function() { return (-this.currentDate.getTimezoneOffset() < 0 ? '-' : '+') + (Math.abs(this.currentDate.getTimezoneOffset() / 60) < 10 ? '0' : '') + (Math.abs(this.currentDate.getTimezoneOffset() / 60)) + ':00'; }, // Fixed now
		T: function() { var m = this.currentDate.getMonth(); this.currentDate.setMonth(0); var result = this.currentDate.toTimeString().replace(/^.+ \(?([^\)]+)\)?$/, '$1'); this.currentDate.setMonth(m); return result;},
		Z: function() { return -this.currentDate.getTimezoneOffset() * 60; },
		// Full Date/Time
		c: function() { return this.format("Y-m-d\\TH:i:sP"); }, // Fixed now
		r: function() { return this.currentDate.toString(); },
		U: function() { return this.currentDate.getTime() / 1000; }
	};
	
	/**
	 * "2009-04-29 08:53:31" => relative time format
	 * "2009-04-29T08:53:31+0000" => relative time format
	 * object Date => relative time format
	 */
	$Crabapple.utils.DateTime.relativeTime = function(originalDate) {
		formatPattern = (arguments[1] && arguments[1].length > 0) ? arguments[1] : "F d, Y";
		if(originalDate instanceof Date){
			this.currentDate = originalDate;
		}else{
			currentDate = (originalDate || "").replace(/-/g,"/").replace(/TZ/g," ").replace(/\+(\w+)/g,"")
			this.currentDate  = new Date(currentDate);
		}
		var diff = (((new Date()).getTime() - this.currentDate.getTime()) / 1000);
		var day_diff = Math.floor(diff / 86400);
			
		if ( isNaN(day_diff) || day_diff < 0)
			return;
		
		return day_diff == 0 && (
				diff < 60 && "just now" ||
				diff < 120 && "1 minute ago" ||
				diff < 3600 && Math.floor( diff / 60 ) + " minutes ago" ||
				diff < 7200 && "1 hour ago" ||
				diff < 86400 && Math.floor( diff / 3600 ) + " hours ago") ||
			day_diff == 1 && "Yesterday" ||
			day_diff < 7 && day_diff + " days ago" ||
			day_diff < 30 && Math.ceil( day_diff / 7 ) + " weeks ago" ||
			day_diff < 365 && Math.floor( day_diff / 30 ) + " months ago" ||
			this.format(formatPattern);
	};
	
	
	/**
	 * replace links in text to their html compliance
	 */
	$Crabapple.utils.linkToHtml = function(elm) {
		var returning = [];
		var regexp = /((ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?)/gi;
		elm.each(function() {
			returning.push(this.replace(regexp,"<a href=\"$1\">$1</a>"));
		});

		return jQuery(returning);
	}
}) (jQuery);/* search.js */
/**
 * Search module
 * @class Search
 * @package CC
 */
(function($) {
	$Crabapple.Search = function(){};

	$Crabapple.extend($Crabapple.Module, $Crabapple.Search, {
		onMouseClick	: function() {},
		onElementFocus	: function() {},
		searchContainer	: null,
		localSearchBtn	: null,
		webSearchBtn	: null,
		
		options: {
			searchLabel: 'Search',
			searchContainerSelector: '',
			webSearchSelector: '',
			localSearchSelector: ''
		},
		/**
		 * init params and handlers
		 */
		init: function(elem, options)
		{
			this.searchContainer = $(this.options.searchContainerSelector);
			this.localSearchBtn = $(this.options.localSearchSelector, this.searchContainer);
			this.webSearchBtn = $(this.options.webSearchSelector, this.searchContainer);
			this.onElementActivities();
			this.onMouseClick();
			
		},
		/**
		 * add on click event handling 
		 */
		onMouseClick: function()
		{
			this.webSearchBtn.unbind().bind('click', $.proxy(function() {
				window.open(config.getSiteSearchEngine() + this.$elem.val().replace(/ /g,"+"));
				return false;
			}, this));
		},
		/**
		 * processing of serch elements behavior 
		 */
		onElementActivities: function()
		{
			this.$elem.parents('form:first').bind('submit', $.proxy(function(e) {
				if (this.$elem.val().length == 0 || ($.data(this.$elem).edited != true)) {
					this.$elem.val('');
					this.$elem.trigger('focus');
					return false;
				}
			}, this));

			this.$elem.unbind().bind('blur', $.proxy(function() {
				
				if (this.$elem.val().length == 0) {
					this.$elem.val(this.options.searchLabel);
					$.data(this.$elem, 'edited', false);
				}
				
			}, this)).bind('focus', $.proxy(function () {
				
				if ($.data(this.$elem).edited != true) {
					this.$elem.val('');
				}
				
			}, this)).bind('keyup', $.proxy(function(event) {

				$.data(this.$elem, 'edited', (this.$elem.val().length == 0) ? false : true);
				
			}, this));
			
			this.webSearchBtn.unbind().bind('click', $.proxy(function() {
				window.open(config.getSiteSearchEngine() + this.$elem.val().replace(/ /g,"+"));
				return false;
			}, this));
		}

	});
	$.pluginize('search', $Crabapple.Search, $Crabapple);
}) (jQuery);/* flux_widget.js */
(function($) {
	$Crabapple.FluxWidget = function () {};

	$Crabapple.extend($Crabapple.Module, $Crabapple.FluxWidget, {

		afterLoadWidgets: function (widgetObj, widget) {},

		/**
		 * flux widgets initialization
		 */
		init: function (widget, options) {
			widget = $(widget);

			if (typeof (getIndex) == 'undefined') {
				getIndex = this.getIndex();
			}

			if (typeof (MTVN) == 'undefined') {
				MTVN = {};
				MTVN.conf = MTVN.conf || {};
				MTVN.conf.flux4 = MTVN.conf.flux4 || {};
				MTVN.conf.flux4['ucid'] = config.getFluxCommunityId();
				MTVN.conf.flux4['widgets'] = {};
			}

			var widgetId = widget.data('widget') + '_' + getIndex();

			MTVN.conf.flux4['widgets'][widgetId] = {
				'name': this.getWidgetName(widget),
				'opts': this.getWidgetConfig(widget),
				'onLoad': $.proxy(function (widgetObj) {
					this.afterLoadWidgets(widgetObj, widget);
				}, this)
			};

			widget.data('widget', widgetId);

			widget.flux4();
		},

		/**
		 * function returned flux widget identefication
		 */
		getIndex: function () {
			var index = 0;
			return function () {
				return ++ index;
			}
		},

		/**
		 * return widget name
		 */
		getWidgetName: function (widget) {
			return widget.data('widget');
		},

		/**
		 * return widget options
		 */
		getWidgetConfig: function (widget) {
			var opts = {};

			// run method to get widget config by it type
			if(typeof this['get' + widget.data('widget') + 'Config'] == 'function') {
				opts = eval('this.get' + widget.data('widget') + 'Config')(widget);
			}
			return opts;
		},

		/**
		 * return activity feed configuration
		 */
		getActivityFeedConfig: function (widget) {
			return {};
		},

		/**
		 * get contentUri from dom element and return share button configuration
		 */
		getShareConfig: function (widget) {
			return {};
		},

		/**
		 * return user bar configuration
		 */
		getUserBarConfig: function (widget) {
			return {};
		}
	});

	/**
	 * Get content json feed by mgid
	 */
	$Crabapple.FluxWidget.requestContentFeed = function (mgid, func) {
		$.ajax({
			url: config.getFluxBaseHref() + '/2.0/00001/JSON/' + config.getFluxCommunityId() + '/feeds/content/?q=' + mgid,
			dataType: 'jsonp',
			success: func
		});
	}

	/**
	 * Get comments json feed by mgid
	 */
	$Crabapple.FluxWidget.requestCommentsFeed = function (mgid, maxResults, func) {
		var max = "";
		var mgidString;
		if(maxResults != null && !isNaN(maxResults = parseInt(maxResults))){
			max="?max-results="+maxResults;
			mgidString ="&q="+mgid;
		}else{
			mgidString ="?q="+mgid;
		}
		$.ajax({
			url: config.getFluxBaseHref() + '/2.0/00001/JSON/' + config.getFluxCommunityId() + '/feeds/comments/'+max+mgidString,
			dataType: 'jsonp',
			success: func
		});
	}

	/**
	 * Get comments on all GT comunity content
	 * 
	**/
	$Crabapple.FluxWidget.requestCommentsRiver = function (maxResults, func) {
		if(maxResults == null && isNaN(maxResults = parseInt(maxResults))){	maxResults="2";	}
		$.ajax({
			url: 'http://activity.flux.com/api/ActivityService/FindActivities?communityId=' + config.getFluxCommunityId() + '&contentId=&dashboardShowAllCommunities=false&dashboardShowCommunityFeed=false&dashboardShowOnlyFollowFeed=false&activityType=&activityFeedFilter=Comments&maxResults='+maxResults+'&cultureLcid=0&nextPageToken=&sortByTopCounter=false&untilPageToken=&includeChildren=false&includeTweets=true&includeGuestActivities=false&includeRating=true&showParentAddedChild=false&contentAliases=&popularActivitiesCount=0&popularActivitiesType=CommentContent',
			dataType: 'jsonp',
			success: func
		});
	}

	/**
	 * Get following statistics of an content
	 **/
	$Crabapple.FluxWidget.requestFollowingStats = function(mgid, func){
		$.ajax({
			url: config.getFluxBaseHref() + '/2.0/00001/JSON/' + config.getFluxCommunityId() + "/feeds/FollowingStats/?q="+mgid,
			dataType: 'jsonp',
			success: func
		});
	}

	/**
	 * Performs item tracking
	 **/
	$Crabapple.FluxWidget.requestItemTracking = function(mgid, func){
		$.ajax({
			url: config.getFluxBaseHref() + '/2.0/00001/JSON/' + config.getFluxCommunityId() + '/Action/Follow?product=API&productLocation=brand',
			type: 'POST',
			parameters: {"contentUri":mgid,"FollowingService":"flux"},
			crossDomain: true,
			success: func
		});
	}

	/**
	 * Get information about currently logged user and comunity.
	 **/
	$Crabapple.FluxWidget.requestUserCoumunityInfo = function(func){
		window.getUserComunityInfo = function(context){
			func(context);
		}
		$.getScript("http://cus.flux.com/api/ContextService/Context?communityUcid="+config.getFluxCommunityId()+"&callback=getUserComunityInfo");
	}
	
}) (jQuery);/* photo_gallery.js */
(function($) {
	$Crabapple.photoGallery = function () {};

	$Crabapple.extend($Crabapple.Module, $Crabapple.photoGallery, {
		options: {
			hideButtonSelector: '',
			containerSelector: null,
			headlineSelector: null,
			viewThumbsSelector: null,
			carouselSelector: null,
			carouselOptions: {}
		},
		
		isCarouselCreated: false,

		init: function(elem, options) {
			$(elem).click(jQuery.proxy(function(){
				this.setShowButtonCatcher();
			}, this));

			$(this.options.hideButtonSelector).click(jQuery.proxy(function(){
				this.setHideButtonCatcher();
			}, this));
			
		},
		
		setShowButtonCatcher: function() {
			$(this.options.viewThumbsSelector).hide();
			$(this.options.hideButtonSelector).show();
			$(this.options.containerSelector).show(0, jQuery.proxy(function(){ this.pushDown()}, this) );
		},
		
		setHideButtonCatcher: function() {
			$(this.options.hideButtonSelector).hide();
			$(this.options.viewThumbsSelector).show();
			$(this.options.containerSelector).hide(0, jQuery.proxy(function(){ this.pushUp()}, this));
		},

		pushDown: function() {
			
			if(this.options.carouselSelector && !this.isCarouselCreated ) {
				$Crabapple(this.options.carouselSelector).carousel(this.options.carouselOptions);
				this.isCarouselCreated = true;
			}
			
			$(this.options.headlineSelector).css('padding-top', $(this.options.containerSelector).height())
		},
		
		pushUp: function() {
			$(this.options.headlineSelector).css('padding-top', '');
		}

	});
}) (jQuery);/* player.js */
(function($) {
	$Crabapple.Player = function () {};

	$Crabapple.extend($Crabapple.Module, $Crabapple.Player, {

		player: new Array(),				
		
	

		init: function (elm, options) {
			//this.onReady = this.options.onReady;

			//********************
			var autoPlay = $(elm).attr('data-autoplay');
			var configParams = escape("site=" + config.getMediaConfigParamSite());
			if (config.getMediaFreewheelNID()) {
				configParams += escape("&nid=" + config.getMediaFreewheelNID());	
			} 
			if (typeof(siteSectionId) == 'undefined') { siteSectionId = 'The_Daily_Show_Home'; }
		
			var flashvars = { sid:siteSectionId, autoPlay:autoPlay, configParams:configParams };
		
			var parobj = {
				wmode:'opaque',
				bgcolor: $(elm).attr('data-bgcolor'),
				seamlesstabbing:true,
				swliveconnect:true,
				allowscriptaccess:'always',
				allownetworking:'all',
				allowfullscreen:true
			};
			
			var mgid = $(elm).attr('data-mgid');
			var width = $(elm).attr('data-width');
			var height = $(elm).attr('data-height');
			
			$Crabapple.playerA = this;

			this.player[$(elm).attr('id')] = new MTVNPlayer.Player($(elm).attr('id'),
				{
					width:width,
					height:height,
					uri:mgid,
					flashVars:flashvars,
					params:parobj
				},
				{
					onReady:this.onReady,
					onMetadata:this.onMetadata,
					onMediaEnd:this.onMediaEnd,
					onPlayheadUpdate:this.onPlayheadUpdate,
					onStateChange:this.onStateChange,
					onPlaylistComplete:this.onPlaylistComplete
				}		
				

			);
						
			return this.player;
				
			//**********************			
			
		},
	
		onReady:function(event){},				
		onMetadata:function(event){},		
		onMediaEnd:function(event){},		
		onPlayheadUpdate:function(event){},	
		onStateChange:function(event){},
		onPlaylistComplete:function(event){}
	});

	$.pluginize('player', $Crabapple.Player, $Crabapple);

}) (jQuery);/* pagination.js */
/**
 * Pagination module
 * @class Pagination
 * @package CC
 * @author shcherba
 */
(function($){
	$Crabapple.Pagination = function(){
	};
	
	$Crabapple.extend($Crabapple.Module, $Crabapple.Pagination, {
		options: {
			context: null,
			reloadArea: null,
			paginationContainer: null,
			onClick: function(){
			},
			pequestParams: {},
			afterLoadHandler: null,
			afterLoadHandlerContext: null,
			fragmentLlink: '',
			paginationHolder: '.pagination_wrap',
			spinnerTemplate: "<div align='center'><img src='/sitewide/images/modules/ajax_busy.gif' width='32' height='32' alt='' border='0' /></div>",
			scrollToTop: false
		},
		/**
		 * init params and handlers
		 */
		init: function(elm, options){
			this._options = options ? options : this._options;
			this.options.context = (options.context) ? options.context : this;
			this.options.onClick = (options.onClick) ? options.onClick : this.ajaxPageReload;
			$.extend(this.options, options);
			this.initActions(elm);
		},
		/**
		 * @param {Object} block
		 */
		initActions: function(block){
			var self = this;
			$(block).undelegate('click');
			$(block).delegate('a', 'click', function(e){
				var page = $(e.target).attr("href").match(/currentPage=([0-9]+)/)[1];
				self.options.onClick.apply(self.options.context, [page ? page : '']);
				return false;
			});
		},
		/**
		 * @param {Object} data
		 */
		ajaxReloadActions: function(data){
			$Crabapple(this.elem).remove();
			$(this.options.reloadArea).empty();
			$(this.options.reloadArea).html(data);
			var elem = $($(this.options.paginationHolder), this.options.reloadArea)[0];
			this._reload(elem);
			
			if (this.options.afterLoadHandler) {
				delete this.options.pequestParams.currentPage;
				this.options.afterLoadHandler.call(this.options.afterLoadHandlerContext, this.options.pequestParams);
			}
		},
		/**
		 * @param {init} page
		 */
		ajaxPageReload: function(page){
			//add page param into request
			this.options.pequestParams.currentPage = page;
			//show spinner
			$(this.options.reloadArea).html(this.options.spinnerTemplate);
			//scroll to Top if needed
			if (this.options.scrollToTop == true){
				$(document).scrollTop(0);
			}
			$.ajax({
				url: this.options.fragmentLlink,
				type: 'GET',
				data: this.options.pequestParams,
				context: this,
				success: function(data){
					this.ajaxReloadActions(data);
				},
				error: function(data){
					//this.ajaxReloadActions(data);
				}
			});
		},
		
		_reload: function(newElem, options){
			this.elem = this.$elem = null;
			$.extend(this._options, options);
			this.init(newElem, this._options);
		}
	});
	
	$.pluginize('pagination', $Crabapple.Pagination, $Crabapple);
	
})(jQuery);
/* gen_ads_reporting.js */
/*
function sendLinkEvent(linkEventText) {
	//s.linkTrackVars='prop20,eVar20';
	//s.prop20=linkEventText;
	//s.eVar20=linkEventText;
	eval("var linkCall = {linkName:'"+linkEventText+"', linkType:'o'}");
		
	mtvn.btg.Controller.sendLinkEvent(linkCall);
}

function sendPromoLinkEvent(promoID) {
	sendLinkEvent(pageName+'/Promo_Button_'+promoID);
}

function siteSearchReport(){
	mtvn.btg.reporting.Search.setConversion();
}
*//* overlay.js */
/**
 * @license 
 * jQuery Tools @VERSION Overlay - Overlay base. Extend it.
 * 
 * NO COPYRIGHTS OR LICENSES. DO WHAT YOU LIKE.
 * 
 * http://flowplayer.org/tools/overlay/
 *
 * Since: March 2008
 * Date: @DATE 
 */
(function($) { 

	// static constructs
	$.tools = $.tools || {version: '@VERSION'};
	
	$.tools.overlay = {
		
		addEffect: function(name, loadFn, closeFn) {
			effects[name] = [loadFn, closeFn];	
		},
	
		conf: {  
			close: null,	
			closeOnClick: true,
			closeOnEsc: true,			
			closeSpeed: 'fast',
			effect: 'default',
			
			// since 1.2. fixed positioning not supported by IE6
			fixed: !$.browser.msie || $.browser.version > 6, 
			
			left: 'center',		
			load: false, // 1.2
			mask: null,  
			oneInstance: true,
			speed: 'normal',
			target: null, // target element to be overlayed. by default taken from [rel]
			top: '10%'
		}
	};

	
	var instances = [], effects = {};
		
	// the default effect. nice and easy!
	$.tools.overlay.addEffect('default', 
		
		/* 
			onLoad/onClose functions must be called otherwise none of the 
			user supplied callback methods won't be called
		*/
		function(pos, onLoad) {
			
			var conf = this.getConf(),
				 w = $(window);				 
				
			if (!conf.fixed)  {
				pos.top += w.scrollTop();
				pos.left += w.scrollLeft();
			} 
				
			pos.position = conf.fixed ? 'fixed' : 'absolute';
			this.getOverlay().css(pos).fadeIn(conf.speed, onLoad); 
			
		}, function(onClose) {
			this.getOverlay().fadeOut(this.getConf().closeSpeed, onClose); 			
		}		
	);		

	
	function Overlay(trigger, conf) {		
		
		// private variables
		var self = this,
			 fire = trigger.add(self),
			 w = $(window), 
			 closers,            
			 overlay,
			 opened,
			 maskConf = $.tools.expose && (conf.mask || conf.expose),
			 uid = Math.random().toString().slice(10);		
		
			 
		// mask configuration
		if (maskConf) {			
			if (typeof maskConf == 'string') { maskConf = {color: maskConf}; }
			maskConf.closeOnClick = maskConf.closeOnEsc = false;
		}			 
		 
		// get overlay and triggerr
		var jq = conf.target || trigger.attr("rel");
		overlay = jq ? $(jq) : null || trigger;	
		
		// overlay not found. cannot continue
		if (!overlay.length) { throw "Could not find Overlay: " + jq; }
		
		// trigger's click event
		if (trigger && trigger.index(overlay) == -1) {
			trigger.click(function(e) {				
				self.load(e);
				return e.preventDefault();
			});
		}   			
		
		// API methods  
		$.extend(self, {

			load: function(e) {
				
				// can be opened only once
				if (self.isOpened()) { return self; }
				
				// find the effect
		 		var eff = effects[conf.effect];
		 		if (!eff) { throw "Overlay: cannot find effect : \"" + conf.effect + "\""; }
				
				// close other instances?
				if (conf.oneInstance) {
					$.each(instances, function() {
						this.close(e);
					});
				}
				
				// onBeforeLoad
				e = e || $.Event();
				e.type = "onBeforeLoad";
				fire.trigger(e);				
				if (e.isDefaultPrevented()) { return self; }				

				// opened
				opened = true;
				
				// possible mask effect
				if (maskConf) { $(overlay).expose(maskConf); }				
				
				// position & dimensions 
				var top = conf.top,					
					 left = conf.left,
					 oWidth = overlay.outerWidth({margin:true}),
					 oHeight = overlay.outerHeight({margin:true}); 
				
				if (typeof top == 'string')  {
					top = top == 'center' ? Math.max((w.height() - oHeight) / 2, 0) : 
						parseInt(top, 10) / 100 * w.height();			
				}				
				
				if (left == 'center') { left = Math.max((w.width() - oWidth) / 2, 0); }

				
		 		// load effect  		 		
				eff[0].call(self, {top: top, left: left}, function() {					
					if (opened) {
						e.type = "onLoad";
						fire.trigger(e);
					}
				}); 				

				// mask.click closes overlay
				if (maskConf && conf.closeOnClick) {
					$.mask.getMask().one("click", self.close); 
				}
				
				// when window is clicked outside overlay, we close
				if (conf.closeOnClick) {
					$(document).bind("click." + uid, function(e) { 
						if (!$(e.target).parents(overlay).length) { 
							self.close(e); 
						}
					});						
				}						
			
				// keyboard::escape
				if (conf.closeOnEsc) { 

					// one callback is enough if multiple instances are loaded simultaneously
					$(document).bind("keydown." + uid, function(e) {
						if (e.keyCode == 27) { 
							self.close(e);	 
						}
					});			
				}

				
				return self; 
			}, 
			
			close: function(e) {

				if (!self.isOpened()) { return self; }
				
				e = e || $.Event();
				e.type = "onBeforeClose";
				fire.trigger(e);				
				if (e.isDefaultPrevented()) { return; }				
				
				opened = false;
				
				// close effect
				effects[conf.effect][1].call(self, function() {
					e.type = "onClose";
					fire.trigger(e); 
				});
				
				// unbind the keyboard / clicking actions
				$(document).unbind("click." + uid).unbind("keydown." + uid);		  
				
				if (maskConf) {
					$.mask.close();		
				}
				 
				return self;
			}, 
			
			getOverlay: function() {
				return overlay;	
			},
			
			getTrigger: function() {
				return trigger;	
			},
			
			getClosers: function() {
				return closers;	
			},			

			isOpened: function()  {
				return opened;
			},
			
			// manipulate start, finish and speeds
			getConf: function() {
				return conf;	
			}			
			
		});
		
		// callbacks	
		$.each("onBeforeLoad,onStart,onLoad,onBeforeClose,onClose".split(","), function(i, name) {
				
			// configuration
			if ($.isFunction(conf[name])) { 
				$(self).bind(name, conf[name]); 
			}

			// API
			self[name] = function(fn) {
				if (fn) { $(self).bind(name, fn); }
				return self;
			};
		});
		
		// close button
		closers = overlay.find(conf.close || ".close");		
		
		if (!closers.length && !conf.close) {
			closers = $('<a class="close"></a>');
			overlay.prepend(closers);	
		}		
		
		closers.click(function(e) { 
			self.close(e);  
		});	
		
		// autoload
		if (conf.load) { self.load(); }
		
	}
	
	// jQuery plugin initialization
	$.fn.overlay = function(conf) {   
		
		// already constructed --> return API
		var el = this.data("overlay");
		if (el) { return el; }	  		 
		
		if ($.isFunction(conf)) {
			conf = {onBeforeLoad: conf};	
		}

		conf = $.extend(true, {}, $.tools.overlay.conf, conf);
		
		this.each(function() {		
			el = new Overlay($(this), conf);
			instances.push(el);
			$(this).data("overlay", el);	
		});
		
		return conf.api ? el: this;		
	}; 
	
})(jQuery);

/* scrollable.js */
/**
 * @license 
 * jQuery Tools @VERSION Scrollable - New wave UI design
 * 
 * NO COPYRIGHTS OR LICENSES. DO WHAT YOU LIKE.
 * 
 * http://flowplayer.org/tools/scrollable.html
 *
 * Since: March 2008
 * Date: @DATE 
 */
(function($) { 

	// static constructs
	$.tools = $.tools || {version: '@VERSION'};
	
	$.tools.scrollable = {
		
		conf: {	
			activeClass: 'active',
			circular: false,
			clonedClass: 'cloned',
			disabledClass: 'disabled',
			easing: 'swing',
			initialIndex: 0,
			item: null,
			items: '.items',
			keyboard: true,
			mousewheel: false,
			next: '.next',   
			prev: '.prev', 
			speed: 400,
			vertical: false,
			touch: true,
			wheelSpeed: 0
		} 
	};
					
	// get hidden element's width or height even though it's hidden
	function dim(el, key) {
		var v = parseInt(el.css(key), 10);
		if (v) { return v; }
		var s = el[0].currentStyle; 
		return s && s.width && parseInt(s.width, 10);	
	}

	function find(root, query) { 
		var el = $(query);
		return el.length < 2 ? el : root.parent().find(query);
	}
	
	var current;		
	
	// constructor
	function Scrollable(root, conf) {   
		
		// current instance
		var self = this, 
			 fire = root.add(self),
			 itemWrap = root.children(),
			 index = 0,
			 vertical = conf.vertical;
				
		if (!current) { current = self; } 
		if (itemWrap.length > 1) { itemWrap = $(conf.items, root); }
		
		// methods
		$.extend(self, {
				
			getConf: function() {
				return conf;	
			},			
			
			getIndex: function() {
				return index;	
			}, 

			getSize: function() {
				return self.getItems().size();	
			},

			getNaviButtons: function() {
				return prev.add(next);	
			},
			
			getRoot: function() {
				return root;	
			},
			
			getItemWrap: function() {
				return itemWrap;	
			},
			
			getItems: function() {
				return itemWrap.children(conf.item).not("." + conf.clonedClass);	
			},
							
			move: function(offset, time) {
				return self.seekTo(index + offset, time);
			},
			
			next: function(time) {
				return self.move(1, time);	
			},
			
			prev: function(time) {
				return self.move(-1, time);	
			},
			
			begin: function(time) {
				return self.seekTo(0, time);	
			},
			
			end: function(time) {
				return self.seekTo(self.getSize() -1, time);	
			},	
			
			focus: function() {
				current = self;
				return self;
			},
			
			addItem: function(item) {
				item = $(item);
				
				if (!conf.circular)  {
					itemWrap.append(item);
				} else {
					itemWrap.children("." + conf.clonedClass + ":last").before(item);
					itemWrap.children("." + conf.clonedClass + ":first").replaceWith(item.clone().addClass(conf.clonedClass)); 						
				}
				
				fire.trigger("onAddItem", [item]);
				return self;
			},
			
			
			/* all seeking functions depend on this */		
			seekTo: function(i, time, fn) {	
				
				// ensure numeric index
				if (!i.jquery) { i *= 1; }
				
				// avoid seeking from end clone to the beginning
				if (conf.circular && i === 0 && index == -1 && time !== 0) { return self; }
				
				// check that index is sane				
				if (!conf.circular && i < 0 || i > self.getSize() || i < -1) { return self; }
				
				var item = i;
			
				if (i.jquery) {
					i = self.getItems().index(i);	
					
				} else {
					item = self.getItems().eq(i);
				}  
				
				// onBeforeSeek
				var e = $.Event("onBeforeSeek"); 
				if (!fn) {
					fire.trigger(e, [i, time]);				
					if (e.isDefaultPrevented() || !item.length) { return self; }			
				}  
	
				var props = vertical ? {top: -item.position().top} : {left: -item.position().left};  
				
				index = i;
				current = self;  
				if (time === undefined) { time = conf.speed; }   
				
				itemWrap.animate(props, time, conf.easing, fn || function() { 
					fire.trigger("onSeek", [i]);		
				});	 
				
				return self; 
			}					
			
		});
				
		// callbacks	
		$.each(['onBeforeSeek', 'onSeek', 'onAddItem'], function(i, name) {
				
			// configuration
			if ($.isFunction(conf[name])) { 
				$(self).bind(name, conf[name]); 
			}
			
			self[name] = function(fn) {
				if (fn) { $(self).bind(name, fn); }
				return self;
			};
		});  
		
		// circular loop
		if (conf.circular) {
			
			var cloned1 = self.getItems().slice(-1).clone().prependTo(itemWrap),
				 cloned2 = self.getItems().eq(1).clone().appendTo(itemWrap);
				
			cloned1.add(cloned2).addClass(conf.clonedClass);
			
			self.onBeforeSeek(function(e, i, time) {

				
				if (e.isDefaultPrevented()) { return; }
				
				/*
					1. animate to the clone without event triggering
					2. seek to correct position with 0 speed
				*/
				if (i == -1) {
					self.seekTo(cloned1, time, function()  {
						self.end(0);		
					});          
					return e.preventDefault();
					
				} else if (i == self.getSize()) {
					self.seekTo(cloned2, time, function()  {
						self.begin(0);		
					});	
				}
				
			});
			
			// seek over the cloned item
			self.seekTo(0, 0, function() {});
		}
		
		// next/prev buttons
		var prev = find(root, conf.prev).click(function() { self.prev(); }),
			 next = find(root, conf.next).click(function() { self.next(); });	
		
		if (!conf.circular && self.getSize() > 1) {
			
			self.onBeforeSeek(function(e, i) {
				setTimeout(function() {
					if (!e.isDefaultPrevented()) {
						prev.toggleClass(conf.disabledClass, i <= 0);
						next.toggleClass(conf.disabledClass, i >= self.getSize() -1);
					}
				}, 1);
			}); 
			
			if (!conf.initialIndex) {
				prev.addClass(conf.disabledClass);	
			}
		}
			
		// mousewheel support
		if (conf.mousewheel && $.fn.mousewheel) {
			root.mousewheel(function(e, delta)  {
				if (conf.mousewheel) {
					self.move(delta < 0 ? 1 : -1, conf.wheelSpeed || 50);
					return false;
				}
			});			
		}
		
		// touch event
		if (conf.touch) {
			var touch = {};
			
			itemWrap[0].ontouchstart = function(e) {
				var t = e.touches[0];
				touch.x = t.clientX;
				touch.y = t.clientY;
			};
			
			itemWrap[0].ontouchmove = function(e) {
				
				// only deal with one finger
				if (e.touches.length == 1 && !itemWrap.is(":animated")) {			
					var t = e.touches[0],
						 deltaX = touch.x - t.clientX,
						 deltaY = touch.y - t.clientY;
	
					self[vertical && deltaY > 0 || !vertical && deltaX > 0 ? 'next' : 'prev']();				
					e.preventDefault();
				}
			};
		}
		
		if (conf.keyboard)  {
			
			$(document).bind("keydown.scrollable", function(evt) {

				// skip certain conditions
				if (!conf.keyboard || evt.altKey || evt.ctrlKey || $(evt.target).is(":input")) { return; }
				
				// does this instance have focus?
				if (conf.keyboard != 'static' && current != self) { return; }
					
				var key = evt.keyCode;
			
				if (vertical && (key == 38 || key == 40)) {
					self.move(key == 38 ? -1 : 1);
					return evt.preventDefault();
				}
				
				if (!vertical && (key == 37 || key == 39)) {					
					self.move(key == 37 ? -1 : 1);
					return evt.preventDefault();
				}	  
				
			});  
		}
		
		// initial index
		if (conf.initialIndex) {
			self.seekTo(conf.initialIndex, 0, function() {});
		}
	} 

		
	// jQuery plugin implementation
	$.fn.scrollable = function(conf) { 
			
		// already constructed --> return API
		var el = this.data("scrollable");
		if (el) { return el; }		 

		conf = $.extend({}, $.tools.scrollable.conf, conf); 
		
		this.each(function() {			
			el = new Scrollable($(this), conf);
			$(this).data("scrollable", el);	
		});
		
		return conf.api ? el: this; 
		
	};
			
	
})(jQuery);
/* tabs.js */
/**
 * @license 
 * jQuery Tools @VERSION Tabs- The basics of UI design.
 * 
 * NO COPYRIGHTS OR LICENSES. DO WHAT YOU LIKE.
 * 
 * http://flowplayer.org/tools/tabs/
 *
 * Since: November 2008
 * Date: @DATE 
 */  
(function($) {
		
	// static constructs
	$.tools = $.tools || {version: '@VERSION'};
	
	$.tools.tabs = {
		
		conf: {
			tabs: 'a',
			current: 'current',
			onBeforeClick: null,
			onClick: null, 
			effect: 'default',
			initialIndex: 0,			
			event: 'click',
			rotate: false,
			
			// 1.2
			history: false
		},
		
		addEffect: function(name, fn) {
			effects[name] = fn;
		}
		
	};
	
	var effects = {
		
		// simple "toggle" effect
		'default': function(i, done) { 
			this.getPanes().hide().eq(i).show();
			done.call();
		}, 
		
		/*
			configuration:
				- fadeOutSpeed (positive value does "crossfading")
				- fadeInSpeed
		*/
		fade: function(i, done) {		
			
			var conf = this.getConf(),            
				 speed = conf.fadeOutSpeed,
				 panes = this.getPanes();
			
			if (speed) {
				panes.fadeOut(speed);	
			} else {
				panes.hide();	
			}

			panes.eq(i).fadeIn(conf.fadeInSpeed, done);	
		},
		
		// for basic accordions
		slide: function(i, done) {
			this.getPanes().slideUp(200);
			this.getPanes().eq(i).slideDown(400, done);			 
		}, 

		/**
		 * AJAX effect
		 */
		ajax: function(i, done)  {			
			this.getPanes().eq(0).load(this.getTabs().eq(i).attr("href"), done);	
		}		
	};   	
	
	var w;
	
	/**
	 * Horizontal accordion
	 * 
	 * @deprecated will be replaced with a more robust implementation
	 */
	$.tools.tabs.addEffect("horizontal", function(i, done) {
	
		// store original width of a pane into memory
		if (!w) { w = this.getPanes().eq(0).width(); }
		
		// set current pane's width to zero
		this.getCurrentPane().animate({width: 0}, function() { $(this).hide(); });
		
		// grow opened pane to it's original width
		this.getPanes().eq(i).animate({width: w}, function() { 
			$(this).show();
			done.call();
		});
		
	});	

	
	function Tabs(root, paneSelector, conf) {
		
		var self = this, 
			 trigger = root.add(this),
			 tabs = root.find(conf.tabs),
			 panes = paneSelector.jquery ? paneSelector : root.children(paneSelector),			 
			 current;
			 
		
		// make sure tabs and panes are found
		if (!tabs.length)  { tabs = root.children(); }
		if (!panes.length) { panes = root.parent().find(paneSelector); }
		if (!panes.length) { panes = $(paneSelector); }
		
		
		// public methods
		$.extend(this, {				
			click: function(i, e) {
				
				var tab = tabs.eq(i);												 
				
				if (typeof i == 'string' && i.replace("#", "")) {
					tab = tabs.filter("[href*=" + i.replace("#", "") + "]");
					i = Math.max(tabs.index(tab), 0);
				}
								
				if (conf.rotate) {
					var last = tabs.length -1; 
					if (i < 0) { return self.click(last, e); }
					if (i > last) { return self.click(0, e); }						
				}
				
				if (!tab.length) {
					if (current >= 0) { return self; }
					i = conf.initialIndex;
					tab = tabs.eq(i);
				}				
				
				// current tab is being clicked
				if (i === current) { return self; }
				
				// possibility to cancel click action				
				e = e || $.Event();
				e.type = "onBeforeClick";
				trigger.trigger(e, [i]);				
				if (e.isDefaultPrevented()) { return; }

				// call the effect
				effects[conf.effect].call(self, i, function() {

					// onClick callback
					e.type = "onClick";
					trigger.trigger(e, [i]);					
				});			
				
				// default behaviour
				current = i;
				tabs.removeClass(conf.current);	
				tab.addClass(conf.current);				
				
				return self;
			},
			
			getConf: function() {
				return conf;	
			},

			getTabs: function() {
				return tabs;	
			},
			
			getPanes: function() {
				return panes;	
			},
			
			getCurrentPane: function() {
				return panes.eq(current);	
			},
			
			getCurrentTab: function() {
				return tabs.eq(current);	
			},
			
			getIndex: function() {
				return current;	
			}, 
			
			next: function() {
				return self.click(current + 1);
			},
			
			prev: function() {
				return self.click(current - 1);	
			},
			
			destroy: function() {
				tabs.unbind(conf.event).removeClass(conf.current);
				panes.find("a[href^=#]").unbind("click.T"); 
				return self;
			}
		
		});

		// callbacks	
		$.each("onBeforeClick,onClick".split(","), function(i, name) {
				
			// configuration
			if ($.isFunction(conf[name])) {
				$(self).bind(name, conf[name]); 
			}

			// API
			self[name] = function(fn) {
				if (fn) { $(self).bind(name, fn); }
				return self;	
			};
		});
	
		
		if (conf.history && $.fn.history) {
			$.tools.history.init(tabs);
			conf.event = 'history';
		}	
		
		// setup click actions for each tab
		tabs.each(function(i) { 				
			$(this).bind(conf.event, function(e) {
				self.click(i, e);
				return e.preventDefault();
			});			
		});
		
		// cross tab anchor link
		panes.find("a[href^=#]").bind("click.T", function(e) {
			self.click($(this).attr("href"), e);		
		}); 
		
		// open initial tab
		if (location.hash && conf.tabs == "a" && root.find("[href=" +location.hash+ "]").length) {
			self.click(location.hash);

		} else {
			if (conf.initialIndex === 0 || conf.initialIndex > 0) {
				self.click(conf.initialIndex);
			}
		}				
		
	}
	
	
	// jQuery plugin implementation
	$.fn.tabs = function(paneSelector, conf) {
		
		// return existing instance
		var el = this.data("tabs");
		if (el) { 
			el.destroy();	
			this.removeData("tabs");
		}

		if ($.isFunction(conf)) {
			conf = {onBeforeClick: conf};
		}
		
		// setup conf
		conf = $.extend({}, $.tools.tabs.conf, conf);		
		
		
		this.each(function() {				
			el = new Tabs($(this), paneSelector, conf);
			$(this).data("tabs", el); 
		});		
		
		return conf.api ? el: this;		
	};		
		
}) (jQuery); 


/* tooltip.js */
/**
 * @license 
 * jQuery Tools @VERSION Tooltip - UI essentials
 * 
 * NO COPYRIGHTS OR LICENSES. DO WHAT YOU LIKE.
 * 
 * http://flowplayer.org/tools/tooltip/
 *
 * Since: November 2008
 * Date: @DATE 
 */
(function($) { 	
	// static constructs
	$.tools = $.tools || {version: '@VERSION'};
	
	$.tools.tooltip = {
		
		conf: { 
			
			// default effect variables
			effect: 'toggle',			
			fadeOutSpeed: "fast",
			predelay: 0,
			delay: 30,
			opacity: 1,			
			tip: 0,
			
			// 'top', 'bottom', 'right', 'left', 'center'
			position: ['top', 'center'], 
			offset: [0, 0],
			relative: false,
			cancelDefault: true,
			
			// type to event mapping 
			events: {
				def: 			"mouseenter,mouseleave",
				input: 		"focus,blur",
				widget:		"focus mouseenter,blur mouseleave",
				tooltip:		"mouseenter,mouseleave"
			},
			
			// 1.2
			layout: '<div/>',
			tipClass: 'tooltip'
		},
		
		addEffect: function(name, loadFn, hideFn) {
			effects[name] = [loadFn, hideFn];	
		} 
	};
	
	
	var effects = { 
		toggle: [ 
			function(done) { 
				var conf = this.getConf(), tip = this.getTip(), o = conf.opacity;
				if (o < 1) { tip.css({opacity: o}); }
				tip.show();
				done.call();
			},
			
			function(done) { 
				this.getTip().hide();
				done.call();
			} 
		],
		
		fade: [
			function(done) { 
				var conf = this.getConf();
				this.getTip().fadeTo(conf.fadeInSpeed, conf.opacity, done); 
			},  
			function(done) { 
				this.getTip().fadeOut(this.getConf().fadeOutSpeed, done); 
			} 
		]		
	};   

		
	/* calculate tip position relative to the trigger */  	
	function getPosition(trigger, tip, conf) {	

		
		// get origin top/left position 
		var top = conf.relative ? trigger.position().top : trigger.offset().top, 
			 left = conf.relative ? trigger.position().left : trigger.offset().left,
			 pos = conf.position[0];

		top  -= tip.outerHeight() - conf.offset[0];
		left += trigger.outerWidth() + conf.offset[1];
		
		// iPad position fix
		if (/iPad/i.test(navigator.userAgent)) {
			top -= $(window).scrollTop();
		}
		
		// adjust Y		
		var height = tip.outerHeight() + trigger.outerHeight();
		if (pos == 'center') 	{ top += height / 2; }
		if (pos == 'bottom') 	{ top += height; }
		
		
		// adjust X
		pos = conf.position[1]; 	
		var width = tip.outerWidth() + trigger.outerWidth();
		if (pos == 'center') 	{ left -= width / 2; }
		if (pos == 'left')   	{ left -= width; }	 
		
		return {top: top, left: left};
	}		

	
	
	function Tooltip(trigger, conf) {

		var self = this, 
			 fire = trigger.add(self),
			 tip,
			 timer = 0,
			 pretimer = 0, 
			 title = trigger.attr("title"),
			 tipAttr = trigger.attr("data-tooltip"),
			 effect = effects[conf.effect],
			 shown,
				 
			 // get show/hide configuration
			 isInput = trigger.is(":input"), 
			 isWidget = isInput && trigger.is(":checkbox, :radio, select, :button, :submit"),			
			 type = trigger.attr("type"),
			 evt = conf.events[type] || conf.events[isInput ? (isWidget ? 'widget' : 'input') : 'def']; 
		
		
		// check that configuration is sane
		if (!effect) { throw "Nonexistent effect \"" + conf.effect + "\""; }					
		
		evt = evt.split(/,\s*/); 
		if (evt.length != 2) { throw "Tooltip: bad events configuration for " + type; } 
		
		
		// trigger --> show  
		trigger.bind(evt[0], function(e) {

			clearTimeout(timer);
			if (conf.predelay) {
				pretimer = setTimeout(function() { self.show(e); }, conf.predelay);	
				
			} else {
				self.show(e);	
			}
			
		// trigger --> hide
		}).bind(evt[1], function(e)  {
			clearTimeout(pretimer);
			if (conf.delay)  {
				timer = setTimeout(function() { self.hide(e); }, conf.delay);	
				
			} else {
				self.hide(e);		
			}
			
		}); 
		
		
		// remove default title
		if (title && conf.cancelDefault) { 
			trigger.removeAttr("title");
			trigger.data("title", title);			
		}		
		
		$.extend(self, {
				
			show: function(e) {  

				// tip not initialized yet
				if (!tip) {
					
					// data-tooltip 
					if (tipAttr) {
						tip = $(tipAttr);

					// single tip element for all
					} else if (conf.tip) { 
						tip = $(conf.tip).eq(0);
						
					// autogenerated tooltip
					} else if (title) { 
						tip = $(conf.layout).addClass(conf.tipClass).appendTo(document.body)
							.hide().append(title);

					// manual tooltip
					} else {	
						tip = trigger.next();  
						if (!tip.length) { tip = trigger.parent().next(); } 	 
					}
					
					if (!tip.length) { throw "Cannot find tooltip for " + trigger;	}
				} 
			 	
			 	if (self.isShown()) { return self; }  
				
			 	// stop previous animation
			 	tip.stop(true, true); 			 	
			 	
				// get position
				var pos = getPosition(trigger, tip, conf);			
		
				// restore title for single tooltip element
				if (conf.tip) {
					tip.html(trigger.data("title"));
				}

				// onBeforeShow
				e = e || $.Event();
				e.type = "onBeforeShow";
				fire.trigger(e, [pos]);				
				if (e.isDefaultPrevented()) { return self; }
		
				
				// onBeforeShow may have altered the configuration
				pos = getPosition(trigger, tip, conf);
				
				// set position
				tip.css({position:'absolute', top: pos.top, left: pos.left});					
				
				shown = true;
				
				// invoke effect 
				effect[0].call(self, function() {
					e.type = "onShow";
					shown = 'full';
					fire.trigger(e);		 
				});					

	 	
				// tooltip events       
				var event = conf.events.tooltip.split(/,\s*/);

				if (!tip.data("__set")) {
					
					tip.bind(event[0], function() { 
						clearTimeout(timer);
						clearTimeout(pretimer);
					});
					
					if (event[1] && !trigger.is("input:not(:checkbox, :radio), textarea")) { 					
						tip.bind(event[1], function(e) {
	
							// being moved to the trigger element
							if (e.relatedTarget != trigger[0]) {
								trigger.trigger(evt[1].split(" ")[0]);
							}
						}); 
					} 
					
					tip.data("__set", true);
				}
				
				return self;
			},
			
			hide: function(e) {

				if (!tip || !self.isShown()) { return self; }
			
				// onBeforeHide
				e = e || $.Event();
				e.type = "onBeforeHide";
				fire.trigger(e);				
				if (e.isDefaultPrevented()) { return; }
	
				shown = false;
				
				effects[conf.effect][1].call(self, function() {
					e.type = "onHide";
					fire.trigger(e);		 
				});
				
				return self;
			},
			
			isShown: function(fully) {
				return fully ? shown == 'full' : shown;	
			},
				
			getConf: function() {
				return conf;	
			},
				
			getTip: function() {
				return tip;	
			},
			
			getTrigger: function() {
				return trigger;	
			}		

		});		

		// callbacks	
		$.each("onHide,onBeforeShow,onShow,onBeforeHide".split(","), function(i, name) {
				
			// configuration
			if ($.isFunction(conf[name])) { 
				$(self).bind(name, conf[name]); 
			}

			// API
			self[name] = function(fn) {
				if (fn) { $(self).bind(name, fn); }
				return self;
			};
		});
		
	}
		
	
	// jQuery plugin implementation
	$.fn.tooltip = function(conf) {
		
		// return existing instance
		var api = this.data("tooltip");
		if (api) { return api; }

		conf = $.extend(true, {}, $.tools.tooltip.conf, conf);
		
		// position can also be given as string
		if (typeof conf.position == 'string') {
			conf.position = conf.position.split(/,?\s/);	
		}
		
		// install tooltip for each entry in jQuery object
		this.each(function() {
			api = new Tooltip($(this), conf); 
			$(this).data("tooltip", api); 
		});
		
		return conf.api ? api: this;		 
	};
		
}) (jQuery);

		

/* validator.js */
/**
 * @license 
 * jQuery Tools Validator @VERSION - HTML5 is here. Now use it.
 * 
 * NO COPYRIGHTS OR LICENSES. DO WHAT YOU LIKE.
 * 
 * http://flowplayer.org/tools/form/validator/
 * 
 * Since: Mar 2010
 * Date: @DATE 
 */
/*jslint evil: true */ 
(function($) {	

	$.tools = $.tools || {version: '@VERSION'};
		
	// globals
	var typeRe = /\[type=([a-z]+)\]/, 
		numRe = /^-?[0-9]*(\.[0-9]+)?$/,
		dateInput = $.tools.dateinput,
		
		// http://net.tutsplus.com/tutorials/other/8-regular-expressions-you-should-know/
		emailRe = /^([a-z0-9_\.\-\+]+)@([\da-z\.\-]+)\.([a-z\.]{2,6})$/i,
		urlRe = /^(https?:\/\/)?[\da-z\.\-]+\.[a-z\.]{2,6}[#&+_\?\/\w \.\-=]*$/i,
		v;
		 
	v = $.tools.validator = {
		
		conf: {   
			grouped: false, 				// show all error messages at once inside the container 
			effect: 'default',			// show/hide effect for error message. only 'default' is built-in
			errorClass: 'invalid',		// input field class name in case of validation error		
			
			// when to check for validity?
			inputEvent: null,				// change, blur, keyup, null 
			errorInputEvent: 'keyup',  // change, blur, keyup, null
			formEvent: 'submit',       // submit, null

			lang: 'en',						// default language for error messages 
			message: '<div/>',
			messageAttr: 'data-message', // name of the attribute for overridden error message
			messageClass: 'error',		// error message element's class name
			offset: [0, 0], 
			position: 'center right',
			singleError: false, 			// validate all inputs at once
			speed: 'normal'				// message's fade-in speed			
		},


		/* The Error Messages */
		messages: {
			"*": { en: "Please correct this value" }		
		},
		
		localize: function(lang, messages) { 
			$.each(messages, function(key, msg)  {
				v.messages[key] = v.messages[key] || {};
				v.messages[key][lang] = msg;		
			});
		},
		
		localizeFn: function(key, messages) {
			v.messages[key] = v.messages[key] || {};
			$.extend(v.messages[key], messages);
		},
		
		/** 
		 * Adds a new validator 
		 */
		fn: function(matcher, msg, fn) {
			
			// no message supplied
			if ($.isFunction(msg)) { 
				fn = msg; 
				
			// message(s) on second argument
			} else {
				if (typeof msg == 'string') { msg = {en: msg}; }
				this.messages[matcher.key || matcher] = msg;
			}

			// check for "[type=xxx]" (not supported by jQuery)
			var test = typeRe.exec(matcher);                                    
			if (test) { matcher = isType(test[1]); }				
			
			// add validator to the arsenal
			fns.push([matcher, fn]);		 
		},

		/* Add new show/hide effect */
		addEffect: function(name, showFn, closeFn) {
			effects[name] = [showFn, closeFn];
		}
		
	};
	
	/* calculate error message position relative to the input */  	
	function getPosition(trigger, el, conf) {	
		
		// get origin top/left position 
		var top = trigger.offset().top, 
			 left = trigger.offset().left,	 
			 pos = conf.position.split(/,?\s+/),
			 y = pos[0],
			 x = pos[1];
		
		top  -= el.outerHeight() - conf.offset[0];
		left += trigger.outerWidth() + conf.offset[1];
		
		
		// iPad position fix
		if (/iPad/i.test(navigator.userAgent)) {
			top -= $(window).scrollTop();
		}
		
		// adjust Y		
		var height = el.outerHeight() + trigger.outerHeight();
		if (y == 'center') 	{ top += height / 2; }
		if (y == 'bottom') 	{ top += height; }
		
		// adjust X
		var width = trigger.outerWidth();
		if (x == 'center') 	{ left -= (width  + el.outerWidth()) / 2; }
		if (x == 'left')  	{ left -= width; }	 
		
		return {top: top, left: left};
	}	
	

	
	// $.is("[type=xxx]") or $.filter("[type=xxx]") not working in jQuery 1.3.2 or 1.4.2
	function isType(type) { 
		function fn() {
			return this.getAttribute("type") == type;  	
		} 
		fn.key = "[type=" + type + "]";
		return fn;
	}	

	
	var fns = [], effects = {
		
		'default' : [
			
			// show errors function
			function(errs) {
				
				var conf = this.getConf();
				
				// loop errors
				$.each(errs, function(i, err) {
						
					// add error class	
					var input = err.input;					
					input.addClass(conf.errorClass);
					
					// get handle to the error container
					var msg = input.data("msg.el"); 
					
					// create it if not present
					if (!msg) { 
						msg = $(conf.message).addClass(conf.messageClass).appendTo(document.body);
						input.data("msg.el", msg);
					}  
					
					// clear the container 
					msg.css({visibility: 'hidden'}).find("p").remove();
					
					// populate messages
					$.each(err.messages, function(i, m) { 
						$("<p/>").html(m).appendTo(msg);			
					});
					
					// make sure the width is not full body width so it can be positioned correctly
					if (msg.outerWidth() == msg.parent().width()) {
						msg.add(msg.find("p")).css({display: 'inline'});		
					} 
					
					// insert into correct position (relative to the field)
					var pos = getPosition(input, msg, conf); 
					 
					msg.css({ visibility: 'visible', position: 'absolute', top: pos.top, left: pos.left })
						.fadeIn(conf.speed);     
				});
						
				
			// hide errors function
			}, function(inputs) {

				var conf = this.getConf();				
				inputs.removeClass(conf.errorClass).each(function() {
					var msg = $(this).data("msg.el");
					if (msg) { msg.css({visibility: 'hidden'}); }
				});
			}
		]  
	};

	
	/* sperial selectors */
	$.each("email,url,number".split(","), function(i, key) {
		$.expr[':'][key] = function(el) {
			return el.getAttribute("type") === key;
		};
	});
	

	/* 
		oninvalid() jQuery plugin. 
		Usage: $("input:eq(2)").oninvalid(function() { ... });
	*/
	$.fn.oninvalid = function( fn ){
		return this[fn ? "bind" : "trigger"]("OI", fn);
	};
	
	
	/******* built-in HTML5 standard validators *********/
	
	v.fn(":email", "Please enter a valid email address", function(el, v) {
		return !v || emailRe.test(v);
	});
	
	v.fn(":url", "Please enter a valid URL", function(el, v) {
		return !v || urlRe.test(v);
	});
	
	v.fn(":number", "Please enter a numeric value.", function(el, v) {
		return numRe.test(v);			
	});
	
	v.fn("[max]", "Please enter a value smaller than $1", function(el, v) {
			
		// skip empty values and dateinputs
		if (v === '' || dateInput && el.is(":date")) { return true; }	
		
		var max = el.attr("max");
		return parseFloat(v) <= parseFloat(max) ? true : [max];
	});
	
	v.fn("[min]", "Please enter a value larger than $1", function(el, v) {

		// skip empty values and dateinputs
		if (v === '' || dateInput && el.is(":date")) { return true; }

		var min = el.attr("min");
		return parseFloat(v) >= parseFloat(min) ? true : [min];
	});
	
	v.fn("[required]", "Please complete this mandatory field.", function(el, v) {
		if (el.is(":checkbox")) { return el.is(":checked"); }
		return !!v; 			
	});
	
	v.fn("[pattern]", function(el) {
		var p = new RegExp("^" + el.attr("pattern") + "$");  
		return p.test(el.val()); 			
	});

	
	function Validator(inputs, form, conf) {		
		
		// private variables
		var self = this, 
			 fire = form.add(self);

		// make sure there are input fields available
		inputs = inputs.not(":button, :image, :reset, :submit");			 

		// utility function
		function pushMessage(to, matcher, returnValue) {
			
			// only one message allowed
			if (!conf.grouped && to.length) { return; }
			
			// the error message
			var msg;
			
			// substitutions are returned
			if (returnValue === false || $.isArray(returnValue)) {
				msg = v.messages[matcher.key || matcher] || v.messages["*"];
				msg = msg[conf.lang] || v.messages["*"].en;

				// substitution
				var matches = msg.match(/\$\d/g);
				
				if (matches && $.isArray(returnValue)) {
					$.each(matches, function(i) {
						msg = msg.replace(this, returnValue[i]);
					});
				} 					 
				
			// error message is returned directly
			} else {
				msg = returnValue[conf.lang] || returnValue;
			}
			
			to.push(msg);
		}
		
		
		// API methods  
		$.extend(self, {

			getConf: function() {
				return conf;	
			},
			
			getForm: function() {
				return form;		
			},
			
			getInputs: function() {
				return inputs;	
			},		
			
			reflow: function() {
				inputs.each(function()  {
					var input = $(this),
						 msg = input.data("msg.el");
						 
					if (msg) {						
						var pos = getPosition(input, msg, conf);
						msg.css({ top: pos.top, left: pos.left });
					}
				});
				return self;
			},
			
			/* @param e - for internal use only */
			invalidate: function(errs, e) {
				
				// errors are given manually: { fieldName1: 'message1', fieldName2: 'message2' }
				if (!e) {
					var errors = [];
					$.each(errs, function(key, val) {
						var input = inputs.filter("[name='" + key + "']");
						if (input.length) {
							
							// trigger HTML5 ininvalid event
							input.trigger("OI", [val]);
							
							errors.push({ input: input, messages: [val]});				
						}
					});

				  	errs = errors; 
					e = $.Event();
				}
				
				// onFail callback
				e.type = "onFail";					
				fire.trigger(e, [errs]); 
				
				// call the effect
				if (!e.isDefaultPrevented()) {						
					effects[conf.effect][0].call(self, errs, e);													
				}
				
				return self;
			},
			
			reset: function(els) {
				els = els || inputs;
				els.removeClass(conf.errorClass).each(function()  {
					var msg = $(this).data("msg.el");
					if (msg) {
						msg.remove();
						$(this).data("msg.el", null);
					}
				}).unbind(conf.errorInputEvent || '');
				return self;
			},
			
			destroy: function() { 
				form.unbind(conf.formEvent + ".V").unbind("reset.V"); 
				inputs.unbind(conf.inputEvent + ".V").unbind("change.V");
				return self.reset();	
			}, 
			
			
//{{{  checkValidity() - flesh and bone of this tool
						
			/* @returns boolean */
			checkValidity: function(els, e) {
				
				els = els || inputs;    
				els = els.not(":disabled");
				if (!els.length) { return true; }

				e = e || $.Event();

				// onBeforeValidate
				e.type = "onBeforeValidate";
				fire.trigger(e, [els]);				
				if (e.isDefaultPrevented()) { return e.result; }				
					
				// container for errors
				var errs = [];
 
				// loop trough the inputs
				els.not(":radio:not(:checked)").each(function() {
						
					// field and it's error message container						
					var msgs = [], 
						 el = $(this).data("messages", msgs),
						 event = dateInput && el.is(":date") ? "onHide.v" : conf.errorInputEvent + ".v";					
					
					// cleanup previous validation event
					el.unbind(event);
					
					
					// loop all validator functions
					$.each(fns, function() {
						var fn = this, match = fn[0]; 
					
						// match found
						if (el.filter(match).length)  {  
							
							// execute a validator function
							var returnValue = fn[1].call(self, el, el.val());
							
							
							// validation failed. multiple substitutions can be returned with an array
							if (returnValue !== true) {								
								
								// onBeforeFail
								e.type = "onBeforeFail";
								fire.trigger(e, [el, match]);
								if (e.isDefaultPrevented()) { return false; }
								
								// overridden custom message
								var msg = el.attr(conf.messageAttr);
								if (msg) { 
									msgs = [msg];
									return false;
								} else {
									pushMessage(msgs, match, returnValue);
								}
							}							
						}
					});
					
					if (msgs.length) { 
						
						errs.push({input: el, messages: msgs});  
						
						// trigger HTML5 ininvalid event
						el.trigger("OI", [msgs]);
						
						// begin validating upon error event type (such as keyup) 
						if (conf.errorInputEvent) {							
							el.bind(event, function(e) {
								self.checkValidity(el, e);		
							});							
						} 					
					}
					
					if (conf.singleError && errs.length) { return false; }
					
				});
				
				
				// validation done. now check that we have a proper effect at hand
				var eff = effects[conf.effect];
				if (!eff) { throw "Validator: cannot find effect \"" + conf.effect + "\""; }
				
				// errors found
				if (errs.length) {					 
					self.invalidate(errs, e); 
					return false;
					
				// no errors
				} else {
					
					// call the effect
					eff[1].call(self, els, e);
					
					// onSuccess callback
					e.type = "onSuccess";					
					fire.trigger(e, [els]);
					
					els.unbind(conf.errorInputEvent + ".v");
				}
				
				return true;				
			}
//}}} 
			
		});
		
		// callbacks	
		$.each("onBeforeValidate,onBeforeFail,onFail,onSuccess".split(","), function(i, name) {
				
			// configuration
			if ($.isFunction(conf[name]))  {
				$(self).bind(name, conf[name]);	
			}
			
			// API methods				
			self[name] = function(fn) {
				if (fn) { $(self).bind(name, fn); }
				return self;
			};
		});	
		
		
		// form validation
		if (conf.formEvent) {
			form.bind(conf.formEvent + ".V", function(e) {
				if (!self.checkValidity(null, e)) { 
					return e.preventDefault(); 
				}
			});
		}
		
		// form reset
		form.bind("reset.V", function()  {
			self.reset();			
		});
		
		// disable browser's default validation mechanism
		if (inputs[0] && inputs[0].validity) {
			inputs.each(function()  {
				this.oninvalid = function() { 
					return false; 
				};		
			});
		}
		
		// Web Forms 2.0 compatibility
		if (form[0]) {
			form[0].checkValidity = self.checkValidity;
		}
		
		// input validation               
		if (conf.inputEvent) {
			inputs.bind(conf.inputEvent + ".V", function(e) {
				self.checkValidity($(this), e);
			});	
		} 
	
		// checkboxes, selects and radios are checked separately
		inputs.filter(":checkbox, select").filter("[required]").bind("change.V", function(e) {
			var el = $(this);
			if (this.checked || (el.is("select") && $(this).val())) {
				effects[conf.effect][1].call(self, el, e); 
			}
		});		
		
		var radios = inputs.filter(":radio").change(function(e) {
			self.checkValidity(radios, e);
		});
		
		// reposition tooltips when window is resized
		$(window).resize(function() {
			self.reflow();		
		});
	}

	
	// jQuery plugin initialization
	$.fn.validator = function(conf) { 
		
		var instance = this.data("validator");
		
		// destroy existing instance
		if (instance) { 
			instance.destroy();
			this.removeData("validator");
		} 
		
		// configuration
		conf = $.extend(true, {}, v.conf, conf);		
		
		// selector is a form		
		if (this.is("form")) {
			return this.each(function() {			
				var form = $(this); 
				instance = new Validator(form.find(":input"), form, conf);	 
				form.data("validator", instance);
			});
			
		} else {
			instance = new Validator(this, this.eq(0).closest("form"), conf);
			return this.data("validator", instance);
		}     
		
	};   
		
})(jQuery);
			

/* gt.events.js */
(function($){

/**
 * @constructor
 * @class EventTarget
 */
var EventTarget = function(){};

EventTarget.prototype = {

	/**
	 * @var Object
	 */
	listeners: {},

	/**
	 * Subscribe listener
	 * 
	 * @param type event type
	 * @param listener event listener
	 * @param scope listener scope (<b>this</b> varibale in event listener)
	 */
	on: function(/** String */type, /** Function */listener, /** Object */scope){
		if (!this.listeners[type]) {
			this.listeners[type] = [];
		}
		this.listeners[type].push({
			listener: listener,
			scope: scope || window
		});
	},

	/**
	 * Unsubscribe listener
	 * @param type event type
	 * @param listener event listener
	 */
	off: function(/** String */type, /** Function */listener){
		if (!this.listeners[type]) return;

		var listeners = this.listeners[event.type];

		for (var i in listeners) {
			if (listener === listeners[i].listener) {
				delete(listeners[i]);
				break;
			}
		}
	},

	/**
	 * Dispatch event
	 * @param event
	 */
	dispatch: function(/** Object */event){
		if (!this.listeners[event.type]) return;

		var listeners = this.listeners[event.type];

		for (var i in listeners) {
			var item = listeners[i];
			item.listener.call(item.scope, event);
			if (event.cancelBubble) break;
		}
	},

	/**
	 * Create and dispatch event
	 * @param type
	 * @param target
	 * @param data
	 */
	fire: function(/** String */type, /** Object */target, /** mixed */data){
		this.dispatch({
			type   : type,
			target : target,
			data   : data || {},
			cancelBubble: false
		});
	}
};


/** @namespace GT.events */
// expose global event target
$GT.events = new EventTarget();

// expose event target prototype as mixin
$GT.events.EventTarget = EventTarget.prototype;

})(jQuery);/* gt.utils.js */
(function($) {
	$GT.utils = function(){};
/*
	toggledParams = {
		'type': height|firstElement,
		'height': elements height (will require param if selected type == height),
		'tagName': first element tag name (will require param if selected type == firstElement),
		'lessOnStart' : false |true (default false) if set to true will be run handlerLess handler
	}
*/
	$GT.utils.dropDownText = function(elem, toggledElement, toggledParams){
		if(toggledParams.type == 'height' &&  toggledParams.height) {
			var handlerMore = jQuery.proxy(function(){
				$(toggledElement).css('height', 'auto');
				this._revertText($(elem));
			},this);
			
			var handlerLess = jQuery.proxy(function(){
				$(toggledElement).css('height', toggledParams.height + 'px');
				this._revertText($(elem));
			}, this); 
		} else if(toggledParams.type == 'firstElement' && toggledParams.tagName != undefined){
			
			var handlerMore = jQuery.proxy(function(){
				$(toggledElement).children().not(':first').show();
				this._revertText($(elem));
			},this);
			
			var handlerLess = jQuery.proxy(function(){
				//
				$(toggledElement).find(toggledParams.tagName).not(':first').hide();
				$(toggledElement).children().not(':first').hide();
				this._revertText($(elem));
			}, this); 
			
		}
		$(elem).toggle(handlerMore, handlerLess);
		if(toggledParams.lessOnStart){
			$(document).ready(handlerLess);
		}
	},

	
	$GT.utils._revertText = function(elem){
		var textRevert = $(elem).attr('reverttext');
		var previewText = $(elem).html();
		$(elem).html(textRevert);
		$(elem).attr('reverttext', previewText);
	}
}) (jQuery);/* embed_button.js */
/* 
 * Embed button js
 * GTRP m042
 */
function embedButton(embedBtnMode, baseUrl, relatedPlayerId, embedBtnWrapperId, embedBtnId, onClickMsg) {

	if(embedBtnMode=="zeroClipboard"){

		ZeroClipboard.setMoviePath(baseUrl+'sitewide/zero_clipboard/ZeroClipboard.swf');
		var clip = new ZeroClipboard.Client();
		clip.setText($Crabapple.Player.players[relatedPlayerId].getEmbedCode());
		clip.glue(embedBtnId, embedBtnWrapperId);
		clip.addEventListener( 'onMouseUp', showMessage);
	}else{
		$("#"+embedBtnId).click ( function(){
			onClickMsg ? onClickMsg : onClickMsg = "To use this video on your page, please, copy this code (Ctrl+C, Enter)";
				window.prompt (onClickMsg, $Crabapple.Player.players[relatedPlayerId].getEmbedCode());
		});
	}

	function showMessage( client ) {
		onClickMsg ? alert(onClickMsg) : alert( "Player embed code is copied to your clipboard" );
	}
}

	
/* utils_community_overlay.js */
/* 
 * GTRP M128 GT Community Overlay
 */
$GT_flux = (typeof $GT_flux !='undefined') ? $GT_flux : {};
Flux4.createWidget('UserProfile', {
	 ownerHeader: { showChangePhotoButton: true },
	 guestHeader: { },
	 tabs: [
		{ id: 'Activity', title: 'My Feed', visibility: 'all' },
		{ id: 'Following', title: 'Tracking', visibility: 'owner' },
		{ id: 'Content', title: 'User Movies', contentTypes: 'UGV', visibility: 'owner', requireTosAcceptance: true },
		/*{ id: 'Content', title: 'My Posts & Uploads', visibility: 'owner', requireTosAcceptance: true }, TODO: uncomment in GT ver 1.1 GTRP-1880 */
		{ id: 'Account', title: 'My Settings', visibility: 'owner' }
	],
	localizedStrings: {
		'_UploadVideo': 'Upload a Video',
		'_AddPost': 'New Blog Post',
		'_NumberOfViews': 'Most User Tracking',
		'_RecentlyUpdated': 'Most Recently Updated'
	}
}, function(widget) { $GT_flux.UserProfileWidget = widget; });


/* dateinput.js */
/**
 * @license                                     
 * jQuery Tools @VERSION Dateinput - <input type="date" /> for humans
 * 
 * NO COPYRIGHTS OR LICENSES. DO WHAT YOU LIKE.
 * 
 * http://flowplayer.org/tools/form/dateinput/
 *
 * Since: Mar 2010
 * Date: @DATE 
 */
(function($) {	
		
	/* TODO: 
		 preserve today highlighted
	*/
	
	$.tools = $.tools || {version: '@VERSION'};
	
	var instances = [], 
		 tool, 
		 
		 // h=72, j=74, k=75, l=76, down=40, left=37, up=38, right=39
		 KEYS = [75, 76, 38, 39, 74, 72, 40, 37],
		 LABELS = {};
	
	tool = $.tools.dateinput = {
		
		conf: { 
			format: 'mm/dd/yy',
			selectors: false,
			yearRange: [-5, 5],
			lang: 'en',
			offset: [0, 0],
			speed: 0,
			firstDay: 0, // The first day of the week, Sun = 0, Mon = 1, ...
			min: undefined,
			max: undefined,
			trigger: false,
			
			css: {
				
				prefix: 'cal',
				input: 'date',
				
				// ids
				root: 0,
				head: 0,
				title: 0, 
				prev: 0,
				next: 0,
				month: 0,
				year: 0, 
				days: 0,
				
				body: 0,
				weeks: 0,
				today: 0,		
				current: 0,
				
				// classnames
				week: 0, 
				off: 0,
				sunday: 0,
				focus: 0,
				disabled: 0,
				trigger: 0
			}  
		},
		
		localize: function(language, labels) {
			$.each(labels, function(key, val) {
				labels[key] = val.split(",");		
			});
			LABELS[language] = labels;	
		}
		
	};
	
	tool.localize("en", {
		months: 		 'January,February,March,April,May,June,July,August,September,October,November,December', 
		shortMonths: 'Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec',  
		days: 		 'Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday', 
		shortDays: 	 'Sun,Mon,Tue,Wed,Thu,Fri,Sat'	  
	});

	
//{{{ private functions
		

	// @return amount of days in certain month
	function dayAm(year, month) {
		return 32 - new Date(year, month, 32).getDate();		
	}
 
	function zeropad(val, len) {
		val = '' + val;
		len = len || 2;
		while (val.length < len) { val = "0" + val; }
		return val;
	}  
	
	// thanks: http://stevenlevithan.com/assets/misc/date.format.js 
	var Re = /d{1,4}|m{1,4}|yy(?:yy)?|"[^"]*"|'[^']*'/g, tmpTag = $("<a/>");
	
	function format(date, fmt, lang) {
		
	  var d = date.getDate(),
			D = date.getDay(),
			m = date.getMonth(),
			y = date.getFullYear(),

			flags = {
				d:    d,
				dd:   zeropad(d),
				ddd:  LABELS[lang].shortDays[D],
				dddd: LABELS[lang].days[D],
				m:    m + 1,
				mm:   zeropad(m + 1),
				mmm:  LABELS[lang].shortMonths[m],
				mmmm: LABELS[lang].months[m],
				yy:   String(y).slice(2),
				yyyy: y
			};

		var ret = fmt.replace(Re, function ($0) {
			return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
		});
		
		// a small trick to handle special characters
		return tmpTag.html(ret).html();
		
	}
	
	function integer(val) {
		return parseInt(val, 10);	
	} 

	function isSameDay(d1, d2)  {
		return d1.getFullYear() === d2.getFullYear() && 
			d1.getMonth() == d2.getMonth() &&
			d1.getDate() == d2.getDate(); 
	}

	function parseDate(val) {
		
		if (!val) { return; }
		if (val.constructor == Date) { return val; } 
		
		if (typeof val == 'string') {
			
			// rfc3339?
			var els = val.split("-");		
			if (els.length == 3) {
				return new Date(integer(els[0]), integer(els[1]) -1, integer(els[2]));
			}	
			
			// invalid offset
			if (!/^-?\d+$/.test(val)) { return; }
			
			// convert to integer
			val = integer(val);
		}
		
		var date = new Date();
		date.setDate(date.getDate() + val);
		return date; 
	}
	
//}}}
		 
	
	function Dateinput(input, conf)  { 

		// variables
		var self = this,  
			 now = new Date(),
			 css = conf.css,
			 labels = LABELS[conf.lang],
			 root = $("#" + css.root),
			 title = root.find("#" + css.title),
			 trigger,
			 pm, nm, 
			 currYear, currMonth, currDay,
			 value = input.attr("data-value") || conf.value || input.val(), 
			 min = input.attr("min") || conf.min,  
			 max = input.attr("max") || conf.max,
			 opened;

		// zero min is not undefined 	 
		if (min === 0) { min = "0"; }
		
		// use sane values for value, min & max		
		value = parseDate(value) || now;
		min   = parseDate(min || conf.yearRange[0] * 365);
		max   = parseDate(max || conf.yearRange[1] * 365);
		
		
		// check that language exists
		if (!labels) { throw "Dateinput: invalid language: " + conf.lang; }
		
		// Replace built-in date input: NOTE: input.attr("type", "text") throws exception by the browser
		if (input.attr("type") == 'date') {
			var tmp = $("<input/>");
				 
			$.each("class,disabled,id,maxlength,name,readonly,required,size,style,tabindex,title,value".split(","), function(i, attr)  {
				tmp.attr(attr, input.attr(attr));		
			});			
			input.replaceWith(tmp);
			input = tmp;
		}
		input.addClass(css.input);
		
		var fire = input.add(self);
			 
		// construct layout
		if (!root.length) {
			
			// root
			root = $('<div><div><a/><div/><a/></div><div><div/><div/></div></div>')
				.hide().css({position: 'absolute'}).attr("id", css.root);			
						
			// elements
			root.children()
				.eq(0).attr("id", css.head).end() 
				.eq(1).attr("id", css.body).children()
					.eq(0).attr("id", css.days).end()
					.eq(1).attr("id", css.weeks).end().end().end()
				.find("a").eq(0).attr("id", css.prev).end().eq(1).attr("id", css.next);		 				  
			
			// title
			title = root.find("#" + css.head).find("div").attr("id", css.title);
			
			// year & month selectors
			if (conf.selectors) {				
				var monthSelector = $("<select/>").attr("id", css.month),
					 yearSelector = $("<select/>").attr("id", css.year);				
				title.html(monthSelector.add(yearSelector));
			}						
			
			// day titles
			var days = root.find("#" + css.days); 
			
			// days of the week
			for (var d = 0; d < 7; d++) { 
				days.append($("<span/>").text(labels.shortDays[(d + conf.firstDay) % 7]));
			}
			
			$("body").append(root);
		}	
		
				
		// trigger icon
		if (conf.trigger) {
			trigger = $("<a/>").attr("href", "#").addClass(css.trigger).click(function(e)  {
				self.show();
				return e.preventDefault();
			}).insertAfter(input);	
		}
		
		
		// layout elements
		var weeks = root.find("#" + css.weeks);
		yearSelector = root.find("#" + css.year);
		monthSelector = root.find("#" + css.month);
			 
		
//{{{ pick
			 			 
		function select(date, conf, e) {  
			
			// current value
			value 	 = date;
			currYear  = date.getFullYear();
			currMonth = date.getMonth();
			currDay	 = date.getDate();				
			
			
			// change
			e = e || $.Event("api");
			e.type = "change";
			
			fire.trigger(e, [date]); 
			if (e.isDefaultPrevented()) { return; }
			
			// formatting			
			input.val(format(date, conf.format, conf.lang));
			
			// store value into input
			input.data("date", date);
			
			self.hide(e); 
		}
//}}}
		
		
//{{{ onShow

		function onShow(ev) {
			
			ev.type = "onShow";
			fire.trigger(ev);
			
			$(document).bind("keydown.d", function(e) {
					
				if (e.ctrlKey) { return true; }				
				var key = e.keyCode;			 
				
				// backspace clears the value
				if (key == 8) {
					input.val("");
					return self.hide(e);	
				}
				
				// esc key
				if (key == 27) { return self.hide(e); }						
					
				if ($(KEYS).index(key) >= 0) {
					
					if (!opened) { 
						self.show(e); 
						return e.preventDefault();
					} 
					
					var days = $("#" + css.weeks + " a"), 
						 el = $("." + css.focus),
						 index = days.index(el);
					 
					el.removeClass(css.focus);
					
					if (key == 74 || key == 40) { index += 7; }
					else if (key == 75 || key == 38) { index -= 7; }							
					else if (key == 76 || key == 39) { index += 1; }
					else if (key == 72 || key == 37) { index -= 1; }
					
					
					if (index > 41) {
						 self.addMonth();
						 el = $("#" + css.weeks + " a:eq(" + (index-42) + ")");
					} else if (index < 0) {
						 self.addMonth(-1);
						 el = $("#" + css.weeks + " a:eq(" + (index+42) + ")");
					} else {
						 el = days.eq(index);
					}
					
					el.addClass(css.focus);
					return e.preventDefault();
					
				}
			 
				// pageUp / pageDown
				if (key == 34) { return self.addMonth(); }						
				if (key == 33) { return self.addMonth(-1); }
				
				// home
				if (key == 36) { return self.today(); } 
				
				// enter
				if (key == 13) {
					if (!$(e.target).is("select")) {
						$("." + css.focus).click(); 
					} 
				}
				
				return $([16, 17, 18, 9]).index(key) >= 0;  				
			});
			
			
			// click outside dateinput
			$(document).bind("click.d", function(e) {					
				var el = e.target;
				
				if (!$(el).parents("#" + css.root).length && el != input[0] && (!trigger || el != trigger[0])) {
					self.hide(e);
				}
				
			}); 
		}
//}}}
		
		
		$.extend(self, {

//{{{  show
								
			show: function(e) {
				
				if (input.attr("readonly") || input.attr("disabled") || opened) { return; }
				
				// onBeforeShow
				e = e || $.Event();
				e.type = "onBeforeShow";
				fire.trigger(e);
				if (e.isDefaultPrevented()) { return; }
			
				$.each(instances, function() {
					this.hide();	
				});
				
				opened = true;
				
				// month selector
				monthSelector.unbind("change").change(function() {
					self.setValue(yearSelector.val(), $(this).val());		
				});
				
				// year selector
				yearSelector.unbind("change").change(function() {
					self.setValue($(this).val(), monthSelector.val());		
				});
				
				// prev / next month
				pm = root.find("#" + css.prev).unbind("click").click(function(e) {
					if (!pm.hasClass(css.disabled)) {	
						self.addMonth(-1);
					}
					return false;
				});
				
				nm = root.find("#" + css.next).unbind("click").click(function(e) {
					if (!nm.hasClass(css.disabled)) {
						self.addMonth();
					}
					return false;
				});	 
				
				// set date
				self.setValue(value);				 
				
				// show calendar
				var pos = input.offset();

				// iPad position fix
				if (/iPad/i.test(navigator.userAgent)) {
					pos.top -= $(window).scrollTop();
				}
				
				root.css({ 
					top: pos.top + input.outerHeight({margins: true}) + conf.offset[0], 
					left: pos.left + conf.offset[1] 
				});
				
				if (conf.speed) {
					root.show(conf.speed, function() {
						onShow(e);			
					});	
				} else {
					root.show();
					onShow(e);
				}
				
				return self;
			}, 
//}}}


//{{{  setValue

			setValue: function(year, month, day)  {
				
				var date = integer(month) >= -1 ?
					new Date(integer(year), integer(month), integer(day || 1)) : year || value
				;				
				
				if (date < min) { date = min; }
				else if (date > max) { date = max; }
				
				year = date.getFullYear();
				month = date.getMonth();
				day = date.getDate(); 
				
				
				// roll year & month
				if (month == -1) {
					month = 11;
					year--;
				} else if (month == 12) {
					month = 0;
					year++;
				} 
				
				if (!opened) { 
					select(date, conf);
					return self; 
				} 				
				
				currMonth = month;
				currYear = year;

				// variables
				var tmp = new Date(year, month, 1 - conf.firstDay), begin = tmp.getDay(),
					 days = dayAm(year, month),
					 prevDays = dayAm(year, month - 1),
					 week;	 
				
				// selectors
				if (conf.selectors) { 
					
					// month selector
					monthSelector.empty();
					$.each(labels.months, function(i, m) {					
						if (min < new Date(year, i + 1, -1) && max > new Date(year, i, 0)) {
							monthSelector.append($("<option/>").html(m).attr("value", i));
						}
					});
					
					// year selector
					yearSelector.empty();		
					var yearNow = now.getFullYear();
					
					for (var i = yearNow + conf.yearRange[0];  i < yearNow + conf.yearRange[1]; i++) {
						if (min <= new Date(i + 1, -1, 1) && max > new Date(i, 0, 0)) {
							yearSelector.append($("<option/>").text(i));
						}
					}		
					
					monthSelector.val(month);
					yearSelector.val(year);
					
				// title
				} else {
					title.html(labels.months[month] + " " + year);	
				} 	   
					 
				// populate weeks
				weeks.empty();				
				pm.add(nm).removeClass(css.disabled); 
				
				// !begin === "sunday"
				for (var j = !begin ? -7 : 0, a, num; j < (!begin ? 35 : 42); j++) { 
					
					a = $("<a/>");
					
					if (j % 7 === 0) {
						week = $("<div/>").addClass(css.week);
						weeks.append(week);			
					}					
					
					if (j < begin)  { 
						a.addClass(css.off); 
						num = prevDays - begin + j + 1;
						date = new Date(year, month-1, num);
						
					} else if (j >= begin + days)  {
						a.addClass(css.off);	
						num = j - days - begin + 1;
						date = new Date(year, month+1, num);
						
					} else  { 
						num = j - begin + 1;
						date = new Date(year, month, num);  
						
						// current date
						if (isSameDay(value, date)) {
							a.attr("id", css.current).addClass(css.focus);
							
						// today
						} else if (isSameDay(now, date)) {
							a.attr("id", css.today);
						}	 
					}
					
					// disabled
					if (min && date < min) {
						a.add(pm).addClass(css.disabled);						
					}
					
					if (max && date > max) {
						a.add(nm).addClass(css.disabled);						
					}
					
					a.attr("href", "#" + num).text(num).data("date", date);					
					
					week.append(a);
				}
				
				// date picking					
				weeks.find("a").click(function(e) {
					var el = $(this); 
					if (!el.hasClass(css.disabled)) {  
						$("#" + css.current).removeAttr("id");
						el.attr("id", css.current);	 
						select(el.data("date"), conf, e);
					}
					return false;
				});

				// sunday
				if (css.sunday) {
					weeks.find(css.week).each(function() {
						var beg = conf.firstDay ? 7 - conf.firstDay : 0;
						$(this).children().slice(beg, beg + 1).addClass(css.sunday);		
					});	
				} 
				
				return self;
			}, 
	//}}}
	
			setMin: function(val, fit) {
				min = parseDate(val);
				if (fit && value < min) { self.setValue(min); }
				return self;
			},
		
			setMax: function(val, fit) {
				max = parseDate(val);
				if (fit && value > max) { self.setValue(max); }
				return self;
			}, 
			
			today: function() {
				return self.setValue(now);	
			},
			
			addDay: function(amount) {
				return this.setValue(currYear, currMonth, currDay + (amount || 1));		
			},
			
			addMonth: function(amount) {
				return this.setValue(currYear, currMonth + (amount || 1), currDay);	
			},
			
			addYear: function(amount) {
				return this.setValue(currYear + (amount || 1), currMonth, currDay);	
			},
						
			hide: function(e) {				 
				
				if (opened) {  
					
					// onHide 
					e = $.Event();
					e.type = "onHide";
					fire.trigger(e);
					
					$(document).unbind("click.d").unbind("keydown.d");
					
					// cancelled ?
					if (e.isDefaultPrevented()) { return; }
					
					// do the hide
					root.hide();
					opened = false;
				}
				
				return self;
			},
			
			getConf: function() {
				return conf;	
			},
			
			getInput: function() {
				return input;	
			},
			
			getCalendar: function() {
				return root;	
			},
			
			getValue: function(dateFormat) {
				return dateFormat ? format(value, dateFormat, conf.lang) : value;	
			},
			
			isOpen: function() {
				return opened;	
			}
			
		}); 
		
		// callbacks	
		$.each(['onBeforeShow','onShow','change','onHide'], function(i, name) {
				
			// configuration
			if ($.isFunction(conf[name]))  {
				$(self).bind(name, conf[name]);	
			}
			
			// API methods				
			self[name] = function(fn) {
				if (fn) { $(self).bind(name, fn); }
				return self;
			};
		});

		
		// show dateinput & assign keyboard shortcuts
		input.bind("focus click", self.show).keydown(function(e) {

			var key = e.keyCode;
	
			// open dateinput with navigation keyw
			if (!opened &&  $(KEYS).index(key) >= 0) {
				self.show(e);
				return e.preventDefault();
			} 
			
			// allow tab
			return e.shiftKey || e.ctrlKey || e.altKey || key == 9 ? true : e.preventDefault();   
			
		}); 
		
		// initial value 		
		if (parseDate(input.val())) { 
			select(value, conf);
		}
		
	} 
	
	$.expr[':'].date = function(el) {
		var type = el.getAttribute("type");
		return type && type == 'date' || !!$(el).data("dateinput");
	};
	
	
	$.fn.dateinput = function(conf) {   
		
		// already instantiated
		if (this.data("dateinput")) { return this; } 
		
		// configuration
		conf = $.extend(true, {}, tool.conf, conf);		
		
		// CSS prefix
		$.each(conf.css, function(key, val) {
			if (!val && key != 'prefix') { 
				conf.css[key] = (conf.css.prefix || '') + (val || key);
			}
		});		
	
		var els;
		
		this.each(function() {									
			var el = new Dateinput($(this), conf);
			instances.push(el);
			var input = el.getInput().data("dateinput", el);
			els = els ? els.add(input) : input;	
		});		
	
		return els ? els : this;		
	}; 
	
	
}) (jQuery);
 
	
/* overlay.apple.js */
/**
 * @license 
 * jQuery Tools @VERSION / Overlay Apple effect. 
 * 
 * NO COPYRIGHTS OR LICENSES. DO WHAT YOU LIKE.
 * 
 * http://flowplayer.org/tools/overlay/apple.html
 *
 * Since: July 2009
 * Date: @DATE 
 */
(function($) { 

	// version number
	var t = $.tools.overlay,
		 w = $(window); 
		
	// extend global configuragion with effect specific defaults
	$.extend(t.conf, { 
		start: { 
			top: null,
			left: null
		},
		
		fadeInSpeed: 'fast',
		zIndex: 9999
	});			
	
	// utility function
	function getPosition(el) {
		var p = el.offset();
		return {
			top: p.top + el.height() / 2, 
			left: p.left + el.width() / 2
		}; 
	}
	
//{{{ load 

	var loadEffect = function(pos, onLoad) {
		
		var overlay = this.getOverlay(),
			 conf = this.getConf(),
			 trigger = this.getTrigger(),
			 self = this,
			 oWidth = overlay.outerWidth({margin:true}),
			 img = overlay.data("img"),
			 position = conf.fixed ? 'fixed' : 'absolute';  
		
		
		// growing image is required.
		if (!img) { 
			var bg = overlay.css("backgroundImage");
			
			if (!bg) { 
				throw "background-image CSS property not set for overlay"; 
			}
			
			// url("bg.jpg") --> bg.jpg
			bg = bg.slice(bg.indexOf("(") + 1, bg.indexOf(")")).replace(/\"/g, "");
			overlay.css("backgroundImage", "none");
			
			img = $('<img src="' + bg + '"/>');
			img.css({border:0, display:'none'}).width(oWidth);			
			$('body').append(img); 
			overlay.data("img", img);
		}
		
		// initial top & left
		var itop = conf.start.top || Math.round(w.height() / 2), 
			 ileft = conf.start.left || Math.round(w.width() / 2);
		
		if (trigger) {
			var p = getPosition(trigger);
			itop = p.top;
			ileft = p.left;
		} 
		
		// put overlay into final position
		if (conf.fixed) {
			itop -= w.scrollTop();
			ileft -= w.scrollLeft();
		} else {
			pos.top += w.scrollTop();
			pos.left += w.scrollLeft();				
		}
			
		// initialize background image and make it visible
		img.css({
			position: 'absolute',
			top: itop, 
			left: ileft,
			width: 0,
			zIndex: conf.zIndex
		}).show();
		
		pos.position = position;
		overlay.css(pos);
		
		// begin growing
		img.animate({			
			top: overlay.css("top"), 
			left: overlay.css("left"), 
			width: oWidth}, conf.speed, function() {
			
			// set close button and content over the image
			overlay.css("zIndex", conf.zIndex + 1).fadeIn(conf.fadeInSpeed, function()  { 
					
				if (self.isOpened() && !$(this).index(overlay)) {	
					onLoad.call(); 
				} else {
					overlay.hide();	
				} 
			});
			
		}).css("position", position);
		
	};
//}}}
	
	
	var closeEffect = function(onClose) {

		// variables
		var overlay = this.getOverlay().hide(), 
			 conf = this.getConf(),
			 trigger = this.getTrigger(),
			 img = overlay.data("img"),
			 
			 css = { 
			 	top: conf.start.top, 
			 	left: conf.start.left, 
			 	width: 0 
			 };
		
		// trigger position
		if (trigger) { $.extend(css, getPosition(trigger)); }
		
		
		// change from fixed to absolute position
		if (conf.fixed) {
			img.css({position: 'absolute'})
				.animate({ top: "+=" + w.scrollTop(), left: "+=" + w.scrollLeft()}, 0);
		}
		 
		// shrink image		
		img.animate(css, conf.closeSpeed, onClose);	
	};
	
	
	// add overlay effect	
	t.addEffect("apple", loadEffect, closeEffect); 
	
})(jQuery);	
		
/* rangeinput.js */
/**
 * @license 
 * jQuery Tools @VERSION Rangeinput - HTML5 <input type="range" /> for humans
 * 
 * NO COPYRIGHTS OR LICENSES. DO WHAT YOU LIKE.
 * 
 * http://flowplayer.org/tools/rangeinput/
 *
 * Since: Mar 2010
 * Date: @DATE 
 */
(function($) {
	 
	$.tools = $.tools || {version: '@VERSION'};
	 
	var tool;
	
	tool = $.tools.rangeinput = {
		                            
		conf: {
			min: 0,
			max: 100,		// as defined in the standard
			step: 'any', 	// granularity of the value. a non-zero float or int (or "any")
			steps: 0,
			value: 0,			
			precision: undefined,
			vertical: 0,
			keyboard: true,
			progress: false,
			speed: 100,
			
			// set to null if not needed
			css: {
				input:		'range',
				slider: 		'slider',
				progress: 	'progress',
				handle: 		'handle'					
			}

		} 
	};
	
//{{{ fn.drag
		
	/* 
		FULL featured drag and drop. 0.7 kb minified, 0.3 gzipped. done.
		Who told d'n'd is rocket science? Usage:
		
		$(".myelement").drag({y: false}).bind("drag", function(event, x, y) {
			// do your custom thing
		});
		 
		Configuration: 
			x: true, 		// enable horizontal drag
			y: true, 		// enable vertical drag 
			drag: true 		// true = perform drag, false = only fire events 
			
		Events: dragStart, drag, dragEnd. 
	*/
	var doc, draggable;
	
	$.fn.drag = function(conf) {
		
		// disable IE specialities
		document.ondragstart = function () { return false; };
		
		conf = $.extend({x: true, y: true, drag: true}, conf);
	
		doc = doc || $(document).bind("mousedown mouseup", function(e) {
				
			var el = $(e.target);  
			
			// start 
			if (e.type == "mousedown" && el.data("drag")) {
				
				var offset = el.position(),
					 x0 = e.pageX - offset.left, 
					 y0 = e.pageY - offset.top,
					 start = true;    
				
				doc.bind("mousemove.drag", function(e) {  
					var x = e.pageX -x0, 
						 y = e.pageY -y0,
						 props = {};
					
					if (conf.x) { props.left = x; }
					if (conf.y) { props.top = y; } 
					
					if (start) {
						el.trigger("dragStart");
						start = false;
					}
					if (conf.drag) { el.css(props); }
					el.trigger("drag", [y, x]);
					draggable = el;
				}); 
				
				e.preventDefault();
				
			} else {
				
				try {
					if (draggable) {  
						draggable.trigger("dragEnd");  
					}
				} finally { 
					doc.unbind("mousemove.drag");
					draggable = null; 
				}
			} 
							
		});
		
		return this.data("drag", true); 
	};	

//}}}
	

	
	function round(value, precision) {
		var n = Math.pow(10, precision);
		return Math.round(value * n) / n;
	}
	
	// get hidden element's width or height even though it's hidden
	function dim(el, key) {
		var v = parseInt(el.css(key), 10);
		if (v) { return v; }
		var s = el[0].currentStyle; 
		return s && s.width && parseInt(s.width, 10);	
	}
	
	function hasEvent(el) {
		var e = el.data("events");
		return e && e.onSlide;
	}
	
	function RangeInput(input, conf) {
		
		// private variables
		var self = this,  
			 css = conf.css, 
			 root = $("<div><div/><a href='#'/></div>").data("rangeinput", self),	
			 vertical,		
			 value,			// current value
			 origo,			// handle's start point
			 len,				// length of the range
			 pos;				// current position of the handle		
		 
		// create range	 
		input.before(root);	
		
		var handle = root.addClass(css.slider).find("a").addClass(css.handle), 	
			 progress = root.find("div").addClass(css.progress);
		
		// get (HTML5) attributes into configuration
		$.each("min,max,step,value".split(","), function(i, key) {
			var val = input.attr(key);
			if (parseFloat(val)) {
				conf[key] = parseFloat(val, 10);
			}
		});			   
		
		var range = conf.max - conf.min, 
			 step = conf.step == 'any' ? 0 : conf.step,
			 precision = conf.precision;
			 
		if (precision === undefined) {
			try {
				precision = step.toString().split(".")[1].length;
			} catch (err) {
				precision = 0;	
			}
		}  
		
		// Replace built-in range input (type attribute cannot be changed)
		if (input.attr("type") == 'range') {			
			var def = input.clone().wrap("<div/>").parent().html(),
				 clone = $(def.replace(/type/i, "type=text data-orig-type"));
				 
			clone.val(conf.value);
			input.replaceWith(clone);
			input = clone;
		}
		
		input.addClass(css.input);
			 
		var fire = $(self).add(input), fireOnSlide = true;

		
		/**
		 	The flesh and bone of this tool. All sliding is routed trough this.
			
			@param evt types include: click, keydown, blur and api (setValue call)
			@param isSetValue when called trough setValue() call (keydown, blur, api)
			
			vertical configuration gives additional complexity. 
		 */
		function slide(evt, x, val, isSetValue) { 

			// calculate value based on slide position
			if (val === undefined) {
				val = x / len * range;  
				
			// x is calculated based on val. we need to strip off min during calculation	
			} else if (isSetValue) {
				val -= conf.min;	
			}
			
			// increment in steps
			if (step) {
				val = Math.round(val / step) * step;
			}

			// count x based on value or tweak x if stepping is done
			if (x === undefined || step) {
				x = val * len / range;	
			}  
			
			// crazy value?
			if (isNaN(val)) { return self; }       
			
			// stay within range
			x = Math.max(0, Math.min(x, len));  
			val = x / len * range;   

			if (isSetValue || !vertical) {
				val += conf.min;
			}
			
			// in vertical ranges value rises upwards
			if (vertical) {
				if (isSetValue) {
					x = len -x;
				} else {
					val = conf.max - val;	
				}
			}	
			
			// precision
			val = round(val, precision); 
			
			// onSlide
			var isClick = evt.type == "click";
			if (fireOnSlide && value !== undefined && !isClick) {
				evt.type = "onSlide";           
				fire.trigger(evt, [val, x]); 
				if (evt.isDefaultPrevented()) { return self; }  
			}				
			
			// speed & callback
			var speed = isClick ? conf.speed : 0,
				 callback = isClick ? function()  {
					evt.type = "change";
					fire.trigger(evt, [val]);
				 } : null;
			
			if (vertical) {
				handle.animate({top: x}, speed, callback);
				if (conf.progress) { 
					progress.animate({height: len - x + handle.height() / 2}, speed);	
				}				
				
			} else {
				handle.animate({left: x}, speed, callback);
				if (conf.progress) { 
					progress.animate({width: x + handle.width() / 2}, speed); 
				}
			}
			
			// store current value
			value = val; 
			pos = x;			 
			
			// se input field's value
			input.val(val);

			return self;
		} 
		
		
		$.extend(self, {  
			
			getValue: function() {
				return value;	
			},
			
			setValue: function(val, e) {
				init();
				return slide(e || $.Event("api"), undefined, val, true); 
			}, 			  
			
			getConf: function() {
				return conf;	
			},
			
			getProgress: function() {
				return progress;	
			},

			getHandle: function() {
				return handle;	
			},			
			
			getInput: function() {
				return input;	
			}, 
				
			step: function(am, e) {
				e = e || $.Event();
				var step = conf.step == 'any' ? 1 : conf.step;
				self.setValue(value + step * (am || 1), e);	
			},
			
			// HTML5 compatible name
			stepUp: function(am) { 
				return self.step(am || 1);
			},
			
			// HTML5 compatible name
			stepDown: function(am) { 
				return self.step(-am || -1);
			}
			
		});
		
		// callbacks
		$.each("onSlide,change".split(","), function(i, name) {
				
			// from configuration
			if ($.isFunction(conf[name]))  {
				$(self).bind(name, conf[name]);	
			}
			
			// API methods
			self[name] = function(fn) {
				if (fn) { $(self).bind(name, fn); }
				return self;	
			};
		}); 
			

		// dragging		                                  
		handle.drag({drag: false}).bind("dragStart", function() {
		
			/* do some pre- calculations for seek() function. improves performance */			
			init();
			
			// avoid redundant event triggering (= heavy stuff)
			fireOnSlide = hasEvent($(self)) || hasEvent(input);
			
				
		}).bind("drag", function(e, y, x) {        
			
			if (input.is(":disabled")) { return false; } 
			slide(e, vertical ? y : x); 
			
		}).bind("dragEnd", function(e) {
			if (!e.isDefaultPrevented()) {
				e.type = "change";
				fire.trigger(e, [value]);	 
			}
			
		}).click(function(e) {
			return e.preventDefault();	 
		});		
		
		// clicking
		root.click(function(e) { 
			if (input.is(":disabled") || e.target == handle[0]) { 
				return e.preventDefault(); 
			}				  
			init(); 
			var fix = vertical ? handle.height() / 2 : handle.width() / 2;
			slide(e, vertical ? len-origo-fix + e.pageY  : e.pageX -origo -fix);  
		});

		if (conf.keyboard) {
			
			input.keydown(function(e) {
		
				if (input.attr("readonly")) { return; }
				
				var key = e.keyCode,
					 up = $([75, 76, 38, 33, 39]).index(key) != -1,
					 down = $([74, 72, 40, 34, 37]).index(key) != -1;					 
					 
				if ((up || down) && !(e.shiftKey || e.altKey || e.ctrlKey)) {
				
					// UP: 	k=75, l=76, up=38, pageup=33, right=39			
					if (up) {
						self.step(key == 33 ? 10 : 1, e);
						
					// DOWN:	j=74, h=72, down=40, pagedown=34, left=37
					} else if (down) {
						self.step(key == 34 ? -10 : -1, e); 
					} 
					return e.preventDefault();
				} 
			});
		}
		
		
		input.blur(function(e) {	
			var val = $(this).val();
			if (val !== value) {
				self.setValue(val, e);
			}
		});    
		
		
		// HTML5 DOM methods
		$.extend(input[0], { stepUp: self.stepUp, stepDown: self.stepDown});
		
		
		// calculate all dimension related stuff
		function init() { 
		 	vertical = conf.vertical || dim(root, "height") > dim(root, "width");
		 
			if (vertical) {
				len = dim(root, "height") - dim(handle, "height");
				origo = root.offset().top + len; 
				
			} else {
				len = dim(root, "width") - dim(handle, "width");
				origo = root.offset().left;	  
			} 	  
		}
		
		function begin() {
			init();	
			self.setValue(conf.value !== undefined ? conf.value : conf.min);
		} 
		begin();
		
		// some browsers cannot get dimensions upon initialization
		if (!len) {  
			$(window).load(begin);
		}
	}
	
	$.expr[':'].range = function(el) {
		var type = el.getAttribute("type");
		return type && type == 'range' || !!$(el).filter("input").data("rangeinput");
	};
	
	
	// jQuery plugin implementation
	$.fn.rangeinput = function(conf) {

		// already installed
		if (this.data("rangeinput")) { return this; } 
		
		// extend configuration with globals
		conf = $.extend(true, {}, tool.conf, conf);		
		
		var els;
		
		this.each(function() {				
			var el = new RangeInput($(this), $.extend(true, {}, conf));		 
			var input = el.getInput().data("rangeinput", el);
			els = els ? els.add(input) : input;	
		});		
		
		return els ? els : this; 
	};	
	
	
}) (jQuery);

/* scrollable.autoscroll.js */
/**
 * @license 
 * jQuery Tools @VERSION / Scrollable Autoscroll
 * 
 * NO COPYRIGHTS OR LICENSES. DO WHAT YOU LIKE.
 * 
 * http://flowplayer.org/tools/scrollable/autoscroll.html
 *
 * Since: September 2009
 * Date: @DATE 
 */
(function($) {		

	var t = $.tools.scrollable; 
	
	t.autoscroll = {
		
		conf: {
			autoplay: true,
			interval: 3000,
			autopause: true
		}
	};	
	
	// jQuery plugin implementation
	$.fn.autoscroll = function(conf) { 

		if (typeof conf == 'number') {
			conf = {interval: conf};	
		}
		
		var opts = $.extend({}, t.autoscroll.conf, conf), ret;
		
		this.each(function() {		
				
			var api = $(this).data("scrollable");			
			if (api) { ret = api; }
			
			// interval stuff
			var timer, stopped = true;
	
			api.play = function() { 
				
				// do not start additional timer if already exists
				if (timer) { return; }
				
				stopped = false;
				
				// construct new timer
				timer = setInterval(function() { 
					api.next();				
				}, opts.interval);
				
			};	

			api.pause = function() {
				timer = clearInterval(timer);
			};
			
			// when stopped - mouseover won't restart 
			api.stop = function() {
				api.pause();
				stopped = true;	
			};
		
			/* when mouse enters, autoscroll stops */
			if (opts.autopause) {
				api.getRoot().add(api.getNaviButtons()).hover(api.pause, api.play);
			}
			
			if (opts.autoplay) {
				api.play();				
			}

		});
		
		return opts.api ? ret : this;
		
	}; 
	
})(jQuery);		
/* scrollable.navigator.js */
/**
 * @license 
 * jQuery Tools @VERSION / Scrollable Navigator
 * 
 * NO COPYRIGHTS OR LICENSES. DO WHAT YOU LIKE.
 *
 * http://flowplayer.org/tools/scrollable/navigator.html
 *
 * Since: September 2009
 * Date: @DATE 
 */
(function($) {
		
	var t = $.tools.scrollable; 
	
	t.navigator = {
		
		conf: {
			navi: '.navi',
			naviItem: null,		
			activeClass: 'active',
			indexed: false,
			idPrefix: null,
			
			// 1.2
			history: false
		}
	};		
	
	function find(root, query) {
		var el = $(query);
		return el.length < 2 ? el : root.parent().find(query);
	}
	
	// jQuery plugin implementation
	$.fn.navigator = function(conf) {

		// configuration
		if (typeof conf == 'string') { conf = {navi: conf}; } 
		conf = $.extend({}, t.navigator.conf, conf);
		
		var ret;
		
		this.each(function() {
				
			var api = $(this).data("scrollable"),
				 navi = conf.navi.jquery ? conf.navi : find(api.getRoot(), conf.navi), 
				 buttons = api.getNaviButtons(),
				 cls = conf.activeClass,
				 history = conf.history && $.fn.history;

			// @deprecated stuff
			if (api) { ret = api; }
			
			api.getNaviButtons = function() {
				return buttons.add(navi);	
			}; 
			
			
			function doClick(el, i, e) {
				api.seekTo(i);				
				if (history) {
					if (location.hash) {
						location.hash = el.attr("href").replace("#", "");	
					}
				} else  {
					return e.preventDefault();			
				}
			}
			
			function els() {
				return navi.find(conf.naviItem || '> *');	
			}
			
			function addItem(i) {  
				
				var item = $("<" + (conf.naviItem || 'a') + "/>").click(function(e)  {
					doClick($(this), i, e);
					
				}).attr("href", "#" + i);
				
				// index number / id attribute
				if (i === 0) {  item.addClass(cls); }
				if (conf.indexed)  { item.text(i + 1); }
				if (conf.idPrefix) { item.attr("id", conf.idPrefix + i); } 
				
				return item.appendTo(navi);
			}

			
			// generate navigator
			if (els().length) {
				els().each(function(i) { 
					$(this).click(function(e)  {
						doClick($(this), i, e);		
					});
				});
				
			} else {
				$.each(api.getItems(), function(i) {
					addItem(i); 
				});
			}   
			
			// activate correct entry
			api.onBeforeSeek(function(e, index) {
				setTimeout(function() {
					if (!e.isDefaultPrevented()) {	
						var el = els().eq(index);
						if (!e.isDefaultPrevented() && el.length) {			
							els().removeClass(cls).eq(index).addClass(cls);
						}
					}
				}, 1);
			}); 
			
			function doHistory(evt, hash) {
				var el = els().eq(hash.replace("#", ""));
				if (!el.length) {
					el = els().filter("[href=" + hash + "]");	
				}
				el.click();		
			}
			
			// new item being added
			api.onAddItem(function(e, item) {
				item = addItem(api.getItems().index(item)); 
				if (history)  { item.history(doHistory); }
			});
			
			if (history) { els().history(doHistory); }
			
		});		
		
		return conf.api ? ret : this;
		
	};
	
})(jQuery);			
/* tabs.slideshow.js */
/**
 * @license 
 * jQuery Tools @VERSION Slideshow - Extend it.
 * 
 * NO COPYRIGHTS OR LICENSES. DO WHAT YOU LIKE.
 * 
 * http://flowplayer.org/tools/tabs/slideshow.html
 *
 * Since: September 2009
 * Date: @DATE 
 */
(function($) {
	
	var tool;
	
	tool = $.tools.tabs.slideshow = { 

		conf: {
			next: '.forward',
			prev: '.backward',
			disabledClass: 'disabled',
			autoplay: false,
			autopause: true,
			interval: 3000, 
			clickable: true,
			api: false
		}
	};  
	
	function Slideshow(root, conf) {
	
		var self = this,
			 fire = root.add(this),
			 tabs = root.data("tabs"),
			 timer, 
			 stopped = true;
		
			 
		// next / prev buttons
		function find(query) {
			var el = $(query);
			return el.length < 2 ? el : root.parent().find(query);	
		}	
		
		var nextButton = find(conf.next).click(function() {
			tabs.next();		
		});
		
		var prevButton = find(conf.prev).click(function() {
			tabs.prev();		
		}); 


		// extend the Tabs API with slideshow methods			
		$.extend(self, {
				
			// return tabs API
			getTabs: function() {
				return tabs;	
			},
			
			getConf: function() {
				return conf;	
			},
				
			play: function() {
	
				// do not start additional timer if already exists
				if (timer) { return self; }	
				
				// onBeforePlay
				var e = $.Event("onBeforePlay");
				fire.trigger(e);				
				if (e.isDefaultPrevented()) { return self; }				
				
				
				// construct new timer
				timer = setInterval(tabs.next, conf.interval);
				stopped = false;				
				
				// onPlay
				fire.trigger("onPlay");				
				
				return self;
			},
		
			pause: function() {
				
				if (!timer) { return self; }

				// onBeforePause
				var e = $.Event("onBeforePause");
				fire.trigger(e);					
				if (e.isDefaultPrevented()) { return self; }		
				
				timer = clearInterval(timer);
				
				// onPause
				fire.trigger("onPause");	
				
				return self;
			},
			
			// when stopped - mouseover won't restart 
			stop: function() {					
				self.pause();
				stopped = true;	
			}
			
		});

		// callbacks	
		$.each("onBeforePlay,onPlay,onBeforePause,onPause".split(","), function(i, name) {
				
			// configuration
			if ($.isFunction(conf[name]))  {
				$(self).bind(name, conf[name]);	
			}
			
			// API methods				
			self[name] = function(fn) {
				return $(self).bind(name, fn);
			};
		});	
		
	
		/* when mouse enters, slideshow stops */
		if (conf.autopause) {
			tabs.getTabs().add(nextButton).add(prevButton).add(tabs.getPanes()).hover(self.pause, function() {
				if (!stopped) { self.play(); }		
			});
		} 
		
		if (conf.autoplay) {
			self.play();	
		}
		
		if (conf.clickable) {
			tabs.getPanes().click(function()  {
				tabs.next();
			});
		} 
		
		// manage disabling of next/prev buttons
		if (!tabs.getConf().rotate) {
			
			var disabled = conf.disabledClass;
			
			if (!tabs.getIndex()) {
				prevButton.addClass(disabled);
			}
			
			tabs.onBeforeClick(function(e, i)  { 
				prevButton.toggleClass(disabled, !i);
				nextButton.toggleClass(disabled, i == tabs.getTabs().length -1); 
			});
		}  
	}
	
	// jQuery plugin implementation
	$.fn.slideshow = function(conf) {
	
		// return existing instance
		var el = this.data("slideshow");
		if (el) { return el; }
 
		conf = $.extend({}, tool.conf, conf);		
		
		this.each(function() {
			el = new Slideshow($(this), conf);
			$(this).data("slideshow", el); 			
		});	
		
		return conf.api ? el : this;
	};
	
})(jQuery); 

/* toolbox.expose.js */
/**
 * @license 
 * jQuery Tools @VERSION / Expose - Dim the lights
 * 
 * NO COPYRIGHTS OR LICENSES. DO WHAT YOU LIKE.
 * 
 * http://flowplayer.org/tools/toolbox/expose.html
 *
 * Since: Mar 2010
 * Date: @DATE 
 */
(function($) { 	

	// static constructs
	$.tools = $.tools || {version: '@VERSION'};
	
	var tool;
	
	tool = $.tools.expose = {
		
		conf: {	
			maskId: 'exposeMask',
			loadSpeed: 'slow',
			closeSpeed: 'fast',
			closeOnClick: true,
			closeOnEsc: true,
			
			// css settings
			zIndex: 9998,
			opacity: 0.8,
			startOpacity: 0,
			color: '#fff',
			
			// callbacks
			onLoad: null,
			onClose: null
		}
	};

	/* one of the greatest headaches in the tool. finally made it */
	function viewport() {
				
		// the horror case
		if ($.browser.msie) {
			
			// if there are no scrollbars then use window.height
			var d = $(document).height(), w = $(window).height();
			
			return [
				window.innerWidth || 							// ie7+
				document.documentElement.clientWidth || 	// ie6  
				document.body.clientWidth, 					// ie6 quirks mode
				d - w < 20 ? w : d
			];
		} 
		
		// other well behaving browsers
		return [$(document).width(), $(document).height()]; 
	} 
	
	function call(fn) {
		if (fn) { return fn.call($.mask); }
	}
	
	var mask, exposed, loaded, config, overlayIndex;		
	
	
	$.mask = {
		
		load: function(conf, els) {
			
			// already loaded ?
			if (loaded) { return this; }			
			
			// configuration
			if (typeof conf == 'string') {
				conf = {color: conf};	
			}
			
			// use latest config
			conf = conf || config;
			
			config = conf = $.extend($.extend({}, tool.conf), conf);

			// get the mask
			mask = $("#" + conf.maskId);
				
			// or create it
			if (!mask.length) {
				mask = $('<div/>').attr("id", conf.maskId);
				$("body").append(mask);
			}
			
			// set position and dimensions 			
			var size = viewport();
				
			mask.css({				
				position:'absolute', 
				top: 0, 
				left: 0,
				width: size[0],
				height: size[1],
				display: 'none',
				opacity: conf.startOpacity,					 		
				zIndex: conf.zIndex 
			});
			
			if (conf.color) {
				mask.css("backgroundColor", conf.color);	
			}			
			
			// onBeforeLoad
			if (call(conf.onBeforeLoad) === false) {
				return this;
			}
			
			// esc button
			if (conf.closeOnEsc) {						
				$(document).bind("keydown.mask", function(e) {							
					if (e.keyCode == 27) {
						$.mask.close(e);	
					}		
				});			
			}
			
			// mask click closes
			if (conf.closeOnClick) {
				mask.bind("click.mask", function(e)  {
					$.mask.close(e);		
				});					
			}			
			
			// resize mask when window is resized
			$(window).bind("resize.mask", function() {
				$.mask.fit();
			});
			
			// exposed elements
			if (els && els.length) {
				
				overlayIndex = els.eq(0).css("zIndex");

				// make sure element is positioned absolutely or relatively
				$.each(els, function() {
					var el = $(this);
					if (!/relative|absolute|fixed/i.test(el.css("position"))) {
						el.css("position", "relative");		
					}					
				});
			 
				// make elements sit on top of the mask
				exposed = els.css({ zIndex: Math.max(conf.zIndex + 1, overlayIndex == 'auto' ? 0 : overlayIndex)});			
			}	
			
			// reveal mask
			mask.css({display: 'block'}).fadeTo(conf.loadSpeed, conf.opacity, function() {
				$.mask.fit(); 
				call(conf.onLoad);
				loaded = "full";
			});
			
			loaded = true;			
			return this;				
		},
		
		close: function() {
			if (loaded) {
				
				// onBeforeClose
				if (call(config.onBeforeClose) === false) { return this; }
					
				mask.fadeOut(config.closeSpeed, function()  {					
					call(config.onClose);					
					if (exposed) {
						exposed.css({zIndex: overlayIndex});						
					}				
					loaded = false;
				});				
				
				// unbind various event listeners
				$(document).unbind("keydown.mask");
				mask.unbind("click.mask");
				$(window).unbind("resize.mask");  
			}
			
			return this; 
		},
		
		fit: function() {
			if (loaded) {
				var size = viewport();				
				mask.css({width: size[0], height: size[1]});
			}				
		},
		
		getMask: function() {
			return mask;	
		},
		
		isLoaded: function(fully) {
			return fully ? loaded == 'full' : loaded;	
		}, 
		
		getConf: function() {
			return config;	
		},
		
		getExposed: function() {
			return exposed;	
		}		
	};
	
	$.fn.mask = function(conf) {
		$.mask.load(conf);
		return this;		
	};			
	
	$.fn.expose = function(conf) {
		$.mask.load(conf, this);
		return this;			
	};


})(jQuery);
/* toolbox.flashembed.js */
/**
 * @license 
 * jQuery Tools @VERSION / Flashembed - New wave Flash embedding
 * 
 * NO COPYRIGHTS OR LICENSES. DO WHAT YOU LIKE.
 * 
 * http://flowplayer.org/tools/toolbox/flashembed.html
 *
 * Since : March 2008
 * Date  : @DATE 
 */ 
(function() {
		
	var IE = document.all,
		 URL = 'http://www.adobe.com/go/getflashplayer',
		 JQUERY = typeof jQuery == 'function', 
		 RE = /(\d+)[^\d]+(\d+)[^\d]*(\d*)/,
		 GLOBAL_OPTS = { 
			// very common opts
			width: '100%',
			height: '100%',		
			id: "_" + ("" + Math.random()).slice(9),
			
			// flashembed defaults
			allowfullscreen: true,
			allowscriptaccess: 'always',
			quality: 'high',	
			
			// flashembed specific options
			version: [3, 0],
			onFail: null,
			expressInstall: null, 
			w3c: false,
			cachebusting: false  		 		 
	};
	
	// version 9 bugfix: (http://blog.deconcept.com/2006/07/28/swfobject-143-released/)
	if (window.attachEvent) {
		window.attachEvent("onbeforeunload", function() {
			__flash_unloadHandler = function() {};
			__flash_savedUnloadHandler = function() {};
		});
	}
	
	// simple extend
	function extend(to, from) {
		if (from) {
			for (var key in from) {
				if (from.hasOwnProperty(key)) {
					to[key] = from[key];
				}
			}
		} 
		return to;
	}	

	// used by asString method	
	function map(arr, func) {
		var newArr = []; 
		for (var i in arr) {
			if (arr.hasOwnProperty(i)) {
				newArr[i] = func(arr[i]);
			}
		}
		return newArr;
	}

	window.flashembed = function(root, opts, conf) {
	
		// root must be found / loaded	
		if (typeof root == 'string') {
			root = document.getElementById(root.replace("#", ""));
		}
		
		// not found
		if (!root) { return; }
		
		if (typeof opts == 'string') {
			opts = {src: opts};	
		}

		return new Flash(root, extend(extend({}, GLOBAL_OPTS), opts), conf); 
	};	
	
	// flashembed "static" API
	var f = extend(window.flashembed, {
		
		conf: GLOBAL_OPTS,
	
		getVersion: function()  {
			var fo, ver;
			
			try {
				ver = navigator.plugins["Shockwave Flash"].description.slice(16); 
			} catch(e) {
				
				try  {
					fo = new ActiveXObject("ShockwaveFlash.ShockwaveFlash.7");
					ver = fo && fo.GetVariable("$version");
					
				} catch(err) {
                try  {
                    fo = new ActiveXObject("ShockwaveFlash.ShockwaveFlash.6");
                    ver = fo && fo.GetVariable("$version");  
                } catch(err2) { } 						
				} 
			}
			
			ver = RE.exec(ver);
			return ver ? [ver[1], ver[3]] : [0, 0];
		},
		
		asString: function(obj) { 

			if (obj === null || obj === undefined) { return null; }
			var type = typeof obj;
			if (type == 'object' && obj.push) { type = 'array'; }
			
			switch (type){  
				
				case 'string':
					obj = obj.replace(new RegExp('(["\\\\])', 'g'), '\\$1');
					
					// flash does not handle %- characters well. transforms "50%" to "50pct" (a dirty hack, I admit)
					obj = obj.replace(/^\s?(\d+\.?\d+)%/, "$1pct");
					return '"' +obj+ '"';
					
				case 'array':
					return '['+ map(obj, function(el) {
						return f.asString(el);
					}).join(',') +']'; 
					
				case 'function':
					return '"function()"';
					
				case 'object':
					var str = [];
					for (var prop in obj) {
						if (obj.hasOwnProperty(prop)) {
							str.push('"'+prop+'":'+ f.asString(obj[prop]));
						}
					}
					return '{'+str.join(',')+'}';
			}
			
			// replace ' --> "  and remove spaces
			return String(obj).replace(/\s/g, " ").replace(/\'/g, "\"");
		},
		
		getHTML: function(opts, conf) {

			opts = extend({}, opts);
			
			/******* OBJECT tag and it's attributes *******/
			var html = '<object width="' + opts.width + 
				'" height="' + opts.height + 
				'" id="' + opts.id + 
				'" name="' + opts.id + '"';
			
			if (opts.cachebusting) {
				opts.src += ((opts.src.indexOf("?") != -1 ? "&" : "?") + Math.random());		
			}			
			
			if (opts.w3c || !IE) {
				html += ' data="' +opts.src+ '" type="application/x-shockwave-flash"';		
			} else {
				html += ' classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"';	
			}
			
			html += '>'; 
			
			/******* nested PARAM tags *******/
			if (opts.w3c || IE) {
				html += '<param name="movie" value="' +opts.src+ '" />'; 	
			} 
			
			// not allowed params
			opts.width = opts.height = opts.id = opts.w3c = opts.src = null;
			opts.onFail = opts.version = opts.expressInstall = null;
			
			for (var key in opts) {
				if (opts[key]) {
					html += '<param name="'+ key +'" value="'+ opts[key] +'" />';
				}
			}	
		
			/******* FLASHVARS *******/
			var vars = "";
			
			if (conf) {
				for (var k in conf) { 
					if (conf[k]) {
						var val = conf[k]; 
						vars += k +'='+ (/function|object/.test(typeof val) ? f.asString(val) : val) + '&';
					}
				}
				vars = vars.slice(0, -1);
				html += '<param name="flashvars" value=\'' + vars + '\' />';
			}
			
			html += "</object>";	
			
			return html;				
		},
		
		isSupported: function(ver) {
			return VERSION[0] > ver[0] || VERSION[0] == ver[0] && VERSION[1] >= ver[1];			
		}		
		
	});
	
	var VERSION = f.getVersion(); 
	
	function Flash(root, opts, conf) {  
	                                                
		// version is ok
		if (f.isSupported(opts.version)) {
			root.innerHTML = f.getHTML(opts, conf);
			
		// express install
		} else if (opts.expressInstall && f.isSupported([6, 65])) {
			root.innerHTML = f.getHTML(extend(opts, {src: opts.expressInstall}), {
				MMredirectURL: location.href,
				MMplayerType: 'PlugIn',
				MMdoctitle: document.title
			});	
			
		} else {
			
			// fail #2.1 custom content inside container
			if (!root.innerHTML.replace(/\s/g, '')) {
				root.innerHTML = 
					"<h2>Flash version " + opts.version + " or greater is required</h2>" + 
					"<h3>" + 
						(VERSION[0] > 0 ? "Your version is " + VERSION : "You have no flash plugin installed") +
					"</h3>" + 
					
					(root.tagName == 'A' ? "<p>Click here to download latest version</p>" : 
						"<p>Download latest version from <a href='" + URL + "'>here</a></p>");
					
				if (root.tagName == 'A') {	
					root.onclick = function() {
						location.href = URL;
					};
				}				
			}
			
			// onFail
			if (opts.onFail) {
				var ret = opts.onFail.call(this);
				if (typeof ret == 'string') { root.innerHTML = ret; }	
			}			
		}
		
		// http://flowplayer.org/forum/8/18186#post-18593
		if (IE) {
			window[opts.id] = document.getElementById(opts.id);
		} 
		
		// API methods for callback
		extend(this, {
				
			getRoot: function() {
				return root;	
			},
			
			getOptions: function() {
				return opts;	
			},

			
			getConf: function() {
				return conf;	
			}, 
			
			getApi: function() {
				return root.firstChild;	
			}
			
		}); 
	}
	
	// setup jquery support
	if (JQUERY) {
		
		// tools version number
		jQuery.tools = jQuery.tools || {version: '@VERSION'};
		
		jQuery.tools.flashembed = {  
			conf: GLOBAL_OPTS
		};	
		
		jQuery.fn.flashembed = function(opts, conf) {		
			return this.each(function() { 
				$(this).data("flashembed", flashembed(this, opts, conf));
			});
		}; 
	} 
	
})();

/* toolbox.history.js */
/**
 * @license 
 * jQuery Tools @VERSION History "Back button for AJAX apps"
 * 
 * NO COPYRIGHTS OR LICENSES. DO WHAT YOU LIKE.
 * 
 * http://flowplayer.org/tools/toolbox/history.html
 * 
 * Since: Mar 2010
 * Date: @DATE 
 */
(function($) {
		
	var hash, iframe, links, inited;		
	
	$.tools = $.tools || {version: '@VERSION'};
	
	$.tools.history = {
	
		init: function(els) {
			
			if (inited) { return; }
			
			// IE
			if ($.browser.msie && $.browser.version < '8') {
				
				// create iframe that is constantly checked for hash changes
				if (!iframe) {
					iframe = $("<iframe/>").attr("src", "javascript:false;").hide().get(0);
					$("body").append(iframe);
									
					setInterval(function() {
						var idoc = iframe.contentWindow.document, 
							 h = idoc.location.hash;
					
						if (hash !== h) {						
							$.event.trigger("hash", h);
						}
					}, 100);
					
					setIframeLocation(location.hash || '#');
				}

				
			// other browsers scans for location.hash changes directly without iframe hack
			} else { 
				setInterval(function() {
					var h = location.hash;
					if (h !== hash) {
						$.event.trigger("hash", h);
					}						
				}, 100);
			}

			links = !links ? els : links.add(els);
			
			els.click(function(e) {
				var href = $(this).attr("href");
				if (iframe) { setIframeLocation(href); }
				
				// handle non-anchor links
				if (href.slice(0, 1) != "#") {
					location.href = "#" + href;
					return e.preventDefault();		
				}
				
			}); 
			
			inited = true;
		}	
	};  
	

	function setIframeLocation(h) {
		if (h) {
			var doc = iframe.contentWindow.document;
			doc.open().close();	
			doc.location.hash = h;
		}
	} 
		 
	// global histroy change listener
	$(window).bind("hash", function(e, h)  { 
		if (h) {
			links.filter(function() {
			  var href = $(this).attr("href");
			  return href == h || href == h.replace("#", ""); 
			}).trigger("history", [h]);	
		} else {
			links.eq(0).trigger("history", [h]);	
		}

		hash = h;

	});
		
	
	// jQuery plugin implementation
	$.fn.history = function(fn) {
			
		$.tools.history.init(this);

		// return jQuery
		return this.bind("history", fn);		
	};	
		
})(jQuery); 

/* toolbox.mousewheel.js */
/**
 * @license 
 * jQuery Tools @VERSION Mousewheel
 * 
 * NO COPYRIGHTS OR LICENSES. DO WHAT YOU LIKE.
 * 
 * http://flowplayer.org/tools/toolbox/mousewheel.html
 * 
 * based on jquery.event.wheel.js ~ rev 1 ~ 
 * Copyright (c) 2008, Three Dub Media
 * http://threedubmedia.com 
 *
 * Since: Mar 2010
 * Date: @DATE 
 */
(function($) { 
	
	$.fn.mousewheel = function( fn ){
		return this[ fn ? "bind" : "trigger" ]( "wheel", fn );
	};

	// special event config
	$.event.special.wheel = {
		setup: function() {
			$.event.add( this, wheelEvents, wheelHandler, {} );
		},
		teardown: function(){
			$.event.remove( this, wheelEvents, wheelHandler );
		}
	};

	// events to bind ( browser sniffed... )
	var wheelEvents = !$.browser.mozilla ? "mousewheel" : // IE, opera, safari
		"DOMMouseScroll"+( $.browser.version<"1.9" ? " mousemove" : "" ); // firefox

	// shared event handler
	function wheelHandler( event ) {
		
		switch ( event.type ) {
			
			// FF2 has incorrect event positions
			case "mousemove": 
				return $.extend( event.data, { // store the correct properties
					clientX: event.clientX, clientY: event.clientY,
					pageX: event.pageX, pageY: event.pageY
				});
				
			// firefox	
			case "DOMMouseScroll": 
				$.extend( event, event.data ); // fix event properties in FF2
				event.delta = -event.detail / 3; // normalize delta
				break;
				
			// IE, opera, safari	
			case "mousewheel":				
				event.delta = event.wheelDelta / 120;
				break;
		}
		
		event.type = "wheel"; // hijack the event	
		return $.event.handle.call( this, event, event.delta );
	}
	
})(jQuery); 

/* tooltip.dynamic.js */
/**
 * @license 
 * jQuery Tools @VERSION / Tooltip Dynamic Positioning
 * 
 * NO COPYRIGHTS OR LICENSES. DO WHAT YOU LIKE.
 * 
 * http://flowplayer.org/tools/tooltip/dynamic.html
 *
 * Since: July 2009
 * Date: @DATE 
 */
(function($) { 

	// version number
	var t = $.tools.tooltip;
	
	t.dynamic = {
		conf: {
			classNames: "top right bottom left"
		}
	};
		
	/* 
	 * See if element is on the viewport. Returns an boolean array specifying which
	 * edges are hidden. Edges are in following order:
	 * 
	 * [top, right, bottom, left]
	 * 
	 * For example following return value means that top and right edges are hidden
	 * 
	 * [true, true, false, false]
	 * 
	 */
	function getCropping(el) {
		
		var w = $(window); 
		var right = w.width() + w.scrollLeft();
		var bottom = w.height() + w.scrollTop();		
		
		return [
			el.offset().top <= w.scrollTop(), 						// top
			right <= el.offset().left + el.width(),				// right
			bottom <= el.offset().top + el.height(),			// bottom
			w.scrollLeft() >= el.offset().left 					// left
		]; 
	}
	
	/*
		Returns true if all edges of an element are on viewport. false if not
		
		@param crop the cropping array returned by getCropping function
	 */
	function isVisible(crop) {
		var i = crop.length;
		while (i--) {
			if (crop[i]) { return false; }	
		}
		return true;
	}
	
	// dynamic plugin
	$.fn.dynamic = function(conf) {
		
		if (typeof conf == 'number') { conf = {speed: conf}; }
		
		conf = $.extend({}, t.dynamic.conf, conf);
		
		var cls = conf.classNames.split(/\s/), orig;	
			
		this.each(function() {		
				
			var api = $(this).tooltip().onBeforeShow(function(e, pos) {				

				// get nessessary variables
				var tip = this.getTip(), tipConf = this.getConf();  

				/*
					We store the original configuration and use it to restore back to the original state.
				*/					
				if (!orig) {
					orig = [
						tipConf.position[0], 
						tipConf.position[1], 
						tipConf.offset[0], 
						tipConf.offset[1], 
						$.extend({}, tipConf)
					];
				}
				
				/*
					display tip in it's default position and by setting visibility to hidden.
					this way we can check whether it will be on the viewport
				*/
				$.extend(tipConf, orig[4]);
				tipConf.position = [orig[0], orig[1]];
				tipConf.offset = [orig[2], orig[3]];

				tip.css({
					visibility: 'hidden',
					position: 'absolute',
					top: pos.top,
					left: pos.left 
				}).show(); 
				
				// now let's see for hidden edges
				var crop = getCropping(tip);		
								
				// possibly alter the configuration
				if (!isVisible(crop)) {
					
					// change the position and add class
					if (crop[2]) { $.extend(tipConf, conf.top);		tipConf.position[0] = 'top'; 		tip.addClass(cls[0]); }
					if (crop[3]) { $.extend(tipConf, conf.right);	tipConf.position[1] = 'right'; 	tip.addClass(cls[1]); }					
					if (crop[0]) { $.extend(tipConf, conf.bottom); 	tipConf.position[0] = 'bottom';	tip.addClass(cls[2]); } 
					if (crop[1]) { $.extend(tipConf, conf.left);		tipConf.position[1] = 'left'; 	tip.addClass(cls[3]); }					
					
					// vertical offset
					if (crop[0] || crop[2]) { tipConf.offset[0] *= -1; }
					
					// horizontal offset
					if (crop[1] || crop[3]) { tipConf.offset[1] *= -1; }
				}  
				
				tip.css({visibility: 'visible'}).hide();
		
			});
			
			// restore positioning as soon as possible
			api.onBeforeShow(function() {
				var c = this.getConf(), tip = this.getTip();		 
				setTimeout(function() { 
					c.position = [orig[0], orig[1]];
					c.offset = [orig[2], orig[3]];
				}, 0);
			});
			
			// remove custom class names and restore original effect
			api.onHide(function() {
				var tip = this.getTip(); 
				tip.removeClass(conf.classNames);
			});
				
			ret = api;
			
		});
		
		return conf.api ? ret : this;
	};	
	
}) (jQuery);
/* tooltip.slide.js */
/**
 * @license 
 * jQuery Tools @VERSION / Tooltip Slide Effect
 * 
 * NO COPYRIGHTS OR LICENSES. DO WHAT YOU LIKE.
 * 
 * http://flowplayer.org/tools/tooltip/slide.html
 *
 * Since: September 2009
 * Date: @DATE 
 */
(function($) { 

	// version number
	var t = $.tools.tooltip;
		
	// extend global configuragion with effect specific defaults
	$.extend(t.conf, { 
		direction: 'up', // down, left, right 
		bounce: false,
		slideOffset: 10,
		slideInSpeed: 200,
		slideOutSpeed: 200, 
		slideFade: !$.browser.msie
	});			
	
	// directions for slide effect
	var dirs = {
		up: ['-', 'top'],
		down: ['+', 'top'],
		left: ['-', 'left'],
		right: ['+', 'left']
	};
	
	/* default effect: "slide"  */
	t.addEffect("slide", 
		
		// show effect
		function(done) { 

			// variables
			var conf = this.getConf(), 
				 tip = this.getTip(),
				 params = conf.slideFade ? {opacity: conf.opacity} : {}, 
				 dir = dirs[conf.direction] || dirs.up;

			// direction			
			params[dir[1]] = dir[0] +'='+ conf.slideOffset;
			
			// perform animation
			if (conf.slideFade) { tip.css({opacity:0}); }
			tip.show().animate(params, conf.slideInSpeed, done); 
		}, 
		
		// hide effect
		function(done) {
			
			// variables
			var conf = this.getConf(), 
				 offset = conf.slideOffset,
				 params = conf.slideFade ? {opacity: 0} : {}, 
				 dir = dirs[conf.direction] || dirs.up;
			
			// direction
			var sign = "" + dir[0];
			if (conf.bounce) { sign = sign == '+' ? '-' : '+'; }			
			params[dir[1]] = sign +'='+ offset;			
			
			// perform animation
			this.getTip().animate(params, conf.slideOutSpeed, function()  {
				$(this).hide();
				done.call();		
			});
		}
	);  
	
})(jQuery);	
		
/* article_body-game_review.js */
$(function() {
	$GT.utils.dropDownText('.article_body-game_review .more', '.article_body-game_review .pane', {
		type: 'height',
		height: '640'
	});
});
/* article_navigation.js */
$(function() {
	$('.article_navigation img').load().each(function(){
		$(this).css({'margin-left': -($(this).width()/2), 'left': '50%'});
	});
});/* blog_promo.js */
$(function(){
	$('.blog_promo img').load().each(function(){
		$(this).css({'margin-left': -($(this).width()/2), 'left': '50%'});
	});
});/* calendar.js */
$(function() {
	var calendar = { };

	// Truke for IE7
	calendar.fixIe7 = function() {
		if ($.browser.msie  && parseInt($.browser.version, 10) === 7) {
			var z_index = $(".calendar ul.schedule").children().length;
			$(".calendar ul.schedule").children().each(function() {
				$(this).css("z-index", z_index--);
			});
		}
	}

	// Setting correct height for Event	
	calendar.setCorrectHeightForEvents = function() {
		$(".calendar ul.schedule div.event").each(function() {
			$(this).height($(this).attr("duration"));
		});
	}

	// Load data
	calendar.loadDataByUrl = function(url) {
		//show loader
		$('.calendar .middle').html("<div class='ajax_spinner'><img src='/sitewide/images/shared/ajax-loader.gif' width='32' height='32' alt='' border='0' /></div>");
		//do ajax call for results
		$.ajax({
			url: url,
			success: function(data) {
				$('.calendar .middle').html($(data).children('.middle').children());
				calendar.fixIe7();
				calendar.setCorrectHeightForEvents();
			},
			error: function() {
				$('.calendar .middle').html('An error has occurred. Please reload the page!');
			}
		});
	};

	// Initialization
	calendar.init = function() {
		this.fixIe7();
		this.setCorrectHeightForEvents();
		$('.calendar .days a').live('click',function(evt){
			evt.preventDefault();
			calendar.loadDataByUrl($(this).attr('href'));
		});
	}
	
	calendar.init();
});
/* carousel_generic.js */
$(function() {
	// initialize scrollable
	$Crabapple(".carousel ul.items").carousel({
		scroll: 3,
		wrap: 'last',
		circularOnLast: true,
		initCallback: function(){$('.carousel ul.items').css({visibility: 'visible'});}
	});
});/* carousel_hp.js */
if($(".carousel_hp ul.items_list").length != 0) {
	$(document).ready(function() {
		// initialize scrollable
		var carouselHp = new $Crabapple.CarouselHp().init($(".carousel_hp ul.items_list")[0], {
			scroll: 4,
			hightLightFirstVisibleItem: true,
			circularOnLast: true
		});
		carouselHp.autoHighlight(10000);
		$(".carousel_hp ul.items_list").show();
	});
}/* carousel_hub.js */
/**
 * This script inserts video player into carousel.
 * We need this for module M006 
 */
if ($('.carousel_hub').length != 0){
	$(function () {
		var delayMillisec = $('.carousel_hub').attr("data-delay_milliseconds");
		var defaultDelay = 6000;
		delayMillisec = new Number(delayMillisec);

		if(isNaN(delayMillisec)){
			delayMillisec = defaultDelay;
		}else if(delayMillisec.valueOf() < 500){
			delayMillisec = defaultDelay;
		}else{
			delayMillisec = parseInt(delayMillisec.valueOf());
		}

		var carouselObjClass = function(){};
		carouselObjClass.prototype = {

			init: function(){
				this.playerVisible = false;
				this.$elem = $('.carousel_hub ul.thumbs');
				this.$playButton = $('.play_button', this.$elem);
				this.carouselOptions = {
						buttonNextHTML: null,
						buttonPrevHTML: null,
						scroll: 3,
						circularOnLast: true,
						hightLightFirstVisibleItem: true,
						itemFallbackDimension: 204
				}
				this.carousel = new $Crabapple.CarouselHp().init(this.$elem[0], this.carouselOptions);
				this.carousel.onAutoHighlight = function() {
					if (this.playerVisible == true) {
						this.playerVisible = false;
						this.hidePlayer();
						this.startCarousel();
						$video_player.pause();
					}
				};
				var self = this;
				if ($Crabapple.Player.players.video_player_box != undefined){
					$video_player = $Crabapple.Player.players.video_player_box;
					$video_player.events.onPlaylistComplete = function(){
						self.hidePlayer();
						self.startCarousel();
					}

					self.hidePlayer();
					this.carousel.autoHighlight(delayMillisec, this.carousel.onAutoHighlight, self);
					
			} else{
					this.carousel.autoHighlight(delayMillisec, this.carousel.onAutoHighlight);
				}
	
				//set processing of video start playing event
				$('.carousel_hub .play_button').click(function(){
					self.stopCarousel();
					$(this).hide();
					$('.carousel_hub .ajax_spinner').show();
					//run player with video
					self.showPlayer();
					$video_player.playURI($(this).attr('data-mgid'));
					$video_player.play();
				});
			},
			hidePlayer: function(){
				$('.carousel_hub .ajax_spinner').hide();
				$('#carouser_wrapper').css('opacity', '0.01').attr('data-play', 'stop');
				//move play button and image over the player
				$('.carousel_hub li > img').show();
				$('.carousel_hub .play_button').show();
			},
			stopCarousel: function(){
				this.carousel.setFreezeOnHover(false);
				this.carousel._autoRotateStop();
			},
			startCarousel: function(){
				this.carousel.setFreezeOnHover(true);
				this.carousel._autoRotateStart();
			},
			showPlayer: function(){
				this.playerVisible = true;
				$video_player.events.onPlayheadUpdate = function(){
				if ($Crabapple.Player.players.video_player_box.state == 'playstates.playing')
				{
					$('#carouser_wrapper').css('opacity', '1').attr('data-play', 'play');
					//move play button and image under the player
					$('.carousel_hub .ajax_spinner').hide();
					$('.carousel_hub li > img').hide();
					$video_player.events.onPlayheadUpdate = function(){};
				}		
			}
			}
		};
		var carouselObj = new carouselObjClass();
		carouselObj.init();	
	});
}/* cc.google_plus.js */
// Callback function for Google Plus
// Use callback or data-callback in button tag
// obj{ "href":target URL,
//      "state": "on"|"off"}
// obj.state signifies whether the like was turned on or off
function google1Callback(obj) {
	if (String(obj.state) == 'on') {
	 shareBarLinkTracking('google1');
	}
}

$(function () {
	var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/plusone.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
});/* click_tracking.js */
// General Link Tracking Omniture Call
function autoLinkTrackEvent(promoName, destinationUrl){
	var destinationUrl = (typeof destinationUrl != 'undefined') ? destinationUrl : 'no_destination_url';
	
	if (destinationUrl.substr(0,1) == '/') {
		destinationUrl = window.location.protocol + "//" + window.location.host + destinationUrl;
	}
	
	if (typeof(mtvn) != 'undefined') {
			mtvn.btg.Controller.sendLinkEvent({
			linkName:promoName + '|' + pageName,
			linkType:'o',
			prop8:repCallObject.prop8,
			prop25:promoName,
			prop26:promoName + '|' + pageName,
			prop27:destinationUrl,
			eVar5:destinationUrl,
			eVar6:repCallObject.eVar6,
			eVar7:promoName,
			eVar8:promoName + '|' + pageName,
			eVar9:pageName
		});
	}
}

/*
 * Sharing Widget Tracking for Omniture
 * http://confluence.mtvi.com/display/ENT/Omniture+Page+Level+Reporting+Requirements+for+GTRP 
 * http://jira.mtvi.com/browse/GTRP-1324
*/
function shareWidgetTracking (shareService, linkValue) {

	if(typeof(pageName) == 'undefined'){
		pageName = 'pageName not found';
	}

	if (typeof(mtvn) != 'undefined') {
		mtvn.btg.Controller.sendLinkEvent({
			linkName:linkValue,
			linkType:'o',
			eVar9:pageName,
			eVar19:shareService,
			events:'event9'
		});	
	}
}


$(function() {

	// Header Top Platform Nav	
	$('#visible_header .header_top>#vertical_cap>.main_menu a').click(function (){
		var destinationUrl = $(this).attr('href');
		autoLinkTrackEvent('top-platform-nav', destinationUrl);
	});
	
	// Top Logo	
	$('#visible_header .header_top>a.logo').click(function (){
		var destinationUrl = $(this).attr('href');
		autoLinkTrackEvent('top-logo', destinationUrl);
	});	
	
	// Top Social Nav
	$('#visible_header .header_top>.share_buttons a').click(function (){
		var destinationUrl = $(this).attr('href');
		autoLinkTrackEvent('top-share-nav', destinationUrl);
	});	
	
	// Top Nav links	
	$('#visible_header .header_bottom>#top_navigation a').click(function (){
		var destinationUrl = $(this).attr('href');
		autoLinkTrackEvent('top-nav', destinationUrl);
	});
	
	// Search Box GT Search
	$('#visible_header .header_bottom>#visible_header_search form:first').submit(function() {
		var destinationUrl = $(this).attr('action');
		autoLinkTrackEvent('searchbox', destinationUrl);
	});
	
	// Search Box WEB Search
	$('#visible_header .header_bottom>#visible_header_search div.web').click(function() {
		var destinationUrl = $(this).attr('data-search_engine');
		autoLinkTrackEvent('searchbox', destinationUrl);
	});
	
	// Footer Links
	$('.global_footer a').click(function (){
		autoLinkTrackEvent('footer', $(this).attr('href'));
	});

	// All other General Modules auto link tracking
	var $allModuleATags = $('#content_holder div.module a'); // Get <a> tags with <div>s that have "module" in the name
	
	$($allModuleATags).each(function (){
		
		// Get the Div that contains the <a> tag
		$divWithModuleClass = $(this).closest('div.module'); // Get <div> that is nearest parent to the <a> tag with module in class name
		var destinationUrl = $(this).attr('href');
		
		var promo_name = 'unnamed_promo'; // protect against no second class name
		var allClasses = $divWithModuleClass.attr('class'); // classes of div as a string
	
		// make sure its not an ad slot && auto tracking is wanted
		skipTheseClasses = /(noAutoLinkTracking|ad_)/;
		if ( !skipTheseClasses.test(allClasses) ){
			
			allClasses = allClasses.replace(/module\b/i, ''); //remove first standalone "module" 
			allClasses = allClasses.replace(/^\s+|\s+$/gi, ''); // remove beginning & end spaces
			allClasses = allClasses.replace(/\s{2,}/gi, ' '); // replace multiple spaces with a single space
					
			promo_name = allClasses.match(/[\w-]+/i, ''); // grab the first word.  dash had to be added
	
			$(this).click(function (){
				autoLinkTrackEvent(promo_name, $(this).attr('href'));
			});
		}
	});
});

/* comments-teaser.js */
/**
 * Used in GTRP M033 What You're Saying
 **/
$(document).ready(function(){

	var module = $(".comments-teaser");
	if(module.length !=0){
		var aboutField = module.attr("about-field");
		var items = $('.middle ul', module);
		var itemLimit = module.attr("display-limit");
		$Crabapple.FluxWidget.requestCommentsRiver(itemLimit, function (data) {
			if(data.Activities.length > 0){
				var patt=new RegExp("<object.*?>(.+)<\/object>","gi");
				for(var i=data.Activities.length-1; i>=0; i--){
					var comment = (data.Activities[i].Data[0].Value).replace(patt, mediaContentReplacer);
					var	date = data.Activities[i].DateCreated;
						date = (date.substring(6,date.length-2))*1;
						date = new Date(date);
					
					var tmp = $('<a>')
						.addClass('author')
						.attr('data-index', i)
						.text(data.Activities[i].Author.AuthorTitle)
						.click(function(e){
							var index = $(this).attr('data-index');
							$GT_flux.UserProfileWidget.openProfile(data.Activities[index].Author.AuthorId, e);
						});
					var li = $(
						"<li>"+
						"<div class='dotted_line'>&nbsp;</div>"+
						"<div class='user_info'>"+
							"<img alt=''  src='"+data.Activities[i].Author.Thumbnails.Medium+"'>"+
						"</div>"+
						"<div class='comment_info'>"+
							"<div><p class='comment'>"+comment+"</p><p class='author'></p></div>"+
							"<p class='about'><span>About:</span> "+aboutField+"</p>"+
							"<p class='posted_time'>- Posted "+$Crabapple.utils.DateTime.relativeTime(date)+"</p>"+
						"</div>"+
						"</li>"
					).prependTo(items);
					$('.comment_info div .author', li).append(tmp);
				}
			}else{
				items.prepend("<li><h3>There are no comments at the moment</h3></li>");
			}
		});
	}
});

function mediaContentReplacer(matchedStr){
	var obj = $(matchedStr);
	var url;
	var link;

	if(obj.attr("type")=="application/x-shockwave-flash" && obj.attr("data")){
		url = obj.attr("data");
	}else if(obj.find("embed").attr("type")=="application/x-shockwave-flash" && obj.find("embed").attr("src")){
		url = obj.find("embed").attr("src");
	}else{
		obj.find("param").each(function(){
			if($(this).attr("name")=="movie"){
				url = $(this).attr("value")
			}
		});
	}
	if(url!=null){
		link = "<a href='"+url+"'>Click to see video content</a> "
	}

	return link;
}/* date_formatter.js */
/*
 * JS code to implement date formatter
 */
$(function(){
	if($('.date_formatter').length){
		$('.date_formatter').each(function(){
			time = $(this).html();
			dateDifference = $Crabapple.utils.DateTime.relativeTime(time);
			if(dateDifference && dateDifference.length > 0){
				$(this).html(dateDifference);
			}
		});
	}
});/* event_schedule-carousel.js */
$(function() {
	// initialize scrollable
	$Crabapple(".event_schedule-carousel ul").carousel({
		scroll: 1,
		buttonPrevHTML: '<a class="prev"><span class="arrow">prev</span></a>',
		buttonNextHTML: '<a class="next"><span class="arrow">next</span></a>',
		circularOnLast: false,
		initCallback: function(){$('.event_schedule-carousel ul').css({visibility: 'visible'});}
	});
});/* exclusive.js */
/* 
 * Used in M076
 */
$(document).ready(function(){
	var module = $(".exclusive");
	if(module.length !=0){
		$Crabapple.FluxWidget.requestCommentsFeed(module.attr('data-contentId'), 1, function(data){
			module.find(".middle .summary a").text(data.NumberOfComments)
		});
	}
});/* flagship_shows.js */
$(document).ready(function(){
	$('.flagship_shows .area_tabs').each(function(){
		$Crabapple(this).tabs($(this).parent().siblings('.tab_content'));
		$(this).find('li').click(function(){
			var title = $(this).html();
			$(this).parent().siblings('h4').html(title + ' Episode');
		});
	});
});/* follow_login.js */
$(document).ready(function(){
	if($('.follow_login').length) {
		var mgid = $('.follow_login .social_and_track').attr('data-mgid');
		$Crabapple.FluxWidget.requestFollowingStats(mgid, function(data){
			$('.follow_login .module_track_this span').text(data.ServicesStatistics[0].FollowersTotalCount);
			if(data.IsFollowedByLoggedUser == true){
				$('.follow_login .module_track_this').addClass('tracked');
			}
		});

		$('.track').click(function(){
			$Crabapple.FluxWidget.requestItemTracking(mgid, function(){
				$('.follow_login .module_track_this').addClass('tracked');
			});
		});
	}
});


/* game_strategies-list.js */
/* 
 * JS code to implement tabs for GTRP M055 Game Strategies Tabbed Listings Template
 */
$(document).ready(function(){
	$Crabapple('.game_strategies-list .module_tabs').tabs('.game_strategies-list .game_strategies');
});/* globalheader.js */
var GtSearch = {
	options: {
		searchForm: '#site_search',
		webSearchEngine: $('#site_search .web').attr('data-search_engine'),
		searchStr: 'Search',
		gtSearchButton: '#site_search .gt',
		webSearchButton: '#site_search .web',
		searchInput: '#search_query'
	},
	init: function() {
		$(this.options.searchInput).val(this.options.searchStr);
		$(this.options.gtSearchButton).unbind().bind('click', $.proxy(function() {
			if ($(this.options.searchInput).val() != '' && $(this.options.searchInput).val() != this.options.searchStr) {
				$(this.options.searchForm).submit();
				return false;
			}
			else {
				return false;
			}
		}, this));
		$(this.options.webSearchButton).unbind().bind('click', $.proxy(function() {
			if ($(this.options.searchInput).val() != '' && $(this.options.searchInput).val() != this.options.searchStr) {
				location.href = this.options.webSearchEngine + $(this.options.searchInput).val().replace(/ /g,"+");
				return false;
			}
			else {
				return false;
			}
		}, this));
		$(this.options.searchInput).unbind().bind('blur', $.proxy(function() {
			if ($(this.options.searchInput).val() == '') {
				$(this.options.searchInput).val(this.options.searchStr);
			}
		}, this)).bind('focus', $.proxy(function () {
			if ($(this.options.searchInput).val() == this.options.searchStr) {
				$(this.options.searchInput).val('');
			}
		}, this));
	}
};

$(document).ready(function(){
	GtSearch.init();
});/* gt.carousel.js */
(function($) {
	$Crabapple.Carousel = function(){};

	$Crabapple.extend($Crabapple.Module, $Crabapple.Carousel, {
		timer: null,
		realSize: 0,
		jCarouselIndexAttr: 'jcarouselindex',
		
		options: {
	        vertical: false,
	        rtl: false,
	        start: 1,
	        offset: 1,
//	        size: null,
	        scroll: 3,
	        visible: null,
	        animation: 'normal',
	        easing: 'swing',
	        auto: 0,
	        wrap: null,
	        initCallback: null,
	        setupCallback: null,
	        reloadCallback: null,
	        itemLoadCallback: null,
	        itemFirstInCallback: null,
	        itemFirstOutCallback: null,
	        itemLastInCallback: null,
	        itemLastOutCallback: null,
	        itemVisibleInCallback: null,
	        itemVisibleOutCallback: null,
	        animationStepCallback: null,
	        buttonNextHTML: '<a href="javascript:" class="next">Next</a>',
	        buttonPrevHTML: '<a href="javascript:" class="prev">Previous</a>',
	        buttonNextEvent: 'click',
	        buttonPrevEvent: 'click',
	        buttonNextCallback: null,
	        buttonPrevCallback: null,
	        itemFallbackDimension: null,
	        rotateInterval: 5000, //additional params
	        freezeOnHover: true, //if true, rotation will stop on content hover acrion
	        nextButtonCallbackList: '', //list of callbacks likes: '_highlightFirstItem,_stopAutoScroll'
	        prevButtonCallbackList: '', //list of callbacks likes: '_highlightFirstItem,_stopAutoScroll'
	        hightLightFirstVisibleItem: false,
	        circularOnLast: false,
	        hideButtons: true // hide buttons prev|next if elements size is less or equal scroll value
		},

		init: function(elem, options)
		{
			if( this.$elem.attr('auto_rotate_interval') ){
				this.options.auto = this.$elem.attr('auto_rotate_interval');
			}
			this.realSize = $('li',this.$elem).size();
			
			if (this.options.hideButtons && this.options.scroll && this.options.scroll >= this.realSize){
				this.options.buttonNextHTML = '';
				this.options.buttonPrevHTML = '';
			}

			if( this.options.hightLightFirstVisibleItem ){
				this.options.itemFirstInCallback = jQuery.proxy(function(args){
					this._highlightFirstVisibilityElement(args);
				},this);
			}
			
			if( this.options.circularOnLast ){
				this.options.itemLastInCallback = jQuery.proxy(function(carousel, item, idx, state){
					this._circularOnLast(carousel, item, idx, state);
				},this);
			}
			
			this.$elem.jcarousel( this.options );
		    return this;
		},
		
		autoHighlight: function( interval )
		{
			this.options.rotateInterval = interval;
			$('li',this.$elem).closest('.middle').mouseover( jQuery.proxy(function(e){
				this._autoRotateStop()
			}, this )).mouseout(jQuery.proxy(function(e){
				this._autoRotateStart()
			}, this )
			).filter(':first').mouseout();	
			
			$('li',this.$elem).hover(jQuery.proxy(function(e){
				this._setActiveElement($('.active', this.$elem ), $(e.currentTarget) )
				this._highlightContentBlock();
			}, this ));
		},
		
		autoHighlightCallback: function( interval, callback )
		{
			this.options.rotateInterval = interval;
			$('li',this.$elem).closest('.middle').mouseover( jQuery.proxy(function(e){
				if (this.options.freezeOnHover) {
					this._autoRotateStop();
				}
			}, this )).mouseout(jQuery.proxy(function(e){
				if (this.options.freezeOnHover) {
					this._autoRotateStart();
				}
			}, this )
			).filter(':first').mouseout();	
			
			$('li',this.$elem).hover(jQuery.proxy(function(e){
				if (!$(e.currentTarget).hasClass('active')) {
					this._setActiveElement($('.active', this.$elem ), $(e.currentTarget) )
					this._highlightContentBlock();
					callback.call(this);
				}
			}, this ));
		},
		
		setFreezeOnHover: function ( value )
		{
			this.options.freezeOnHover = value;
		},
		
		_autoHihlightNextElement: function()
		{
			var currentElement 	= $('li.active');
			var allContainers	= $('li',this.$elem);
			if(!currentElement){
				$(allContainers).filter(':first').addClass('.active');
			} else {
				var currentIndex = currentElement.attr(this.jCarouselIndexAttr);
				var nextIndex = parseInt(currentIndex) + 1;
				
				if( currentIndex%this.options.scroll == 0 ) {
					if( this.options.buttonNextHTML ){
						$('.next',this.$elem.closest('.middle')).trigger(this.options.buttonNextEvent);
					} else {
						nextIndex = 1;
					}
				}
				var nextElement = $('li['+this.jCarouselIndexAttr+'="'+nextIndex.toString()+'"]')
				
				this._setActiveElement( allContainers, nextElement );
				this._highlightContentBlock();
			}
		},
		
		_highlightFirstVisibilityElement: function(args) 
		{
			var allContainers	= $('li',this.$elem);
			var firstVisbleElement = $('li['+this.jCarouselIndexAttr+'="'+args.first+'"]',this.$elem).filter(':last');
			
			this._setActiveElement( allContainers, firstVisbleElement );
			this._highlightContentBlock();
		},
		
		_highlightContentBlock: function()
		{
			var currentElement 	= $('li.active', this.$elem);
			var currentIndex = currentElement.attr(this.jCarouselIndexAttr);
			var contentElementIndex = 0;
			
			if( currentIndex <= this.realSize ) {
				contentElementIndex = currentIndex-1;
			} else {
				contentElementIndex = (currentIndex-1)%this.realSize;
			}

			var middleBlock = this.$elem.closest('.middle');
			var blockLiHeight = $("ul.area li", middleBlock).height();
			
			var topOffsetPx = parseInt(blockLiHeight) * contentElementIndex;
			
			if( parseInt(topOffsetPx)){
				topOffset = '-'+topOffsetPx+'px';
			} else {
				topOffset = '0';
			}
			
			$("ul.area", middleBlock).first().css('top', topOffset);
		},
		
		_autoRotateStart: function()
		{
			this.timer =  setInterval(jQuery.proxy(function(e){
				this._autoHihlightNextElement();
			}, this
			), this.options.rotateInterval);
		},
		
		_autoRotateStop: function()
		{
			this.timer = clearInterval(this.timer);
		},
		
		_setActiveElement: function(container, element)
		{
			container.removeClass('active');
			element.addClass('active');
		},
		
		_circularOnLast: function(carousel, item, idx, state)
		{
			if( idx >= this.realSize && this.realSize != this.options.scroll ){
				this.options.wrap 	= 'circular';
				this.$elem.jcarousel( this.options );
			}
			return this;
		}		

	});
	$.pluginize('carousel', $Crabapple.Carousel, $Crabapple);
}) (jQuery);

/* gt.click_tracking.js */
/*
$(document).ready(function(){
	$Crabapple('body').clickTracking({
		topNavigationSelector: '.top_navigation a',
		pageName : '',
		promoName: "disabled",
		linkName: 'top-navigation',
		linkType: 'o',
		eVar7: 'top-nav',
		prop25: 'top-nav',
		eVar9: 'top-nav'
	});
});
*//* gt.flux_widget.js */
(function($) {
	$GT.FluxWidget = function () {};

	$Crabapple.extend($Crabapple.FluxWidget, $GT.FluxWidget, {

		afterLoadWidgets: function (widgetObj, widget) {
			if (widget.children().length) {
				// set comments amount
				if ($('.activityFeed', widget).length) {

					widgetObj.onCommented = function() {
						$Crabapple.FluxWidget.requestContentFeed(widget.attr('data-contentUri'), function (data) {
							if (data.NumberOfComments == 1) {
								$('.module_header .description').text(data.NumberOfComments + " comment");
							} else {
								$('.module_header .description').text(data.NumberOfComments +" comments");
							}
						});
					};

					// post comments amount on load
					widgetObj.onCommented();
				}
			} else {
				setTimeout($.proxy(function() { this.afterLoadWidgets(widgetObj, widget); }, this), 500);
			}
		},

		init: function () {},
		/**
		 * get widget configuration by widget type
		 */
		getWidgetConfig: function (widget) {
			var opts = {};

			switch (widget.attr('data-widget')) {
				case 'ActivityFeed':
					opts = this.getActivityFeedConfig(widget);
					break;
				case 'Share':
					opts = this.getShareConfig(widget);
					break;
				case 'shareDropDown':
					opts = this.getShareDropDownConfig(widget);
					break;
				case 'UserBar':
					opts = this.getUserBarConfig(widget);
					break;
				case 'ContentAction':
					opts = this.getUserBarConfig(widget);
					break;
				defaut:
					break;
			}
			return opts;
		},
		/**
		 * get activity feed configuration for GT
		 */
		getActivityFeedConfig: function (widget) {
			return {
				commentMessage: widget.attr('comment-message'),
				contentUri: widget.attr('data-contentUri'),
				enabledFilters: ['Comments'],
				title: widget.attr('data-title'),
				sortingVisible: widget.attr('sorting-visible'),
				maxResults: widget.attr('max-results'),
				filterVisible: false,
				defaultFilter: 'Comments',
				paginationType: widget.attr('pagination-type')
			};
		},
		/**
		 * get confgutarion for share button and pushed it to array
		 */
		getShareConfig: function (widget) {
			var elements = new Array();

			var shareElements = widget.attr('data-shares').split(",");

			for(var i=0; i< shareElements.length; i++){

				if( shareElements[i] != "Facebook" && shareElements[i] != "FacebookList" ){
					elements.push({ id: shareElements[i]});
				} else {
					var sendButton = true;
					if( shareElements[i] == "FacebookList") {
						sendButton = false;
					}

					elements.push({
						id: 'Facebook',
						type: 'fblike',
						colorScheme: 'light',
						send: sendButton,
						layout: widget.attr('facebook-layout') ? widget.attr('facebook-layout'):'standart'
					});
				}
			}
			return {
				contentUri: widget.attr('data-contentUri'),
				layout: 'horizontal',
				elements: elements,
				title: ' '
			}
		},

		/**
		 * get confgutarion for drop down menu with share buttons
		 */
		getShareDropDownConfig: function (widget) {
			return {
				contentUri: widget.attr('data-contentUri'),
				layout: 'horizontal',
				elements: [
					{ id: 'Facebook'},
					{ id: 'Twitter' },
					{ id: 'Digg' },
					{ id: 'StumbleUpon'},
					{ id: 'Tumblr'},
					{ id: 'GoogleBuzz'}
				]
				
			}
		},

		/**
		 * get confgutarion for user bar
		 */
		getUserBarConfig: function (widget) {
			return {
				displayMode: 'EmbeddedTop',
				elements: [{
					id: 'MyStuff',
					showThumbnail: true
				}]
			};
		},
		/**
		 * get confgutarion for content action
		 */
		getContentActionConfig: function (widget) {
			return { 
				contentUri: widget.attr('data-contentUri'),
				items: [
					{ id: 'contentRating', type: 'thumbsUpDown', title: { thumbsUpTitle: '', thumbsDownTitle: '' }},
					{ id: 'rate', ratingSystem: 'thumbsUpDown'}

			]};
		},
		/**
		 * get widget name/type
		 */
		getWidgetName: function (widget) {
			if (widget.attr('data-widget') == 'shareDropDown') {
				return 'Share';
			} else {
				return widget.attr('data-widget');
			}
		}
	});

	$.pluginize('fluxWidget', $GT.FluxWidget, $GT);
}) (jQuery);/* gt.game_contribute-ratings.js */
/*
 * Grab user rating stats from Flux and submit voting result for Module M018 Game Contribue Rating.
 * When module loaded we send DAAPI request to Flux and grab number of votes for our content, 
 * average user game rating and number of user reviews and uploaded videos.
 * When user set rating for this game with slider we check user login to flux, submit his rating and refresh stats.
 * If user click at 'Write a review' or 'Upload a Video' we provide him flux overlay with apportunity to send UGT or UGV
 */
if($('.video_game_contribute-ratings').length != 0) {
	$GT.videoGameContribute = {
		options : {
			fluxRequestPath: $('.ratings').attr('data-fluxBaseHref') + '/2.0/00001/json/',
			currentUserUcid: null,
			comunityId: $('.ratings').attr('data-communityId'),
			contentId: $('.ratings').attr('data-contentUri'),
			fluxRequest: null,
			totalVotesContainer: '.users .votes',
			averageScoreContainer: '.users .user_rating strong',
			writeReviewButton: '#writeReview',
			uploadVideoButton: '#uploadVideo',
			reviewsCount: '#reviews_count',
			videosCount: '#videos_count',
			thisToText: '$GT.videoGameContribute',
			UGCCount: null,
			userScore: '.ratings .your p strong'
		},
		
		init: function () {
			this.options.fluxRequest = this.options.fluxRequestPath + this.options.comunityId;
			this.buildingData();
			this.addingHandlers();
			this.getStats();
		},
		
		buildingData: function () {
			this.fluxInit();
		},
		
		votingStats: function (){
			$.getScript(this.options.fluxRequest + '/feeds/ClassifierStats?q=' + this.options.contentId + '&callback=' + this.options.thisToText + '.gtrpGetVotingStats');
		},
		
		getUserScore: function () {
			self = this;
			if (self.options.currentUserUcid != null) {
				$.getScript(self.options.fluxRequest + '/feeds/widgets4Context?callback=' + self.options.thisToText + '.userLoginCheck', function() {
					$.getScript(self.options.fluxRequest + '/feeds/classifierStats?q=' + self.options.contentId + '&voterUcid=' + self.options.currentUserUcid + '&callback=' + self.options.thisToText + '.setUserScore');
				});
			} else {
				$(self.options.userScore).text('N/A');
				$(self.options.userScore).show();
			}
		},
		
		userLoginCheck: function (context){
			if (context.user){
				this.options.currentUserUcid = context.user.ucid;
			}
		},
		
		getStats: function () {
			var self=this;
			this.votingStats();
			$.getScript(this.options.fluxRequest + '/feeds/posts/?parentContent=' + this.options.contentId + '&includeTotalCount=true&callback=' + this.options.thisToText + '.gtrpGetUGTandUGVStats', function() {
				$(self.options.reviewsCount).text(self.options.UGCCount + ' user reviews');
			});
			$.getScript(this.options.fluxRequest + '/feeds/videos/?parentContent=' + this.options.contentId + '&includeTotalCount=true&callback=' + this.options.thisToText + '.gtrpGetUGTandUGVStats', function() {
				$(self.options.videosCount).text(self.options.UGCCount + ' user movies');
			});
		},
		
		setUserScore: function (classifier) {
			var api = $(':range').data('rangeinput');
			if (classifier.Items.length > 0){
				console.log("user");
				$(self.options.userScore).text(classifier.Items[0].Title);
				api.setValue(classifier.Items[0].Title);
			}
			else {
				console.log("no user");
				$(self.options.userScore).text('N/A');
			}
			$(self.options.userScore).show();
		},
		
		gtrpGetVotingStats: function (classifier) {
			var votesCount = 0;
			var allScore = 0;
			var averageScore = 'N/A';
			if(classifier.Items.length > 0 ) {
				$.each(classifier.Items, function() {
					if(!isNaN(this.Title)) {
						votesCount += this.Total;
						allScore += (this.Title*1) * this.Total;
					}
				});
				if (!isNaN(allScore)) {
					averageScore = (allScore/votesCount).toFixed(1);
					if (averageScore >= 10.0) {
						averageScore = 10;
					}
				} else {
					votesCount = 0;
				}
			}
			$(this.options.totalVotesContainer).text(votesCount + ' votes');
			$(this.options.averageScoreContainer).text(averageScore);
		},
			
		gtrpGetUGTandUGVStats: function (ugc){
			this.options.UGCCount = ugc.TotalCount;
		},
		
		gtrpContext: function (context){
			if (context.user){
				this.options.currentUserUcid = context.user.ucid;
			} else {
				Flux4.performRoadBlockerCheck();
			}
		},
			
		addingHandlers: function() {
			var self = this;
			$(this.options.writeReviewButton).click(function(event) {
				$.getScript(self.options.fluxRequest + '/feeds/widgets4Context?callback=' + self.options.thisToText + '.gtrpContext', function() {
					$GT_flux.UserProfileWidget.openProfile(self.options.currentUserUcid, event, 'content:UGT', 'parentId:'+ self.options.contentId);
				});
			});

			$(this.options.uploadVideoButton).click(function(event) {
				$.getScript(self.options.fluxRequest + '/feeds/widgets4Context?callback=' + self.options.thisToText + '.gtrpContext', function() {
					$GT_flux.UserProfileWidget.openProfile(self.options.currentUserUcid, event, 'content:UGV', 'parentId:'+ self.options.contentId);
				});
			});
		},
		
		fluxInit: function () {
			var self = this;
			var aliasArray = {};
			if ($('.ratings').attr('data-stage') == 'live') {
				aliasArray = {
					2: 57,
					3: 58,
					4: 59,
					5: 60,
					6: 61,
					7: 62,
					8: 63,
					9: 64,
					10: 65
				};
			}
			else {
				aliasArray = {
					2: 83,
					3: 84,
					4: 85,
					5: 86,
					6: 87,
					7: 88,
					8: 89,
					9: 90,
					10: 91
				};
			}
			var counter = 1;
			for(elements in aliasArray) {
				counter++;
			}
			var items = [];
			for(var i=0;i<counter;i++) {
				var currentNumber = i+2;
				items.push({ type: 'button', counterId: aliasArray[currentNumber]});
			}
			Flux.createWidget('CustomCounts', {
				contentUri: this.options.contentId,
				containerId: 'rating',
				layout: 'vertical',
				items: items
			}, function (widget) {
				function slider () {
					if($('.actionable').length != 0) {
						$Crabapple(':range').rating({
							valueArea: self.options.userScore
						});
						var api = $(':range').data('rangeinput');
						api.setValue(5.5);
						self.getUserScore();
						$('.slider .handle').click(function(e) {
							$.getScript(self.options.fluxRequest + '/feeds/widgets4Context?callback=' + self.options.thisToText + '.gtrpContext', function() {
								if(api.getValue() == '1' || self.options.currentUserUcid == null) { return }
								$('input.actionable[counterId="'+ aliasArray[api.getValue()] +'"]').click();
							});
							e.stopPropagation();
							setTimeout(function(){self.votingStats();}, 4000);
							clearTimeout(self.votingStats);
						});
						$('.slider').click(function() {
							$.getScript(self.options.fluxRequest + '/feeds/widgets4Context?callback=' + self.options.thisToText + '.gtrpContext', function() {
								if(api.getValue() == '1' || self.options.currentUserUcid == null) { return }
								$(self.options.userScore).text(api.getValue());
								$('input.actionable[counterId="'+ aliasArray[api.getValue()] +'"]').click();
							});
							setTimeout(function(){self.votingStats();}, 4000);
							clearTimeout(self.votingStats);
						});
					}
					else {
						setTimeout(slider, 100);
					}
				}
				slider();
			});
		}
	}
	$GT.videoGameContribute.init();
}/* gt.guides.js */
$(document).ready(function(){
	$Crabapple('.guides ul.items').carousel({
		wrap: 'circular',
		scroll: 3
	});
});/* gt.hp_carousel.js */
(function($) {
	$Crabapple.CarouselHp = function(elem, options){
		this.options = {
	        vertical: false,
	        rtl: false,
	        start: 1,
	        offset: 1,
//	        size: null,
	        scroll: 3,
	        visible: null,
	        animation: 'normal',
	        easing: 'swing',
	        auto: 0,
	        wrap: null,
	        initCallback: null,
	        setupCallback: null,
	        reloadCallback: null,
	        itemLoadCallback: null,
	        itemFirstInCallback: null,
	        itemFirstOutCallback: null,
	        itemLastInCallback: null,
	        itemLastOutCallback: null,
	        itemVisibleInCallback: null,
	        itemVisibleOutCallback: null,
	        animationStepCallback: null,
	        buttonNextHTML: '<div class="next">Next</div>',
	        buttonPrevHTML: '<div class="prev">Previous</div>',
	        buttonNextEvent: 'click',
	        buttonPrevEvent: 'click',
	        buttonNextCallback: null,
	        buttonPrevCallback: null,
	        itemFallbackDimension: null,
	        rotateInterval: 5000, //additional params
	        nextButtonCallbackList: '', //list of callbacks likes: '_highlightFirstItem,_stopAutoScroll'
	        prevButtonCallbackList: '', //list of callbacks likes: '_highlightFirstItem,_stopAutoScroll'
	        hightLightFirstVisibleItem: false,
	        circularOnLast: false,
	        hideButtons: true, // hide buttons prev|next if elements size is less or equal scroll value
			  freezeOnHover: true, //if true, rotation will stop on content hover acrion
			  playerRun: false
		};
		return this;
	};

	$Crabapple.CarouselHp.prototype = {
		timer: null,
		realSize: 0,
		jCarouselIndexAttr: 'jcarouselindex',
		
		
		
		
		init: function(elem, options)
		{
			this.elem = elem;
			this.$elem = $(elem);
			$.extend(this.options, options);
			this.arrayOfCarousels = {};
			var _options = this.options
			if( this.$elem.attr('auto_rotate_interval') ){
				this.options.auto = this.$elem.attr('auto_rotate_interval');
			}
			this.realSize = $('li',this.$elem).size();
			
			if (this.options.hideButtons && this.options.scroll && this.options.scroll >= this.realSize){
				this.options.buttonNextHTML = '';
				this.options.buttonPrevHTML = '';
			}

			if( this.options.hightLightFirstVisibleItem ){
				this.options.itemFirstInCallback = jQuery.proxy(function(args){
					this._highlightFirstVisibilityElement(args);
				},this);
			}
			
			if( this.options.circularOnLast ){
				this.options.itemLastInCallback = jQuery.proxy(function(carousel, item, idx, state){
					this._circularOnLast(carousel, item, idx, state);
				},this);
			}
			var self = this;
			(function(options) {
				if(!(self.arrayOfCarousels.hasOwnProperty(self.$elem[0].className))) {
				var carousel = self.$elem.jcarousel( options );
				self.arrayOfCarousels[self.$elem[0].className] = {obj: carousel, options: options};
				} else {
					console.log('ERROR');
				}
			})(_options);
			
		    return this;
		},
		
		autoHighlight: function( interval, callback, scope)
		{
			var self = this;
			this.options.rotateInterval = interval;
			$('li',this.$elem).closest('.middle').mouseover( jQuery.proxy(function(e){
				if (this.options.freezeOnHover) {
					this._autoRotateStop();
				}
			}, this )).mouseout(jQuery.proxy(function(e){
				if (this.options.freezeOnHover) {
					this._autoRotateStart();
				}
			}, this )
			).filter(':first').mouseout();

			$('li',this.$elem).hover(jQuery.proxy(function(e){
				this._setActiveElement($('.active', this.$elem ), $(e.currentTarget) )
				this._highlightContentBlock();
				if (callback) {
					callback.apply(scope);
				}
			}, this ));
		},
		
		_autoHihlightNextElement: function()
		{
			var currentElement 	= $('li.active', this.$elem);
			var allContainers	= $('li',this.$elem);
			if(!currentElement){
				$(allContainers).filter(':first').addClass('.active');
			} else {
				var currentIndex = currentElement.attr(this.jCarouselIndexAttr);
				var nextIndex = parseInt(currentIndex) + 1;
				
				if( currentIndex%this.getCarouselOptions(this.$elem).scroll == 0 ) {
					if(this.getCarouselOptions(this.$elem).buttonNextHTML ){
						$('.next',this.$elem.closest('.middle')).trigger(this.getCarouselOptions(this.$elem).buttonNextEvent);
					} else {
						nextIndex = 1;
					}
				}
				var nextElement = $('li['+this.jCarouselIndexAttr+'="'+nextIndex.toString()+'"]', this.$elem)
				
				this._setActiveElement( allContainers, nextElement );
				this._highlightContentBlock();
			}
		},
		
		_highlightFirstVisibilityElement: function(args) 
		{
			var allContainers	= $('li',this.$elem);
			var firstVisbleElement = $('li['+this.jCarouselIndexAttr+'="'+args.first+'"]',this.$elem).filter(':last');
			
			this._setActiveElement( allContainers, firstVisbleElement );
			this._highlightContentBlock();
		},
		
		_highlightContentBlock: function()
		{
			var currentElement 	= $('li.active', this.$elem);
			var currentIndex = currentElement.attr(this.jCarouselIndexAttr);
			var contentElementIndex = 0;
			
			if( currentIndex <= this.realSize ) {
				contentElementIndex = currentIndex-1;
			} else {
				contentElementIndex = (currentIndex-1)%this.realSize;
			}

			var middleBlock = this.$elem.closest('.middle');
			var blockLiHeight = $("ul.area li", middleBlock).height();
			
			var topOffsetPx = parseInt(blockLiHeight) * contentElementIndex;
			
			if( parseInt(topOffsetPx)){
				topOffset = '-'+topOffsetPx+'px';
			} else {
				topOffset = '0';
			}
			
			$("ul.area", middleBlock).first().css('top', topOffset);
		},
		
		_autoRotateStart: function()
		{
			this.timer =  setInterval(jQuery.proxy(function(e){
				this._autoHihlightNextElement();
			}, this
			), this.options.rotateInterval);
		},
		
		_autoRotateStop: function()
		{
			this.timer = clearInterval(this.timer);
		},
		
		_setActiveElement: function(container, element)
		{
			container.removeClass('active');
			element.addClass('active');
		},
		setFreezeOnHover: function(value)
		{
			this.options.freezeOnHover = value;
		},
		
		_circularOnLast: function(carousel, item, idx, state)
		{
			if( idx >= (this.realSize - this.options.scroll) && this.realSize != this.options.scroll ){
				this.options.wrap = 'circular';
				this.$elem.jcarousel( this.options );
			}
			return this;
		},
		getCarouselInstance: function(elem) {
			return this.arrayOfCarousels[elem[0].className].obj;
		},
		
		getCarouselOptions: function(elem) {
			return this.arrayOfCarousels[elem[0].className].options;
		}

	};
}) (jQuery);/* gt.line_listing_filters.js */
/**
 * plugin for line listing filter (M113 fragment)
 */
(function($) {
	$GT.LineListingFilters = function(){};

	$Crabapple.extend($Crabapple.Module, $GT.LineListingFilters, {

		options: {
			filters : {},
			init: $.noop,
			click: $.noop
		},

		disable: function(filterName, filterValue){
			if (!this.options.filters[filterName]) {
				return;
			}
			if (this.options.filters[filterName][filterValue.toLowerCase()]) {
				delete this.options.filters[filterName][filterValue.toLowerCase()];
			}
		},

		enable: function(filterName, filterValue){
			if (!this.options.filters[filterName]) {
				this.options.filters[filterName] = {};
			}
			this.options.filters[filterName][filterValue.toLowerCase()] = true;
		},

		isFilteredBy : function(filterName, filterValue)
		{
			return (this.options.filters[filterName] && this.options.filters[filterName][filterValue.toLowerCase()]);
		},

		getFilter : function(filterName)
		{
			return this.options.filters[filterName];
		},

		init: function(elem, options)
		{
			$('ul li span', elem).click(function(e){
				e.stopPropagation();

				var node = $(e.target);

				// if we click on child span
				if (node.parent().is('span'))
				{
					node = node.parent();
				}

				var filterName = node.attr('name'),
					filterValue = node.attr('value');

				if (node.hasClass('selected')) {
					this.disable(filterName, filterValue);
					node.removeClass('selected');
				} else {
					this.enable(filterName, filterValue);
					node.addClass('selected');
				}

				this.options.click.bind(this)();
			}.bind(this));

			// call module initialization callback
			this.options.init.bind(this)();
		}
	});
	$.pluginize('lineListingFilters', $GT.LineListingFilters, $GT);
}) (jQuery);/* gt.line_listing_filters_ajax.js */
/**
 * Description: plugin for line listing filter and sorting
 * Module:      M005, M034, M113
 */
(function($){
	$GT.Filter = function(){};
	$Crabapple.extend($Crabapple.Module, $GT.Filter, {
		options: {
			reloadArea: null,
			fragmentLink: null,
			paginationHolder: null,
			spinnerTemplate: "<div class='ajax_spinner'><img src='/sitewide/images/shared/ajax-loader.gif' width='32' height='32' alt='Loading...' /></div>",
			filterItem: null,
			click: $.noop,
			requestParams: {},
			ajaxObj: null
		},
		init: function(options){
			var self = this;
			this.paginationBuild();
			this.initHash();
			this.hashListener();
			$('.module_header .sort span').click(function(){
				$(this).addClass('current').siblings().removeClass();
				self.options.requestParams.sortBy = $(this).attr('data-sort');
				if(self.options.requestParams.currentPage){
					self.options.requestParams.currentPage = 1;
				};
				self.filterResults();
			});
			$(this.options.filterItem).click(function(e){
				e.stopPropagation();
				var node = $(e.target);
				if(node.parent().is('span')){
					node = node.parent();
				}
				if(node.attr('name') !== 'time'){
					if(node.hasClass('selected')){
						node.removeClass('selected');
					} else {
						node.addClass('selected');
					}
				} else {
					$(self.options.filterItem + '[name="time"]').removeClass('selected');
					if (!node.hasClass('selected')){
						node.addClass('selected');
					}
				}
				self.makeQuery();
				self.filterResults();
				self.options.click.bind(self)();
			});
		},
		paginationBuild: function(){
			$Crabapple(this.options.paginationHolder).pagination({
				reloadArea: this.options.reloadArea,
				fragmentLlink: this.options.fragmentLink,
				paginationHolder: this.options.paginationHolder,
				spinnerTemplate: this.options.spinnerTemplate,
				pequestParams: this.options.requestParams
			});
		},
		makeQuery: function(options){
			var self = this;
			$(this.options.filterItem).each(function(){
				delete self.options.requestParams[$(this).attr('name')];
			});
			$('#utils_filter ul > li').each(function(){
				$(this).find('.filter_group span.selected').each(function(){
					if(self.options.requestParams[$(this).attr('name')]){
						self.options.requestParams[$(this).attr('name')] = self.options.requestParams[$(this).attr('name')] + ', ' + $(this).attr('value');
					} else {
						self.options.requestParams[$(this).attr('name')] = $(this).attr('value');
					};
				});
			});
			if(this.options.requestParams.currentPage){
				this.options.requestParams.currentPage = 1;
			};
		},
		ajaxReloadActions: function(data){
			$(this.options.reloadArea).empty();
			$(this.options.reloadArea).html(data);
		},
		filterResults: function(options){
			$(this.options.reloadArea).html(this.options.spinnerTemplate);
			if(this.options.ajaxObj){
				this.options.ajaxObj.abort();
			}
			this.options.ajaxObj = $.ajax({
				url: this.options.fragmentLink,
				type: 'GET',
				data: this.options.requestParams,
				context: this,
				success: function(data){
					this.ajaxReloadActions(data);
					this.paginationBuild();
				},
				error: function(data){}
			});
		},
		initHash: function(){
			var hash = location.hash.match(/^#?(.*)$/)[1];
			if(hash){
				this.makeHashQuery(hash);
			};
		},
		hashListener: function(){
			var self = this;
			if(location.hash.match(/^#?(.*)$/)[1]){
				$('.dropdown_holder dd a').click(function(){
					if($(this).attr('href').match(/(.*)#.*/)[1] == location.href.match(/(.*)#.*/)[1]){
						var hash = $(this).attr('href').match(/(.*)#(.*)/)[2];
						self.makeHashQuery(hash);
					};
				});
			};
		},
		makeHashQuery: function(hash){
			if(hash == 'trailers'){
				hashTag = 'trailer';
			} else if(hash == 'previews'){
				hashTag = 'preview';
			} else if(hash == 'reviews'){
				hashTag = 'review';
			};
			$(this.options.filterItem).each(function(){
				$(this).removeClass('selected');
				if($(this).attr('value') && $(this).attr('value').toLowerCase() == hashTag.toLowerCase()){
					$(this).addClass('selected');
				};
			});
			this.makeQuery();
			this.filterResults();
		}
	});
	$.pluginize('lineListingFiltersAJAX', $GT.Filter, $GT);
})(jQuery);/* gt.page_navigation.js */
/*
 * JS code to implement navigation tab selection.
 * This js checks the browser url to match established regular expression (videos, reviews, etc.)
 * and determines the regular expression which will be used to compare each url of the menu items elements.
 * Checking all of the menu items hrefs to match determined regular expression, the class "current" is added to
 * the matching element. If any href matches the regular expression, home link is highlighted.
 * Tested on following urls:
 * Home http://www.gametrailers.com/games/halo-reach/3g4s1i
 * Videos http://www.gametrailers.com/games/mass-effect-3/videos
 * Reviews http://www.gametrailers.com/reviews/mass-effect-3-review/s5d4e7 or http://www.gametrailers.com/reviews/mass-effect-3-review/7w8e9d
 * or http://www.gametrailers.com/reviews/mass-effect-3-review/user-reviews
 * Guide http://www.gametrailers.com/games/halo-reach/guide
 * Blog http://www.gametrailers.com/games/mass-effect-3/blog
 * Photos http://www.gametrailers.com/shows/gttv/fe4te2/best-gallery-ever/1 or
 * http://www.gametrailers.com/games/halo-reach/fe4t2s/halo-reach-screenshots/1
 * DLC http://www.gametrailers.com/games/mass-effect-3/dlc/
 * Forums http://forums.gametrailers.com/
 */
(function($) {
	$GT.PageNavigation = function(){};
	$Crabapple.extend($Crabapple.Module, $GT.PageNavigation, {

		options: {target:false},

		url: document.location.href,

		clearURL : function(url) {
			if (url.indexOf('?') > 0) {
				url = url.substr(0, url.indexOf('?'));
			}
			if (url.indexOf('#') > 0) {
				url = url.substr(0, url.indexOf('#'));
			}
			if (url.substr(-1) == '/') {
				url = url.substr(0, url.length-1);
			}
			return url;
		},

		init: function(elem, options) {
			
			var targetModule = $(options.target);

			if(targetModule.length != 0){
				/*Subheader navigation fix for Review page*/				
				var subHeader = $('a', this.$elem).removeClass('current');
				targetModule = $('a', targetModule);
				subHeader.each(function(index,subHeaderNavItem){
					var subHeaderUrl = $(subHeaderNavItem).attr('href');
					targetModule.each(function(index,reviewModuleNavItem){
						if(subHeaderUrl == $(reviewModuleNavItem).attr('href')){
							$(subHeaderNavItem).addClass('current');
							return;
						}
					});
				});
			}else {
				this.url = this.clearURL(this.url);
				var navItems = $('a', this.$elem);
				navItems.each(function(id, link) {
					var url = this.clearURL($(link).attr('href'));
					if (this.url == url) {
						$(link).addClass('current');
					} else {
						$(link).removeClass('current');
					}
				}.bind(this));
			}
		}
	});
	$.pluginize('pageNavigation', $GT.PageNavigation, $GT);
}) (jQuery);
/* gt.photo_gallery.js */
(function($) {
	$GT.photoGallery = function () {};
	$Crabapple.extend($Crabapple.photoGallery, $GT.photoGallery, {
		options: {
			adHolder: null,
			adPusher: null
		},
		init: function (){
			this.pushAd();
		},
		pushDown: function(){
			this.parentClass.pushDown.apply(this, arguments);
			this.pushAd();
		},
		pushUp: function(){
			this.parentClass.pushUp.apply(this, arguments);
			this.pushAd();
		},
		pushAd: function(){
			$(this.options.adHolder).css('margin-top', $(this.options.adPusher).outerHeight() + 10);
		}
	});
	$.pluginize('photoGallery', $GT.photoGallery, $GT);
}) (jQuery);/* gt.player.js */
/* 
 * Embed player API for GT
 * used in m042
 */
(function($) {
	$Crabapple.Player = function () {};
	/*
	 * Variable that can be used to get access to embeded players by theirs IDs
	 **/
	$Crabapple.Player.players = {};

	$Crabapple.extend($Crabapple.Module, $Crabapple.Player, {

		players: function(id){
			if (id in $Crabapple.Player.players) {
				return $Crabapple.Player.players[id];
			} else {
				return undefiend;
			}
		},
		init: function (elem, options) {
			var paramList = {
				wmode: 'opaque',
				allowscriptaccess: 'always',
				allowfullscreen: true
			}
			var playerConfig = {
				width:$(elem).attr('data-width'),
				height:$(elem).attr('data-height'),
				uri:$(elem).attr('data-mgid'),
				flashVars:{
					autoPlay:$(elem).attr('data-autoplay'),
					sid:(typeof(siteSectionId) == 'undefined') ? 'The_Daily_Show_Home' : siteSectionId
				},
				params: paramList
			}
			var events = {
				onReady:(typeof (options) != "undefined" && typeof options.onReady == "function") ? options.onReady : this.onReady,
				onMetadata:(typeof (options) != "undefined" && typeof options.onMetadata == "function") ? options.onMetadata : this.onMetadata,
				onMediaEnd:(typeof (options) != "undefined" && typeof options.onMediaEnd == "function") ? options.onMediaEnd : this.onMediaEnd,
				onPlayheadUpdate:(typeof (options) != "undefined" && typeof options.onPlayheadUpdate == "function") ? options.onPlayheadUpdate : this.onPlayheadUpdate,
				onStateChange:(typeof (options) != "undefined" && typeof options.onStateChange == "function") ? options.onStateChange : this.onStateChange,
				onPlaylistComplete:(typeof (options) != "undefined" && typeof options.onPlaylistComplete == "function") ? options.onPlaylistComplete : this.onPlaylistComplete
			}

			$Crabapple.Player.players[$(elem).attr('id')] = new MTVNPlayer.Player(
				$(elem).attr('id'),
				playerConfig,
				events
			);
			//console.log($Crabapple.Player.players);
			return $Crabapple.Player.players;
		},
		/*
		 * Define here all the dafault events for MTVN Player
		 * example: $Crabapple('#video-player').player({
						onReady: function(){
							console.log("Ready");
						}
					});
		 */
		onReady:function(event){},
		onMetadata:function(event){},
		onMediaEnd:function(event){},
		onPlayheadUpdate:function(event){},
		onStateChange:function(event){},
		onPlaylistComplete:function(event){},
		/*
		 * Define here custom methods which can be launched when the player is callded
		 * example: $Crabapple('#video-player').player().player("playSDVideo",$someMgid);
		 */

		/* id - player id
		 * sdButtonId - SD button id
		 */
		playSDVideo: function (id, sdButtonId){
			var btn = $('#'+sdButtonId);
			btn.click(function() {
				if(!btn.hasClass("current")){
					$.cookie('gtUserChoice', 'sd');
					var videoObject = $('#'+id);
					videoObject.attr('width','768');
					videoObject.attr('height','432');
					videoObject.parent().css("height",432);
					btn.addClass("current");
					btn.next().removeClass("current");
				}
			});
		},
		/* id - player id
		 * hdButtonId - HD button id
		 */
		playHDVideo: function(id, hdButtonId){
			var btn = $('#'+hdButtonId);
			btn.click(function() {
				if(!btn.hasClass("current")){
					$.cookie('gtUserChoice', 'hd');
					var videoObject = $('#'+id);
					videoObject.attr('width','960');
					videoObject.attr('height','540');
					videoObject.parent().css("height",540);
					btn.addClass("current");
					btn.prev().removeClass("current");
				}
			});
		},
		/*
		 * Function that provides the palyer instance with specified ID
		 * if there is no player with such ID the function returns 'undefined'
		 **/
		getPlayerInstance: function(id, callback){
			callback($Crabapple.Player.players[id]);
		},
		/*
		 * Function that returns embed code of the player with specified ID
		 *
		 **/
		getEmbedCode: function(id, callback){
			callback($Crabapple.Player.players[id].getEmbedCode());
		}

	});

	$.pluginize('player', $Crabapple.Player, $Crabapple);

}) (jQuery);

/* gt.search_by_filters-list.js */
$(document).ready(function(){
	if($('.search_by_filters-line_listings').length) {
		var dataId = $('.search_results').attr('data-id');
		$GT(".search_by_filters-line_listings").lineListingFiltersAJAX({
			reloadArea: '.search_by_filters-line_listings .search_results',
			fragmentLink: '/feeds/search_by_filters/' + $('.search_results').attr('data-type') + '/' + $('.search_results').attr('promo-id') + (dataId ? '/' + dataId : ''),
			paginationHolder: '.search_by_filters-line_listings .pagination',
			filterItem: '#utils_filter .filter_group span'
		});
	};
});/* gt.search_results.js */
(function($){
	$GT.SearchResults = function(options){
		this.paginationInst = {};
		this.options = {
			tabClass: '.module_tabs',
			moduleClass: '.search_results-site_search',
			tabContent: '.search_results',
			searchInput: '.search',
			searchContainer: '#search_results_search',
			paginationDefine: false,
			seeAllContainer: '.module_see_all',
			ajaxObj: null,
			sortBy: 'most_recent',
			platformName: '',
			spinnerTemplate: "<div class='ajax_spinner'><img src='/sitewide/images/shared/ajax-loader.gif' width='32' height='32' alt='' border='0' /></div>"
		}
		$.extend(this.options, options);
	};
	
	$GT.SearchResults.prototype = {
		init: function(){
			if ($(this.options.moduleClass).length != 0) {
				this.buildItems();
				this.attachHandlers();
			} else {return}
		},
		createQueries: function(){
			this.moduleQuery = this.options.moduleClass + ' ' + this.options.tabClass;
			this.tabContentQuery = this.options.moduleClass + ' ' + this.options.tabContent;
			this.searchQuery = this.options.moduleClass + ' ' + this.options.searchInput;
			this.searchContainerSelectorQuery = this.options.moduleClass + ' ' + this.options.searchContainer;
		},
		buildItems: function(){
			this.createQueries();
		},
		_getStaticTabId: function(btn){
			return $(btn).parent().parent().parent().attr('id');
		},
		getTabId: function(btn){
				var tab$ = $(btn).parent();
				if(!$(btn).hasClass('current')) {
					tab$.siblings().children('a').removeClass('current');
					$(btn).addClass('current');
			}
			return tab$.attr('class').replace('tab_', '');
		},
		attachHandlers: function(){
			var self = this;
			$('.search_results-site_search .middle .module_tabs li a').click(function(){
				self.options.platformName = '';
				self.options.sortBy = 'most_recent'
				self.ajaxTabLoad(self.getTabId(this), self.options.platformName, self.options.sortBy);
				return false;
			});
			$(this.searchContainerSelectorQuery + ' .gt').click(function(){
				$(self.options.searchContainer).submit();
			});
			$(this.searchContainerSelectorQuery + ' .web').click(function(){
				location.href = $(this).attr('data-search_engine') + $(self.searchQuery).val().replace(/ /g,"+");
			});
			this.attachMoreLinks();
		},
		attachMoreLinks: function () {
			var string = this.tabContentQuery + ' ' + this.options.seeAllContainer;
			this.seeMoreBox = $(string);
			var self = this;
			$(this.seeMoreBox.find('a')).each(function(){
				$(this).click(function(e){
					var tabId = self._getStaticTabId(this);
					var string = self.options.tabClass + ' .tab_' + tabId + ' a';
					$(string).click();
					$(document).scrollTop(0);
					return false;
				})
			});
		},
		paginationInit: function(tabId, tabContent, platformName, sortBy){
			var self = this;
			var options = {
				reloadArea: tabContent,
				fragmentLlink: '/fragments/search/child',
				pequestParams: {
					keywords: this.getKeywords(),
					tabName: tabId,
					platforms: platformName,
					sortBy: sortBy
				},
				paginationHolder: '.pagination',
				spinnerTemplate: self.options.spinnerTemplate,
				afterLoadHandler: self.reloadFilters,
				afterLoadHandlerContext: self,
				scrollToTop: true
			}
			var string = this.tabContentQuery + ' ' + options.paginationHolder;
			var elem = $(string)[0];
			if (this.options.paginationDefine) {
				this.paginationInst._reload(elem, options);
			} else {
				this.paginationInst = new $Crabapple.instantiate($Crabapple.Pagination, [elem, options]);
				this.options.paginationDefine = true;
			}
		},
		ajaxTabLoad: function (tabId, platformName, sortBy) {
			var self = this;
			$(this.tabContentQuery).html(this.options.spinnerTemplate);
			if(this.options.ajaxObj){
				this.options.ajaxObj.abort();
			}
			this.options.ajaxObj = $.ajax({
				url: '/fragments/search/child?keywords=' + this.getKeywords() + '&tabName=' + tabId + '&platforms=' + platformName + '&sortBy=' + sortBy,
				type: 'GET',
				context: $(self.tabContentQuery),
				success: function(data){
					$('.search_results-site_search .middle .search_results').html(data);
					self.setPlatformsFilter();
					self.setSortingFilter();
					self.paginationInit(tabId, self.tabContentQuery, platformName, sortBy);
					self.attachMoreLinks();		
				}
			});
		},
		setPlatformsFilter: function() {
			var self = this;
			$(this.tabContentQuery + ' .platforms_filter span').click(function(){
				if(!$(this).hasClass('current')){
					$(this).addClass('current').siblings().removeClass();
					var platformName = $(this).data('sort');
					if (platformName == 'all'){
						platformName = '';
					}
					self.options.platformName = platformName;
					var currentTab = $(self.moduleQuery + ' li').siblings().children('a.current');
					self.ajaxTabLoad(self.getTabId(currentTab), self.options.platformName, self.options.sortBy);
				}
			return false;
			});
		},
		setSortingFilter: function() {
			var self = this;
			$(this.tabContentQuery + ' .module_header .sort span').click(function(){
				self.options.sortBy = $(this).data('sort');
				$(this).addClass('current').siblings().removeClass();
				var currentTab = $(self.moduleQuery + ' li').siblings().children('a.current');
				self.ajaxTabLoad(self.getTabId(currentTab), self.options.platformName, self.options.sortBy);
				return false;
			});
		},
		getKeywords: function(){
			return $(this.searchQuery).attr('data-keywords');
		},
		reloadFilters: function(){
			this.setPlatformsFilter();
			this.setSortingFilter();
		}
	};
})(jQuery);
/* gt.slider.js */
(function($) {
	$Crabapple.Rating = function(){};

	$Crabapple.extend($Crabapple.Module, $Crabapple.Rating, {
		
		options: {
			disabled: false,
			max: 10,
			min: 0,
			speed: 200,
			progress: false,
			step: 0.1,
			vertical: false,
			valueArea: null
		},
		slider: null,

		init: function(elem, options)
		{
			this.options.onSlide = jQuery.proxy( function(e, currentValue ){
				this.setRating(currentValue);
			}, this);
			this.slider = this.$elem.rangeinput( this.options);
			this.setRating( this.$elem.attr("value") );
			return this;
		},

		setRating: function( value )
		{
			$(this.options.valueArea).html(value);
		},
		
		getValue: function()
		{
			return this.$elem.slider("value");
		}
	});
		
	$.pluginize('rating', $Crabapple.Rating, $Crabapple);
}) (jQuery);/* gt.tooltip.js */
(function($) {
	$Crabapple.Tooltip = function(){};

	$Crabapple.extend($Crabapple.Module, $Crabapple.Tooltip, {
		
		init: function () {
			$(this.options.target).mousemove($.proxy(this.pos, this)).mouseover($.proxy(this.show, this)).mouseleave($.proxy(this.hide, this));
			
			this.target  = this.options.target;
			this.tooltip = this.options.tooltip;
		},
		
		options: {
			target:  '.target',
			tooltip: '.tooltip'
		},

		isIE: document.all ? true : false,
		tooltipIndex: 0,
		
		show: function(event){
			this.tooltipIndex = $(this.target).index($(event.currentTarget));
			$(this.tooltip).eq(this.tooltipIndex).show();
		},
		
		hide: function(){
			$(this.tooltip).hide();
		},
		
		pos: function(event){
			var top       = this.isIE ? event.clientY + document.documentElement.scrollTop : event.pageY;
			var left      = this.isIE ? event.clientX + document.documentElement.scrollLeft : event.pageX;
			var height    = $(this.tooltip).eq(this.tooltipIndex).outerHeight();
			var relative  = $(this.tooltip).eq(this.tooltipIndex).offsetParent();
			var relativeY = parseInt(relative.offset().top);
			var relativeX = parseInt(relative.offset().left);
			
			$(this.tooltip).eq(this.tooltipIndex).css('top', (top - (height / 2) - relativeY));
			$(this.tooltip).eq(this.tooltipIndex).css('left', (left + 15 - relativeX));
		}
		
	});
		
	$.pluginize('tooltip', $Crabapple.Tooltip, $Crabapple);
}) (jQuery);

/* gt.top_games.js */
/**
 * Description: AJAX tabs for Top Games Ranking Module
 * Module:      M084
 */
(function($){
	$GT.Platforms = function(){};
	$Crabapple.extend($Crabapple.Module, $GT.Platforms, {
		options: {
			reloadArea: '.top_games .tabs_container',
			spinnerTemplate: "<div class='ajax_spinner'><img src='/sitewide/images/shared/ajax-loader.gif' width='32' height='32' alt='Loading...' /></div>",
			ajaxObj: null
		},
		init: function(options){
			var self = this;
			$('.module_tabs.platform li').click(function(){
				self.spinnerCounter();
				$(this).addClass("current").siblings().removeClass();
				if($(this).attr('data-platform-id')){
					var platformId = '/' + $(this).attr('data-platform-id');
				} else {
					var platformId = '';
				};
				requestUrl = '/feeds/video_game_platform_top_video_games/' + $('.top_games').attr('data-promo-id') + platformId;
				self.ajaxRequest();
			});
		},
		ajaxRequest: function(options){
			$(this.options.reloadArea).html(this.options.spinnerTemplate);
			if(this.options.ajaxObj){
				this.options.ajaxObj.abort();
			};
			this.options.ajaxObj = $.ajax({
				url: requestUrl,
				type: 'GET',
				context: this,
				success: function(data){
					this.spinnerReset();
					$(this.options.reloadArea).html(data);
					$Crabapple('.top_games .module_tabs.sub').tabs('.top_games .subtabs_content');
				},
				error: function(data){}
			});
		},
		spinnerCounter: function(){
			spinnerAreaHeight = $('.top_games .tabs_container').height();
			spinnerAreaWidth = $('.top_games .tabs_container').width();
			$('.top_games .tabs_container').height(spinnerAreaHeight);
			$('.top_games .tabs_container').width(spinnerAreaWidth);
		},
		spinnerReset: function(){
			$('.top_games .tabs_container').height('');
			$('.top_games .tabs_container').width('');
		}
	});
	$.pluginize('platforms', $GT.Platforms, $GT);
})(jQuery);/* headline_articles.js */
/*
 * Used in headline_articles child fragment
 */
$(document).ready(function(){
	var module = $(".headline_articles");
	if(module.length !=0){
		$Crabapple.FluxWidget.requestCommentsFeed(module.attr('data-contentId'), 1, function(data){
			console.log(data);
			module.find(".info .counter").text(data.NumberOfComments)
		});
	}
});/* hpvideos.js */
$(function() {
	$Crabapple(".hpvideos").tooltip({
		target:  '.hpvideos .dont_miss .thumbnail',
		tooltip: '.hpvideos .dont_miss .tooltip'
	});
});/* most_watched_videos-list.js */
$(function() {
	$Crabapple('.most_watched_videos-list .module_tabs').tabs('.most_watched_videos-list .video_list');
});/* navigation.js */
// add active marker to navigation menu items
$(document).ready(function(){
	if($('.module.navigation').length) {
		var myUrlSource = location.pathname.slice(7);
		var myUrl = myUrlSource.match(/\w+/i);
		var stack = $('.navigation .middle ul');
		if(myUrl != null){
			stack.children().each(function(){
				if($(this).text().toLowerCase().indexOf(myUrl) >= 0) {
					$(this).addClass('active');
				}
			});
		}
		else {
			var parent = $('.navigation ul>li');
			$('a[href$="/about/"]', parent).parent().addClass('active');
		}
	}
 });/* news_ticker.js */
// for module M090

$(function() {
	var fade = 1;
	var delay = 7;
	
	var layout = $('.news_ticker');	
	var links = $('.news_ticker a');

	if (layout.data('fade'))
	{
		fade = layout.data('fade');
	}
	
	if (layout.data('delay'))
	{
		delay = layout.data('delay');
	}
	
	if (links.length > 0)
	{	
		var currentItem = 0;
		var lastItem = null;
		
		var circle = function(items, fade){
			$(items[lastItem]).fadeOut(fade);
			$(items[currentItem]).fadeIn(fade, function(){
				setTimeout(circle, delay, links, fade);
			});
			
			currentItem = (currentItem == (items.length - 1)) ? 0 : currentItem + 1;
			lastItem = (currentItem == 0) ? items.length - 1 : currentItem - 1;
			
		}
		
		delay = delay*1000;
		fade = fade*1000;
		
		circle(links, fade);
	}
});/* photo_gallery_body-player.js */
$(document).ready(function() {

	var drawPagesNavigation = function (carousel) {
		
		var currentPage = 1;
		var pageCheck = function () {
			var count = $('.photo_gallery_body-player ul.items > li').length;
			if(currentPage == 1){
				$('.photo_gallery_body-player .thumb_content .prev').addClass('disabled');
			} else {
				$('.photo_gallery_body-player .thumb_content .prev').removeClass('disabled');
			};
			if(currentPage == count){
				$('.photo_gallery_body-player .thumb_content .next').addClass('disabled');
			} else {
				$('.photo_gallery_body-player .thumb_content .next').removeClass('disabled');
			};
			$('.photo_gallery_body-player .page').removeClass('current').eq(currentPage - 1).addClass('current');
		};
		
		var pageCount = function() {
			var count = $('.photo_gallery_body-player ul.items > li').length;
			var res = '';
			for(var i=1; i<=count; i++){
				res += '<li><a class="page" href="#">' + i + '</a></li>';
			}
			res = '<li><a class="prev" href="">&laquo; Prev</a></li>' + res + '<li><a class="next" href="">Next &raquo;</a></li>';
			return res;
		}
		if($('.photo_gallery_body-player ul.items > li').length != 1){
			$('.inner .jcarousel-container').after('<div class="jcarousel-paginator-control"><div><ul>' + pageCount() + '</ul></div><div>');
		}
		pageCheck();
		
		$('.photo_gallery_body-player .page').click( jQuery.proxy(function(event) {
			var scrollPage = parseInt( $(event.currentTarget).text() );
			currentPage = scrollPage;
			carousel.scroll(scrollPage);
			pageCheck();
			return false;
		}, carousel ));
		
		$('.photo_gallery_body-player .thumb_content .prev').click( jQuery.proxy(function(event) {
			carousel.scroll(currentPage - 1);
			currentPage -= 1;
			pageCheck();
			return false;
		}, carousel ));
		
		$('.photo_gallery_body-player .thumb_content .next').click( jQuery.proxy(function(event) {
			carousel.scroll(currentPage + 1);
			currentPage += 1;
			pageCheck();
			return false;
		}, carousel ));
	
	};
	
	var carouselOptionsParams = {
		scroll:         1,
		visible:        1,
		buttonPrevHTML: '',
		buttonNextHTML: '',
		initCallback:   drawPagesNavigation
	};
	
	$GT('.photo_gallery_body-player .view_thumbs').photoGallery({
		hideButtonSelector: '.photo_gallery_body-player .hide_thumbnails',
		containerSelector:  '.thumb_content',
		headlineSelector:   '.photo_gallery_body-player h1',
		viewThumbsSelector: '.view_thumbs',
		carouselSelector:   '.photo_gallery_body-player ul.items',
		carouselOptions:    carouselOptionsParams,
		adHolder:           '.ad_300x250',
		adPusher:           '.sidebar'
	});
});

/* Spike photo gallery right bar push down */
jQuery('document').ready(function () 
{
	if (jQuery('.photo_gallery_body-player .sidebar').length)
	{ 
		jQuery('.photo_gallery_body').parent().parent().find('.right').css('margin-top',jQuery('.photo_gallery_body .sidebar').outerHeight()+10);
	}
});/* photo_gallery_carousel.js */
$(function() {
	// initialize scrollable
	$Crabapple(".photo_gallery_carousel ul").carousel({
		scroll: 1,
		buttonPrevHTML: '<a class="prev"><div class="arrow">Prev</div></a>',
		buttonNextHTML: '<a class="next"><div class="arrow">Next</div></a>',
		circularOnLast: false,
		initCallback: function(){$('.event_schedule-carousel ul').css({visibility: 'visible'});}
	});
});/* photo_gallery_results.js */

$(function() {
	$Crabapple('.photo_gallery_results .load_more').click(function() {
		var source = $('#url_holder').html();
		$('#url_holder').remove();
		$.ajax({
			type: 'GET',
			url: source
		}).done(function(html) {
			$Crabapple('.photo_gallery_results #gallery_container').append(html);
			if ($('#url_holder').attr('data-count') < 8) {
				$('.load_more').hide();
			}
		})
		return false;
	});
});/* platform_discussions.js */
/* 
 * JS code to implement tabs for GTRP M019 Discussions Module Template
 */
$(document).ready(function(){
		$Crabapple('.platform_discussions ul.module_tabs').tabs('.platform_discussions div.panes > ul');
});/* publisher_developer.js */
$(function() {
	$GT.utils.dropDownText('.publisher_developer .more', '.publisher_developer .pane', {
		type: 'height',
		height: '0',
		lessOnStart: true
	});
});/* search_results-site_search.js */
$(function(){
	var searchResultModule = new $GT.SearchResults();
	searchResultModule.init();
});/* search_videos-extras.js */
$(document).ready(function(){
	$Crabapple('.search_videos-extras .items').carousel({
		scroll: 3,
		circularOnLast: true
	})
});/* search_videos-list.js */
/**
 * @package: GTRP M005 Video line listings template
 * @module:  M005
 * @link:    http://jira.mtvi.com/browse/GTRP-487
 */

$(document).ready(function(){
	if($('.search_videos-list').length) {
		$GT('.search_videos-list').lineListingFiltersAJAX({
			reloadArea: '.search_videos-list ul',
			fragmentLink: $('.search_videos-list ul').attr('data-url'),
			paginationHolder: '.search_videos-list ul .pagination'
		});
	}
});
/* search_wordpress_blogs-teaser.js */
/*
 * Used in headline_articles child fragment (m078)
 */
$(document).ready(function(){
	var module = $(".search_wordpress_blogs-teaser");
	if(module.length !=0){
		var posts = $(".posts li");
		posts.each(function(index, element){
			var post = $(element).find('.headline_articles');
			$Crabapple.FluxWidget.requestCommentsFeed(post.attr('data-contentId'), 1, function(data){
				post.find(".info .counter").text(data.NumberOfComments);
			});
		});
	}
});/* show_information-carousel.js */
$(function() {
	$Crabapple(".show_information-carousel ul.items").carousel({
			wrap: 'circular',
			buttonNextHTML: '<a href="javascript:" class="next">Next</a>',
	        buttonPrevHTML: '<a href="javascript:" class="prev">Previous</a>',
			scroll: 1
	});
	$GT.utils.dropDownText('.show_information-carousel .more', '.show_information-carousel .pane', {
		type:'height',
		height: '80'
	});
});/* show_navigation.js */
$(document).ready(function(){
	$GT('.module_sub_header_navigation').pageNavigation({target:'.article_body-game_review .module_tabs'});
});/* similar_games-carousel.js */
$(document).ready(function(){
	$Crabapple('.similar_games-carousel ul').carousel({
		scroll: 2,
		buttonPrevHTML: '<div class="prev"><div>Previous</div></div>',
		buttonNextHTML: '<div class="next"><div>Next</div></div>'
	});
});

/* top_games.js */
$(document).ready(function(){
	if($('.top_games .platform').length) {
		$Crabapple('.top_games .module_tabs.sub').tabs('.top_games .subtabs_content');
		$GT('.top_games').platforms();
	} else {
		$Crabapple('.top_games .module_tabs').tabs('.top_games .tabs_content');
	};
	$Crabapple(".top_games").tooltip({
		target:  '.top_games .another_places h3 a',
		tooltip: '.top_games .another_places .tooltip'
	});
});/* trending-horizontal.js */
/*
 * Used in m101
 **/
$(document).ready(function(){
	$Crabapple('.trending-horizontal .scrollable').carousel({
		scroll: 1,
		buttonNextHTML: "<div class='next'><div>Next</div></div>",
		buttonPrevHTML: "<div class='prev'><div>Previous</div></div>"
	});
});/* video_download.js */
/**
 * Download button functionality for videos
 */
$(function() {
	$('.download_button').click(function(){
		var itemMgid = $('.video_information-player').attr('data-contentId');
		if (itemMgid) {
			mtvn.btg.Controller.sendLinkEvent({linkName:'download_'+pageName});
			$.getJSON('/feeds/video_download/'+itemMgid, function(data){
				if (data.url)
				{
					window.location = data.url;
				}
				else
				{
					alert('Sorry, there was an error retrieving a downloadable version of this video.');
				}
			})
		}
	});
});/* video_game_information.js */
$(function() {
	$GT.utils.dropDownText('.video_game_information .content .more', '.video_game_information .content .pane', {
		type:'firstElement', 
		tagName: 'p', 
		lessOnStart: true
	});
	$GT.utils.dropDownText('.video_game_information .sidebar .more', '.video_game_information .sidebar .pane', {
		type:'height', 
		height: '75'
	});
});/* video_game_release_calendar-list.js */
/**
 * line listing filter logic for M074 Video Game Release Calendar
 */
function videoGameReleaseCalendar(){
	var module = $('.video-game-release-calendar_list');
	// set up ajax refresh
	$('.open_week', module).click(function(e) {
		var link = $(e.target);
		var promotionId = $('.tabs_content', module).attr('promotion');
		$('.tabs_content').html("<div class='ajax_spinner'><img src='/sitewide/images/shared/ajax-loader.gif' width='32' height='32' alt='' /></div>");
		$.ajax({
			url: '/release_calendar_ajax/' + $.now() + '/' + promotionId + '/' + link.attr('date') + '/' + link.attr('contentType'),
			type: 'GET',
			success: function(data){
				module.html(data);
				videoGameReleaseCalendar();
			}
		});
	});

	// method to update video game releases after click on filter
	var filterRelease = function (value) {
		// skip empty <li>

		if (!$(value).html() || !$(value).html().trim()) {
			return 0;
		}

		// if no selected filters, show all elements
		if ($.isEmptyObject(this.getFilter('platform')) && $.isEmptyObject(this.getFilter('genre'))) {
			value.style.display = '';
			// external variable, should be defined in parent scope
			// used to return count of visible releases
			// because if no releases active we should hide whole panel
			return 1;
		}

		// receive video game information
		// receive list of platforms for current video game
		var videoGamePlatforms = $('.filter_platforms', value).html();
		if (videoGamePlatforms)
		{
			videoGamePlatforms = videoGamePlatforms.split(',');
		}
		// receive list of game genres for current video game
		var videoGameGenres = $('.filter_genres', value).html();
		if (videoGameGenres) {
			videoGameGenres = videoGameGenres.split(',');
		}

		// check if current video game should be hidden
		var hideVideoGame = true;
		for (var index in videoGameGenres) {
			if (this.isFilteredBy('genre', videoGameGenres[index])) {
				hideVideoGame = false;
				break;
			}
		}
		for (var index in videoGamePlatforms) {
			if (this.isFilteredBy('platform', videoGamePlatforms[index])) {
				hideVideoGame = false;
				break;
			}
		}

		value.style.display = hideVideoGame ? 'none' : '';

		return hideVideoGame ? 0 : 1;
	};

	$GT(".video-game-release-calendar_list .utils_filter").lineListingFilters({
		init : function() {
			filterRelease = filterRelease.bind(this);
		},
		click : function(e) {
			var weekReleasesCount = 0;
			var weekReleases = $('.week > li', module);
			for (var i=0; i<weekReleases.length; i++) {
				weekReleasesCount += filterRelease(weekReleases[i]);
			}

			// hide week block if no releases are showed
			if (!weekReleasesCount && $('.no_releases', module).hasClass('hidden')) {
				$('.no_releases', module).removeClass('hidden');
				$('.week', module).addClass('hidden');
			} else if (weekReleasesCount && !$('.no_releases', module).hasClass('hidden')) {
				$('.no_releases', module).addClass('hidden');
				$('.week', module).removeClass('hidden');
			}

			// iterate through quarter video game releases
			var quarterReleasesCount = 0;
			var quarterReleases = $('.quarter .grid > li', module);
			for (var i=0; i<quarterReleases.length; i++) {
				quarterReleasesCount += filterRelease(quarterReleases[i]);
			}

			// hide quarter block if no releases are showed
			if (quarterReleasesCount > 0) {
				$('.quarter', module).removeClass('hidden');
			} else {
				$('.quarter', module).addClass('hidden');
			}
		}
	});
};

$(document).ready(videoGameReleaseCalendar);/* video_games_release_schedule-calendar.js */
$(function() {
	// setup ul.module_tabs_vertical to work as tabs for each li directly under ul.panes
	$Crabapple(".video_games_release_schedule-calendar .module_tabs_vertical").tabs(".video_games_release_schedule-calendar .panes > li", {event:'mouseover'});
	// setup tooltips for H3 games titles
	$Crabapple(".video_games_release_schedule-calendar").tooltip({
		target:  '.video_games_release_schedule-calendar .panes h3',
		tooltip: '.video_games_release_schedule-calendar .tooltip'
	});
});/* video_games_with_review-review.js */
/**
 * @package: GTRP M097 Reviews Line Listings
 * @module:  M097
 */

$(document).ready(function() {
	if($('.video_games_with_review-review').length) {
		$GT('.video_games_with_review-review').lineListingFiltersAJAX({
			reloadArea: '.video_games_with_review-review .results',
			fragmentLink: $('.video_games_with_review-review .results').attr('data-url'),
			paginationHolder: null
		});
	}
});/* video_information-player.js */
/* 
 * m042
 */
$(document).ready(function(){
	var module = $('.video_information-player');
	if(module.length !=0){
		$Crabapple.FluxWidget.requestCommentsFeed(module.attr('data-contentId'), 1, function(data){
			module.find(".info .description .comments").append(data.NumberOfComments)
		});
	}
	
	var getHiDef = $('#getHiDefButton');
	if (getHiDef.length)
	{
		getHiDef.click(function(){
			var playerOverlay = $('.player-overlay');
			if (playerOverlay.length == 0)
			{
				$('body').append('<div class="player-overlay"><div class="getHD-message"><div class="closeHD-message">Close</div>The quality of videos is relative to the speed of your internet connection. If you are using a 56k modem to access GameTrailers, your connection speed may be too slow to get a high-quality video stream; therefore the video may appear blurry or be delivered like a filmstrip instead of a movie. In order to get high quality video with a slower connection, we offer a download  option for most of our videos.</div></div>');
				$('.player-overlay, .closeHD-message').click(function(){
					playerOverlay.fadeOut('fast');
				});
				
				playerOverlay = $('.player-overlay');
			}
			
			var footerPos = $('.footer').offset();
			
			playerOverlay.css({
				height: footerPos.top + 70
			}).fadeIn(400);
			
			var messageOverlay = $('.getHD-message');
			var playerWrap = $('.player_wrap');
			var playerPosition = playerWrap.offset();
			messageOverlay.css({
				left: ( parseInt(playerPosition.left)+ (parseInt(playerWrap.width() / 2) - parseInt(messageOverlay.width() / 2) - 30) )+ 'px',
				top: ( parseInt(playerPosition.top) + (parseInt(playerWrap.height() / 2) - parseInt(messageOverlay.height() / 2) - 30) )+ 'px',
			}).show();
		});
	}
});
/* video_information.js */
/*
 * Used in video information child fragment
 */
$(document).ready(function(){
	var module = $(".video_information");
	if(module.length !=0){
		$.each(module, function(){
			var item = $(this);
			$Crabapple.FluxWidget.requestCommentsFeed(item.attr('data-contentId'), 1, function(data){
				item.find(".status .comments").text(data.NumberOfComments)
			});
		})
	}
});

