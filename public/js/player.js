$(function(){
	var availableVideos = ['d1cc2edb-1f61-4b3d-bfe9-adb54628d001', 'af764be6-6498-471b-a33f-6dc536b0fb83', '17bdf2c8-118e-4aeb-ac92-dea7254f57d5', 'faf1be8a-c482-438c-a666-eb31eec7927a'];
	var videoId = window.location.hash ? window.location.hash.replace('#', '') : availableVideos[0];
	if (availableVideos.indexOf(videoId) == -1) {
		availableVideos.insert(0, videoId);
	}
	var videoWidth = "100%";
	var videoHeight = "100%";
	var autoPlay = true;
	var actioned = false;
	var hideNavigationBarDelay = 3000;
	function hidePlayerDelayed(){
		setTimeout(function (){
			if (!actioned) {
				if ($('.control_item[data-player-action=exitfullscreen]').length > 0) {
					$('#player_nav').slideUp(1000);
				}
			} else {
				actioned = false;
				setTimeout(hidePlayerDelayed, hideNavigationBarDelay);
			}
		}, hideNavigationBarDelay);				
	};
	function reloadPlayer() {
		$('#video-player').attr('data-mgid', 'mgid:arc:video:gametrailers.com:' + videoId)
		.attr('data-width', videoWidth).attr('data-height', videoHeight).attr('data-autoplay', autoPlay);
		$Crabapple('#video-player').player();
		$('.control_item[data-player-action=play]').attr('data-player-action', 'stop').removeClass('play').addClass('stop');
		if ($('.control_item[data-player-action=exitfullscreen]').length > 0) {
			goFullScreen();
		}
	};
	function goFullScreen() {
		$('#video-player').css('position', 'absolute').height($(document).height()).width($(document).width());
		if ($('#video-player').find('embed').length == 0) {
			$('#video-player').append($('<embed wmode="transparent"/>'));
		}
		hidePlayerDelayed();
		actioned = false;
	};
	function exitFullScreen() {
		$('#video-player').css('position', 'relative').css('height', '100%').css('width', '100%');
	};
	reloadPlayer();
	var player = {
		'play': function (){
			$('.control_item.active').attr('data-player-action', 'stop').removeClass('play').addClass('stop');
			MTVNPlayer.getPlayers()[0].play();
		},
		'stop': function (){
			$('.control_item.active').attr('data-player-action', 'play').removeClass('stop').addClass('play');
			MTVNPlayer.getPlayers()[0].pause();
		},
		'next': function (){
			var currentIndex = availableVideos.indexOf(videoId);
			if (currentIndex + 1 < availableVideos.length) {
				videoId = availableVideos[currentIndex+1];
				window.location.hash = videoId;
				reloadPlayer();
			}
		},
		'previous': function (){
			var currentIndex = availableVideos.indexOf(videoId);
			if (currentIndex - 1 > -1) {
				videoId = availableVideos[currentIndex-1];
				window.location.hash = videoId;
				reloadPlayer();
			}
		},
		'fullscreen': function () {
			goFullScreen();
			$('.control_item.active').attr('data-player-action', 'exitfullscreen').removeClass('fullscreen').addClass('exitfullscreen');;
		},
		'exitfullscreen': function () {
			exitFullScreen();
			$('.control_item.active').attr('data-player-action', 'fullscreen').removeClass('exitfullscreen').addClass('fullscreen');;
		}
	};
	var onPlayerReady = function (){
		if (window.location.href.indexOf('full-screen') > 0) {
			var $currentItem = $('.control_item.active');
			$currentItem.removeClass('active');
			$('.control_item[data-player-action=fullscreen]').addClass('active');
			player['fullscreen']();
			MTVNPlayer.getPlayers()[0].unbind('onStateChange', onPlayerReady);
		}
	}
	MTVNPlayer.getPlayers()[0].bind('onStateChange', onPlayerReady);
	$(document).keydown(function(event){
		if ($('.control_item[data-player-action=exitfullscreen]').length > 0) {
			if (!$('#player_nav').is(':visible')) {
				$('#player_nav').slideDown(1000, function () {
					hidePlayerDelayed();
				});
				return;
			}
		}
		actioned = true;
		switch (event.which){
			case 37: {
				var $currentItem = $('.control_item.active');
				var $prevItem = $currentItem.prev('.control_item');
				if ($prevItem.length > 0) {
					$currentItem.removeClass('active');
					$prevItem.addClass('active');
				}
			}
			break;
			case 38: {
				window.location.href = "index.html";
				break;
			}
			case 39: {
				var $currentItem = $('.control_item.active');
				var $nextItem = $currentItem.next('.control_item');
				if ($nextItem.length > 0) {
					$currentItem.removeClass('active');
					$nextItem.addClass('active');
				}
				break;
			}
			case 40: {
				window.history.back();
				// Down arrow do nothing
				break;
			}
			case 13: {
				var action = $('.control_item.active').attr('data-player-action');
				player[action]();
				break;
			}
		}
		return false;
	});
});